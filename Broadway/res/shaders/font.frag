#version 330 core

uniform sampler2D texUnit;

in vec4 outColor;
in vec2 outTexCoords;

void main()
{
    vec4 texColor = texture(texUnit, outTexCoords);

    gl_FragColor = texColor;
}