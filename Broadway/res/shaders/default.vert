#version 330 core

uniform mat4 projection;
uniform mat4 view;

in vec4 position;
in vec4 color;
in vec2 texCoord;
in vec4 normal;

out vec4 outColor;
out vec2 outTexCoords;

void main()
{
    outColor      = color;
    outTexCoords  = texCoord;
    gl_Position   = projection * view * position;
}