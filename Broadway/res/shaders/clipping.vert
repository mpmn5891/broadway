#version 330 core

uniform mat4 projection;
uniform mat4 view;

in vec4 position;
in vec4 color;
in vec2 texCoord;
in vec4 normal;
in vec4 clipLeft;
in vec4 clipRight;
in vec4 clipTop;
in vec4 clipBottom;

out vec4 outColor;
out vec2 outTexCoords;

void main()
{
    outColor      = color;
    outTexCoords  = texCoord;
    gl_Position   = projection * view * position;
    gl_ClipDistance[0] = dot(position, clipLeft);
    gl_ClipDistance[1] = dot(position, clipRight);
    gl_ClipDistance[2] = dot(position, clipTop);
    gl_ClipDistance[3] = dot(position, clipBottom);
}