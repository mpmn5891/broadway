package bmg.broadway.theater.entitySystem;

import bmg.broadway.theater.entitySystem.components.TransformComponent;

public class Component
{
	protected Entity entity;
	protected TransformComponent transformComponent;

	void setup(Entity entity)
	{
		this.entity = entity;
		this.transformComponent = entity.getTransformComponent();
		onCreate();
	}

	protected void onCreate()
	{

	}

	protected void onInit()
	{

	}

	protected void onUpdate(float delta)
	{

	}

	protected void onRender(float delta)
	{

	}

	protected void onDispose()
	{

	}
}
