package bmg.broadway.theater.entitySystem.components;

import bmg.broadway.theater.entitySystem.Component;
import bmg.emef.math.Quaternion;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class TransformComponent extends Component
{
	private final Vector3 position = new Vector3(0, 0, 0);
	private final Vector3 scale = new Vector3(1, 1, 1);
	private final Quaternion rotation = new Quaternion();

	private final Transform localTransform = new Transform();
	private final Transform worldTransform = new Transform();

	private TransformComponent parent = null;

	private boolean hasChanged = false;
	private boolean changed = true;

	protected void recomputeTransforms()
	{
		hasChanged = false;
		if (changed)
		{
			localTransform.reset().scale(this.scale).rotate(this.rotation).translate(this.position);
			hasChanged = true;
			changed = false;
		}
		if (parent != null)
		{
			worldTransform.set(localTransform).apply(parent.getWorldTransform());
		}
	}

	@Override
	protected void onUpdate(float delta)
	{
		recomputeTransforms();
	}

	public Vector3 getPosition()
	{
		return position;
	}

	public TransformComponent setPosition(Vector2 v)
	{
		return setPosition(v.x, v.y);
	}

	public TransformComponent setPosition(float x, float y)
	{
		return setPosition(x, y, 0);
	}

	public TransformComponent setPosition(Vector3 v)
	{
		return setPosition(v.x, v.y, v.z);
	}

	public TransformComponent setPosition(float x, float y, float z)
	{
		changed = true;
		this.position.set(x, y, z);
		return this;
	}

	public TransformComponent translate(Vector2 v)
	{
		return translate(v.x, v.y);
	}

	public TransformComponent translate(float x, float y)
	{
		return translate(x, y, 0);
	}

	public TransformComponent translate(Vector3 v)
	{
		return translate(v.x, v.y, v.z);
	}

	public TransformComponent translate(float x, float y, float z)
	{
		changed = true;
		this.position.add(x, y, z);
		return this;
	}

	public Vector3 getScale()
	{
		return scale;
	}

	public TransformComponent setScale(Vector2 v)
	{
		return setScale(v.x, v.y);
	}

	public TransformComponent setScale(float sx, float sy)
	{
		return setScale(sx, sy, 1);
	}

	public TransformComponent setScale(Vector3 v)
	{
		return setScale(v.x, v.y, v.z);
	}

	public TransformComponent setScale(float sx, float sy, float sz)
	{
		changed = true;
		this.scale.set(sx, sy, sz);
		return this;
	}

	public TransformComponent scale(Vector2 v)
	{
		return scale(v.x, v.y);
	}

	public TransformComponent scale(float sx, float sy)
	{
		return scale(sx, sy, 1);
	}

	public TransformComponent scale(Vector3 v)
	{
		return scale(v.x, v.y, v.z);
	}

	public TransformComponent scale(float sx, float sy, float sz)
	{
		changed = true;
		this.scale.scale(sx, sy, sz);
		return this;
	}

	public Quaternion getRotation()
	{
		return rotation;
	}

	public TransformComponent setRotation(Quaternion q)
	{
		changed = true;
		this.rotation.set(q);
		return this;
	}

	public TransformComponent setRotation(Vector3 v)
	{
		changed = true;
		this.rotation.set(v.x, v.y, v.z);
		return this;
	}

	public TransformComponent setRotation(float r)
	{
		changed = true;
		this.rotation.set(0, 0, r);
		return this;
	}

	public TransformComponent setRotation(float rx, float ry, float rz)
	{
		changed = true;
		this.rotation.set(rx, ry, rz);
		return this;
	}

	public TransformComponent rotate(Vector3 v)
	{
		changed = true;
		this.rotation.set(v.x, v.y, v.z);
		return this;
	}

	public TransformComponent rotate(Quaternion q)
	{
		changed = true;
		this.rotation.multiply(q);
		return this;
	}

	public TransformComponent rotate(float r)
	{
		return rotate(0, 0, r);
	}

	public TransformComponent rotate(float rx, float ry, float rz)
	{
		changed = true;
		Quaternion quat = Quaternion.REUSABLE_STACK.pop();
		this.rotation.multiply(quat.set(rx, ry, rz));
		Quaternion.REUSABLE_STACK.push(quat);
		return this;
	}

	public Transform getLocalTransform()
	{
		return localTransform;
	}

	public Transform getWorldTransform()
	{
		return worldTransform;
	}

	public TransformComponent getParent()
	{
		return parent;
	}

	public void setParent(TransformComponent parent)
	{
		this.parent = parent;
	}

	public boolean hasChanged()
	{
		return hasChanged || (parent != null && parent.hasChanged());
	}
}
