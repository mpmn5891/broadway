package bmg.broadway.theater.entitySystem.components;

public class MouseInputComponent
{
	public boolean mouseOver = false;
	public boolean mouseClicked = false;

	public boolean isMouseOver()
	{
		return mouseOver;
	}

	public boolean isClicked()
	{
		return mouseClicked;
	}
}
