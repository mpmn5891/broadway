package bmg.broadway.theater.entitySystem.components;

import bmg.broadway.geometry.Polygon;
import bmg.broadway.theater.entitySystem.Component;
import bmg.emef.graphics.Color;

public class PolygonRenderComponent extends Component
{
	private Polygon polygon;
	private Color color;

	public PolygonRenderComponent()
	{
		this(new Polygon());
	}

	public PolygonRenderComponent(Polygon polygon)
	{
		this(polygon, Color.BLACK);
	}

	public PolygonRenderComponent(Polygon polygon, Color color)
	{
		this.polygon = polygon;
		this.color = color;
	}

	public PolygonRenderComponent polygon(Polygon polygon)
	{
		this.polygon = polygon;
		return this;
	}

	public PolygonRenderComponent color(Color color)
	{
		this.color = color;
		return this;
	}

	public Polygon getPolygon()
	{
		return polygon;
	}

	public Color getColor()
	{
		return color;
	}
}
