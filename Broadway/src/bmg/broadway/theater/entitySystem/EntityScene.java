package bmg.broadway.theater.entitySystem;

import java.util.ArrayList;
import java.util.List;

import bmg.broadway.theater.Theater;
import bmg.broadway.theater.entitySystem.components.TransformComponent;
import bmg.emef.events.callbacks.BiCallback;
import bmg.emef.events.callbacks.UniCallback;

public class EntityScene
{
	private final List<BiCallback<EntityScene, Float>> updateSystems = new ArrayList<>();
	private final List<BiCallback<EntityScene, Float>> renderSystems = new ArrayList<>();

	private final List<Entity> entities = new ArrayList<>();

	public EntityScene()
	{
		registerUpdateSystem(EntityScene::defaultComponentUpdateSystem);
		registerRenderSystem(EntityScene::defaultComponentRenderSystem);
	}

	public void init()
	{
		forEachEntity(e ->
		{
			e.forEachComponent(Component::onInit);
		});
	}

	public void update(float delta)
	{
		for (BiCallback<EntityScene, Float> system : updateSystems)
		{
			system.invoke(this, delta);
		}
	}

	public void render(float delta)
	{
		for (BiCallback<EntityScene, Float> system : renderSystems)
		{
			system.invoke(this, delta);
		}
	}

	public EntityScene addEntity(Entity e)
	{
		entities.add(e);
		return this;
	}

	public void removeEntity(Entity e)
	{
		if (!e.isDestroyed())
		{
			e.destroy();
		}
		entities.remove(e);
	}

	public void forEachEntity(UniCallback<Entity> callback)
	{
		for (Entity e : entities)
		{
			if (!e.isDestroyed())
			{
				callback.invoke(e);
			}
		}
	}

	public void forEachRootEntity(UniCallback<Entity> callback)
	{
		forEachEntityWithComponent(TransformComponent.class, e ->
		{
			if (e.getTransformComponent().getParent() == null)
			{
				callback.invoke(e);
			}
		});
	}

	public <T extends Component> void forEachEntityWithComponent(Class<T> klass, UniCallback<Entity> callback)
	{
		forEachEntity(e ->
		{
			if (e.hasComponent(klass))
			{
				callback.invoke(e);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> void forEachEntityOfType(Class<T> klass, UniCallback<T> callback)
	{
		forEachEntity(e ->
		{
			if (e.getClass() == klass || e.getClass().getSuperclass() == klass)
			{
				callback.invoke((T) e);
			}
		});
	}

	public <T extends Component> List<Entity> getEntitiesWithComponent(Class<T> klass, List<Entity> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		forEachEntityWithComponent(klass, dest::add);

		return dest;
	}

	public <T extends Entity> List<Entity> getEntitiesOfType(Class<T> klass, List<Entity> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		forEachEntityOfType(klass, dest::add);

		return dest;
	}

	public EntityScene registerUpdateSystem(BiCallback<EntityScene, Float> system)
	{
		updateSystems.add(system);
		return this;
	}

	public EntityScene registerRenderSystem(BiCallback<EntityScene, Float> system)
	{
		renderSystems.add(system);
		return this;
	}

	private static void defaultComponentUpdateSystem(EntityScene entityScene, float delta)
	{
		entityScene.forEachEntity(entity ->
		{
			entity.forEachComponent(c ->
			{
				c.onUpdate(delta);
			});
			if (entity.isDestroyed())
			{
				Theater.getInstance().getTaskManager().runOnUpdate(() ->
				{
					entityScene.removeEntity(entity);
				});
			}
		});
	}

	private static void defaultComponentRenderSystem(EntityScene entityScene, float delta)
	{
		entityScene.forEachEntity(entity ->
		{
			if (!entity.isDestroyed())
			{
				entity.forEachComponent(c ->
				{
					c.onRender(delta);
				});
			}
		});
	}
}
