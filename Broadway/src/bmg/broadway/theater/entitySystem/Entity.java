package bmg.broadway.theater.entitySystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bmg.broadway.theater.entitySystem.components.TransformComponent;
import bmg.broadway.util.IDGen;
import bmg.emef.events.callbacks.UniCallback;

public class Entity
{
	public final long ID = IDGen.generate();
	private final List<Component> components = new ArrayList<>();
	private final Map<ComponentType, List<Component>> componentsByType = new HashMap<>();
	private boolean destroyed = false;
	private TransformComponent transformComponent;

	public Entity()
	{
		addComponent(transformComponent = new TransformComponent());
	}

	public void onAddToEntityScene(EntityScene entityScene)
	{
		// optional override method
	}

	public void onRemoveFromEntityScene(EntityScene entityScene)
	{
		// optional override method
	}

	public Entity addComponent(Component c)
	{
		if (destroyed)
		{
			return null;
		}

		components.add(c);

		ComponentType type = ComponentType.of(c.getClass());
		List<Component> typedList = componentsByType.get(type);
		if (typedList == null)
		{
			componentsByType.put(type, typedList = new ArrayList<>());
		}
		typedList.add(c);
		c.setup(this);
		return this;
	}

	public Entity removeComponent(Component c)
	{
		if (destroyed)
		{
			return null;
		}

		components.remove(c);

		ComponentType type = ComponentType.of(c.getClass());
		List<Component> typedList = componentsByType.get(type);
		if (typedList != null)
		{
			typedList.remove(c);
		}
		c.onDispose();
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> T getComponentOfType(Class<T> klass)
	{
		ComponentType type = ComponentType.of(klass);
		List<Component> typedList = componentsByType.get(type);
		if (typedList == null || typedList.isEmpty())
		{
			return null;
		}
		return (T) typedList.get(0);
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> List<T> getComponentsOfType(Class<T> klass, List<T> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		ComponentType type = ComponentType.of(klass);
		List<Component> typedList = componentsByType.get(type);
		if (typedList != null)
		{
			for (Component c : typedList)
			{
				dest.add((T) c);
			}
		}
		return dest;
	}

	public List<Component> getComponents(List<Component> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		for (Component c : components)
		{
			dest.add(c);
		}

		return dest;
	}

	public <T extends Component> boolean hasComponent(Class<T> klass)
	{
		ComponentType type = ComponentType.of(klass);
		List<Component> typedList = componentsByType.get(type);
		return !(typedList == null || typedList.isEmpty());
	}

	public void forEachComponent(UniCallback<Component> callback)
	{
		for (Component c : components)
		{
			callback.invoke(c);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> void forEachComponentOfType(Class<T> klass, UniCallback<T> callback)
	{
		ComponentType type = ComponentType.of(klass);
		List<Component> typedList = componentsByType.get(type);
		if (typedList != null)
		{
			for (Component c : typedList)
			{
				callback.invoke((T) c);
			}
		}
	}

	public boolean isDestroyed()
	{
		return destroyed;
	}

	public TransformComponent getTransformComponent()
	{
		return transformComponent;
	}

	public void destroy()
	{
		if (destroyed)
		{
			return;
		}
		for (Component c : components)
		{
			c.onDispose();
		}
		components.clear();
		componentsByType.clear();
		destroyed = true;
	}
}
