package bmg.broadway.theater.entitySystem;

import java.util.HashMap;
import java.util.Map;

class ComponentType
{
	private static final Map<Class<? extends Component>, ComponentType> componentTypes = new HashMap<>();
	private static int typeIndex = 0;
	private int index;

	private ComponentType()
	{
		this.index = typeIndex++;
	}

	static ComponentType of(Class<? extends Component> klass)
	{
		ComponentType type = componentTypes.get(klass);
		if (type == null)
		{
			componentTypes.put(klass, type = new ComponentType());
		}
		return type;
	}

	boolean equals(ComponentType other)
	{
		return this.index == other.index;
	}

	public int getIndex()
	{
		return index;
	}
}
