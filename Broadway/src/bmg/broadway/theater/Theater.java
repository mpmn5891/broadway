package bmg.broadway.theater;

import java.util.HashMap;
import java.util.Map;

import bmg.emef.environment.Environment;

public class Theater
{
	private static Theater instance = null;

	private Map<String, Stage> stages;
	private Stage current;
	private TaskManager taskManager;

	public static Theater getInstance()
	{
		if (Theater.instance == null)
		{
			Theater.instance = new Theater();
		}
		return Theater.instance;
	}

	private Theater()
	{
		Environment.eventManager.registerInitializeListener(this::onEngineInit);
		Environment.eventManager.registerUpdateListener(this::onEngineUpdate);
		Environment.eventManager.registerRenderListener(this::onEngineRender);
		Environment.eventManager.registerFocusListener(this::onEngineFocusChanged);
		Environment.eventManager.registerResizeListener(this::onEngineWindowResized);
		Environment.eventManager.registerDisposeListener(this::onEngineDispose);
		taskManager = TaskManager.getInstance();
	}

	public void lightsCameraAction()
	{
		taskManager.runOnUpdate(() ->
		{
			Environment.display.setIcon(Environment.io.getImageReader().readImage(Environment.io.createInternalFileHandle("textures/broadway.jpg")));
		});
		Environment.start();
	}

	public Theater addStage(Stage s)
	{
		if (s != null)
		{
			if (stages == null)
			{
				stages = new HashMap<>();
			}

			if (Environment.Analytics.glInitialized)
			{
				s.setup();
			}
			else
			{
				getTaskManager().runOnPostInit(() ->
				{
					s.setup();
				});
			}
			stages.put(s.getName(), s);
		}
		return this;
	}

	public Stage setCurrentStage(String stageName)
	{
		if (stages == null || stages.isEmpty())
		{
			return null;
		}
		Stage s = stages.get(stageName);
		if (s != null)
		{
			if (current == null)
			{
				current = s;
				current.enter();
			}
			else
			{
				if (!current.equals(s))
				{
					current.leave();
					current = s;
					current.enter();
				}
			}
			return current;
		}
		return null;
	}

	public Stage currentStage()
	{
		return current;
	}

	public Stage removeStage(String stageName)
	{
		if (stages == null || stages.isEmpty())
		{
			return null;
		}
		Stage s = stages.remove(stageName);
		if (current != null)
		{
			if (current.equals(s))
			{
				current.leave();
				current = null;
			}
		}
		s.dispose();
		return s;
	}

	public boolean containsStage(String stageName)
	{
		if (stages == null || stages.isEmpty())
		{
			return false;
		}
		return stages.containsKey(stageName);
	}

	public Stage reSetupStage(String stageName)
	{
		if (stages == null || stages.isEmpty())
		{
			return null;
		}
		Stage s = stages.get(stageName);
		if (s != null)
		{
			removeStage(s.getName());
			addStage(s);
			return s;
		}
		return null;
	}

	public TaskManager getTaskManager()
	{
		return taskManager;
	}

	private void onEngineInit()
	{

	}

	private void onEngineUpdate(float delta)
	{
		if (current != null)
		{
			current.update(delta);
		}
	}

	private void onEngineRender(float delta)
	{
		if (current != null)
		{
			current.render(delta);
		}
	}

	private void onEngineFocusChanged(boolean hasFocus)
	{
		if (current != null)
		{
			current.focusChanged(hasFocus);
		}
	}

	private void onEngineWindowResized()
	{
		if (current != null)
		{
			current.resized(Environment.display.getWidth(), Environment.display.getHeight());
		}
	}

	private void onEngineDispose()
	{
		for (Stage s : stages.values())
		{
			if (s == current)
			{
				s.leave();
			}
			s.dispose();
		}
	}
}
