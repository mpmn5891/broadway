package bmg.broadway.theater;

public abstract class Stage
{
	private String name;

	public Stage(String name)
	{
		this.name = name;
	}

	public void setup()
	{
		// optional override method
	}

	public void enter()
	{
		// optional override method
	}

	public void update(float delta)
	{
		// optional override method
	}

	public void render(float delta)
	{
		// optional override method
	}

	public void resized(float width, float height)
	{
		// optional override method
	}

	public void focusChanged(boolean hasFocus)
	{
		// optional override method
	}

	public void leave()
	{
		// optional override method
	}

	public void dispose()
	{
		// optional override method
	}

	public String getName()
	{
		return name;
	}
}
