package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.emef.util.MathUtils;

/**
 * UI Scene Node that holds a collection of Widget actors and displays them in a horizontal shelf
 * 
 * @author Matthew Newbury
 *
 */
public class WidgetShelf extends WidgetGroup
{
	private float preferredWidth, preferredHeight;
	private float minimumWidth, minimumHeight;
	private float maximumWidth, maximumHeight;

	private Alignment align = Alignment.CENTER;

	public WidgetShelf getAsWidgetShelf()
	{
		return (WidgetShelf) this;
	}

	@Override
	public void pack()
	{
		minimumWidth = 0;
		preferredWidth = 0;
		maximumWidth = 0;
		for (Actor actor : members)
		{
			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.pack();
				if (actor instanceof Widget)
				{
					Widget widget = (Widget) actor;
					minimumWidth += widget.getLeftPadding() + widget.getMinimumWidth() + widget.getRightPadding();
					preferredWidth += widget.getLeftPadding() + widget.getPreferredWidth() + widget.getRightPadding();
					maximumWidth += widget.getLeftPadding() + widget.getMaximumWidth() + widget.getRightPadding();
					minimumHeight = MathUtils.max(minimumHeight, widget.getMinimumHeight());
					preferredHeight = MathUtils.max(preferredHeight, widget.getPreferredHeight());
					maximumHeight = MathUtils.max(maximumHeight, widget.getMaximumHeight());
				}
				else
				{
					minimumWidth += layout.getMaximumWidth();
					preferredWidth += layout.getPreferredWidth();
					maximumWidth += layout.getMaximumWidth();
					minimumHeight = MathUtils.max(minimumHeight, layout.getMinimumHeight());
					preferredHeight = MathUtils.max(preferredHeight, layout.getPreferredHeight());
					maximumHeight = MathUtils.max(maximumHeight, layout.getMaximumHeight());
				}
			}
			else
			{
				minimumWidth += actor.getWidth();
				preferredWidth += actor.getWidth();
				maximumWidth += actor.getWidth();
				minimumHeight = MathUtils.max(minimumHeight, actor.getHeight());
				preferredHeight = MathUtils.max(preferredHeight, actor.getHeight());
				maximumHeight = MathUtils.max(maximumHeight, actor.getHeight());
			}
		}
		super.pack();
	}

	@Override
	public void doLayout()
	{
		float x = 1;
		float y = 0;
		float centerLine = getHeight() / 2;
		float lastRightPadding = -1;

		for (Actor actor : members)
		{
			float lp = 0;
			float rp = 0;

			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.invalidate();
				layout.pack();
			}

			if (actor instanceof Widget)
			{
				Widget widget = (Widget) actor;
				lp = widget.getLeftPadding();
				rp = widget.getRightPadding();
			}

			if (lastRightPadding != -1)
			{
				lastRightPadding = MathUtils.max(lastRightPadding, lp);
				x += lastRightPadding;
			}

			switch (align)
			{
				case BOTTOM:
				{
					y = getHeight() - actor.getHeight();
					break;
				}
				case CENTER:
				{
					y = centerLine - (actor.getHeight() / 2);
					break;
				}
				case TOP:
				{
					y = 0;
					break;
				}
			}

			actor.getTransform().setPosition(x, y);

			x += actor.getWidth() + 1;

			lastRightPadding = rp;
		}

		setWidth(x);
	}

	public WidgetShelf setAlignment(Alignment align)
	{
		this.align = align;
		return this;
	}

	@Override
	public float getPreferredWidth()
	{
		return preferredWidth;
	}

	@Override
	public float getPreferredHeight()
	{
		return preferredHeight;
	}

	@Override
	public float getMinimumWidth()
	{
		return minimumWidth;
	}

	@Override
	public float getMinimumHeight()
	{
		return minimumHeight;
	}

	@Override
	public float getMaximumWidth()
	{
		return maximumWidth;
	}

	@Override
	public float getMaximumHeight()
	{
		return maximumHeight;
	}

	public enum Alignment
	{
		TOP,
		CENTER,
		BOTTOM;
	}
}
