package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.graphics.TrueTypeFont;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;

public class Button extends Widget
{
	private TrueTypeFont font = TrueTypeFont.DEFAULT;

	private Color backgroundColor = Color.LIGHT_BLUE;
	private Color highlightColor = Color.WHITE;
	private Color borderColor = Color.BLACK;
	private Color textColor = Color.BLACK;

	private boolean hoverFill = true;
	private boolean clicked, hovered;
	private String text = "";

	public Button()
	{
		setPreferredSize(100, 25);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		hovered = (getScene().getMouseOverActor() == this);

		if (hovered)
		{
			if (Mouse.isButtonDown(Mouse.ButtonCode.BUTTON_1))
			{
				clicked = true;
				Mouse.consume(Mouse.ButtonCode.BUTTON_1);
			}
		}
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();
		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			g.setDrawColor(hovered ? hoverFill ? highlightColor : backgroundColor : backgroundColor);
			g.fillRoundedRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height, 8);
			g.setDrawColor(hoverFill ? borderColor : hovered ? highlightColor : borderColor);
			g.drawRoundedRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height, 8);
			g.setFont(font);
			g.setDrawColor(textColor);
			g.drawCenteredString(getBounds().x, getBounds().y, getBounds().width, getBounds().height, text);
			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	public Button setText(String text)
	{
		this.text = text;
		return this;
	}

	public String getText()
	{
		return text;
	}

	public Button setBackgroundColor(Color background)
	{
		this.backgroundColor = background;
		return this;
	}

	public Button setHighlightColor(Color highlight)
	{
		this.highlightColor = highlight;
		return this;
	}

	public Button setBorderColor(Color border)
	{
		this.borderColor = border;
		return this;
	}

	public Button setFontColor(Color font)
	{
		this.textColor = font;
		return this;
	}

	public Button setFont(TrueTypeFont font)
	{
		this.font = font;
		return this;
	}

	public Button setFillHoverStyle(Boolean value)
	{
		this.hoverFill = value;
		return this;
	}

	public boolean isClicked()
	{
		return isClicked(true);
	}

	public boolean isClicked(boolean consume)
	{
		if (clicked && consume)
		{
			clicked = false;
			return true;
		}
		return false;
	}

	public boolean isHovered()
	{
		return hovered;
	}

	public Button getAsButton()
	{
		return (Button) this;
	}
}
