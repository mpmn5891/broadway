package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.emef.environment.Environment;

public class Widget extends Actor implements Layout
{
	protected float preferredWidth, preferredHeight;
	protected float minimumWidth, minimumHeight;
	protected float maximumWidth, maximumHeight;

	protected float leftPadding, topPadding, rightPadding, bottomPadding;

	private boolean needsLayout = false;
	private boolean fillParent = false;

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();
	}

	public Widget getAsWidget()
	{
		return (Widget) this;
	}

	@Override
	public void validate()
	{
		Actor parent = getParent();
		if (parent != null && fillParent)
		{
			float parentWidth = parent.getWidth();
			float parentHeight = parent.getHeight();
			if (parent == getScene().getRootNode())
			{
				parentWidth = Environment.display.getWidth();
				parentHeight = Environment.display.getHeight();
			}

			if (getWidth() != parentWidth || getHeight() != parentHeight)
			{
				setWidth(parentWidth);
				setHeight(parentHeight);
				invalidate();
			}
		}

		if (needsLayout)
		{
			needsLayout = false;
			doLayout();
		}

	}

	@Override
	public void invalidate()
	{
		needsLayout = true;
	}

	@Override
	public void pack()
	{
		setWidth(getPreferredWidth());
		setHeight(getPreferredHeight());
		validate();
	}

	@Override
	public void doLayout()
	{

	}

	@Override
	public float getPreferredWidth()
	{
		return preferredWidth;
	}

	@Override
	public float getPreferredHeight()
	{
		return preferredHeight;
	}

	@Override
	public float getMinimumWidth()
	{
		return minimumWidth;
	}

	@Override
	public float getMinimumHeight()
	{
		return minimumHeight;
	}

	@Override
	public float getMaximumWidth()
	{
		return maximumWidth;
	}

	@Override
	public float getMaximumHeight()
	{
		return maximumHeight;
	}

	public void setPreferredSize(float width, float height)
	{
		this.preferredWidth = width;
		this.preferredHeight = height;
	}

	public void setPreferredWidth(float preferredWidth)
	{
		this.preferredWidth = preferredWidth;
	}

	public void setPreferredHeight(float preferredHeight)
	{
		this.preferredHeight = preferredHeight;
	}

	public void setMinimumSize(float width, float height)
	{
		this.minimumWidth = width;
		this.minimumHeight = height;
	}

	public void setMinimumWidth(float minimumWidth)
	{
		this.minimumWidth = minimumWidth;
	}

	public void setMinimumHeight(float minimumHeight)
	{
		this.minimumHeight = minimumHeight;
	}

	public void setMaximumSize(float width, float height)
	{
		this.maximumWidth = width;
		this.maximumHeight = height;
	}

	public void setMaximumWidth(float maximumWidth)
	{
		this.maximumWidth = maximumWidth;
	}

	public void setMaximumHeight(float maximumHeight)
	{
		this.maximumHeight = maximumHeight;
	}

	public void setFillParent(boolean shouldFill)
	{
		this.fillParent = shouldFill;
	}

	public float getLeftPadding()
	{
		return leftPadding;
	}

	public float getTopPadding()
	{
		return topPadding;
	}

	public float getRightPadding()
	{
		return rightPadding;
	}

	public float getBottomPadding()
	{
		return bottomPadding;
	}

	public void setPadding(float padding)
	{
		setPadding(padding, padding);
	}

	public void setPadding(float leftRight, float topBottom)
	{
		setPadding(leftRight, topBottom, leftRight, topBottom);
	}

	public void setPadding(float left, float top, float right, float bottom)
	{
		leftPadding = left;
		topPadding = top;
		rightPadding = right;
		bottomPadding = bottom;
	}

	public void setLeftPadding(float leftPadding)
	{
		this.leftPadding = leftPadding;
	}

	public void setTopPadding(float topPadding)
	{
		this.topPadding = topPadding;
	}

	public void setRightPadding(float rightPadding)
	{
		this.rightPadding = rightPadding;
	}

	public void setBottomPadding(float bottomPadding)
	{
		this.bottomPadding = bottomPadding;
	}
}
