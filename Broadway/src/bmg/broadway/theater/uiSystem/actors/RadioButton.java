package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;

public class RadioButton extends Widget
{
	private buttonShape shape = buttonShape.circle;

	private Color background = Color.LIGHT_BLUE;
	private Color border = Color.BLACK;
	private Color highlight = Color.WHITE;

	private boolean selected = false;
	private boolean hovered = false;

	private RadioGroup group = null;

	public RadioButton()
	{
		setPreferredSize(25, 25);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		hovered = getScene().getMouseOverActor() == this;

		if (hovered)
		{
			if (Mouse.isButtonDown(Mouse.ButtonCode.BUTTON_1))
			{
				selected = !selected;
				Mouse.consume(Mouse.ButtonCode.BUTTON_1);
				if (group != null)
				{
					group.notify(this);
				}
			}
		}
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();

		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			g.setDrawColor(background);
			float adjustedWidth = getBounds().width / 1.5f, adjustedHeight = getBounds().height / 1.5f;
			switch (shape)
			{
				case box:
				{
					g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					g.setDrawColor(hovered ? highlight : border);
					if (selected)
					{
						g.fillRectangle(getBounds().x + ((getBounds().width / 2) - adjustedWidth / 2), getBounds().y + ((getBounds().height / 2) - adjustedHeight / 2),
								adjustedWidth, adjustedHeight);
					}
					g.drawRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					break;
				}
				case circle:
				{
					g.fillOval(getBounds().x, getBounds().y, getBounds().width / 2, getBounds().height / 2);
					g.setDrawColor(hovered ? highlight : border);
					if (selected)
					{
						g.fillOval(getBounds().x + ((getBounds().width / 2) - adjustedWidth / 2), getBounds().y + ((getBounds().height / 2) - adjustedHeight / 2),
								adjustedWidth / 2, adjustedHeight / 2);
					}
					g.drawOval(getBounds().x, getBounds().y, getBounds().width / 2, getBounds().height / 2);
					break;
				}
				case roundedBox:
				{
					g.fillRoundedRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height, 8);
					g.setDrawColor(hovered ? highlight : border);
					if (selected)
					{
						g.fillOval(getBounds().x + ((getBounds().width / 2) - adjustedWidth / 2), getBounds().y + ((getBounds().height / 2) - adjustedHeight / 2),
								adjustedWidth / 2, adjustedHeight / 2);
					}
					g.drawRoundedRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height, 8);
					break;
				}
				case toggle:
				{
					g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					if (selected)
					{
						g.setDrawColor(highlight);
						g.fillRectangle(getBounds().x + getBounds().width / 2, getBounds().y, getBounds().width / 2, getBounds().height);
					}
					else
					{
						g.setDrawColor(border);
						g.fillRectangle(getBounds().x, getBounds().y, getBounds().width / 2, getBounds().height);
					}
					g.setDrawColor(hovered ? highlight : border);
					g.drawRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					break;
				}
				default:
				{
					g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					g.setDrawColor(hovered ? highlight : border);
					g.drawRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
					if (selected)
					{
						g.fillRectangle(getBounds().x + ((getBounds().width / 2) - adjustedWidth / 2), getBounds().y + ((getBounds().height / 2) - adjustedHeight / 2),
								adjustedWidth, adjustedHeight);
					}
					break;
				}
			}
			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	public RadioButton setGroup(RadioGroup group)
	{
		if (this.group != null && group != this.group)
		{
			this.group.removeGroupMember(this);
			if (group != null)
			{
				group.addGroupMember(this);
			}
		}
		this.group = group;
		return this;
	}

	public RadioGroup getGroup()
	{
		return group;
	}

	public boolean isSelected()
	{
		return selected;
	}

	public RadioButton setSelected(boolean selected)
	{
		this.selected = selected;
		return this;
	}

	public RadioButton setButtonShape(buttonShape shape)
	{
		this.shape = shape;
		return this;
	}

	public RadioButton setBackgroundColor(Color background)
	{
		this.background = background == null ? Color.LIGHT_BLUE : background;
		return this;
	}

	public RadioButton setBorderColor(Color border)
	{
		this.border = border == null ? Color.BLACK : border;
		return this;
	}

	public RadioButton setHighlightColor(Color highlight)
	{
		this.highlight = highlight == null ? Color.WHITE : highlight;
		return this;
	}

	public RadioButton getAsRadioButton()
	{
		return (RadioButton) this;
	}

	public enum buttonShape
	{
		circle,
		box,
		roundedBox,
		toggle;
	}
}
