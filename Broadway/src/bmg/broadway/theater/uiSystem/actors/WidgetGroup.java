package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Group;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.emef.environment.Environment;
import bmg.emef.math.Vector2;

/**
 * UI Scene Node that is a collection of Widget Actors.
 * <P>
 * participates in layout
 * 
 * @author Matthew Newbury
 *
 */
public class WidgetGroup extends Group implements Layout
{
	private boolean needsLayout = true;
	private boolean fillParent = false;

	@Override
	public void add(Actor... actors)
	{
		invalidate();
		super.add(actors);
	}

	@Override
	public void remove(Actor... actors)
	{
		invalidate();
		super.remove(actors);
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();
		super.draw(batch, delta);
	}

	@Override
	public Actor hit(Vector2 sceneCoords)
	{
		Vector2 tVec = Vector2.REUSABLE_STACK.pop().set(sceneCoords);
		if (getBounds().contains(getTransform().unproject(tVec)))
		{
			Actor hit = null;
			for (Actor a : members)
			{
				tVec.set(sceneCoords);
				hit = a.hit(tVec);
				if (hit != null)
				{
					break;
				}
			}
			return hit == null ? this : hit;
		}
		Vector2.REUSABLE_STACK.push(tVec);
		return null;
	}

	public WidgetGroup getAsWidgetGroup()
	{
		return (WidgetGroup) this;
	}

	@Override
	public void validate()
	{
		Actor parent = getParent();
		if (parent != null && fillParent)
		{
			float parentWidth = parent.getWidth();
			float parentHeight = parent.getHeight();
			if (parent == getScene().getRootNode())
			{
				parentWidth = Environment.display.getWidth();
				parentHeight = Environment.display.getHeight();
			}

			if (getWidth() != parentWidth || getHeight() != parentHeight)
			{
				setWidth(parentWidth);
				setHeight(parentHeight);
				invalidate();
			}
		}

		if (needsLayout)
		{
			needsLayout = false;
			doLayout();
		}
	}

	@Override
	public void invalidate()
	{
		needsLayout = true;
	}

	@Override
	public void pack()
	{
		setWidth(getPreferredWidth());
		setHeight(getPreferredHeight());
		validate();
		if (needsLayout)
		{
			setWidth(getPreferredWidth());
			setHeight(getPreferredHeight());
			validate();
		}
	}

	@Override
	public void assignParentActor(Actor actor)
	{
		super.assignParentActor(actor);
		invalidate();
	}

	@Override
	public void doLayout()
	{

	}

	@Override
	public float getPreferredWidth()
	{
		return 0;
	}

	@Override
	public float getPreferredHeight()
	{
		return 0;
	}

	@Override
	public float getMinimumWidth()
	{
		return getPreferredWidth();
	}

	@Override
	public float getMinimumHeight()
	{
		return getPreferredHeight();
	}

	@Override
	public float getMaximumWidth()
	{
		return 0;
	}

	@Override
	public float getMaximumHeight()
	{
		return 0;
	}

	public void setFillParent(boolean shouldFill)
	{
		this.fillParent = shouldFill;
	}
}
