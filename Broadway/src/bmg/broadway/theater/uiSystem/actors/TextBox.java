package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.graphics.TrueTypeFont;
import bmg.broadway.graphics.cameras.Camera;
import bmg.broadway.util.Timer;
import bmg.emef.Input.Keyboard;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;
import bmg.emef.math.Vector3;

public class TextBox extends Widget
{
	private Timer blink;
	private TrueTypeFont font = TrueTypeFont.DEFAULT;

	private Color background = Color.LIGHT_BLUE;
	private Color textColor = Color.BLACK;
	private Color highlighColor = Color.WHITE;
	private Color borderColor = Color.BLACK;

	private String text = "";
	private int cursor = 0;
	private float cursorPos = 0f;
	private float border = 5f;

	private boolean showCursor = true;
	private boolean hasKeyboardFocus = false;

	public TextBox()
	{
		setPreferredSize(100, 25);
		blink = new Timer(0.5f);
		blink.start();
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		if (Mouse.isButtonDown(Mouse.ButtonCode.BUTTON_1))
		{
			hasKeyboardFocus = getScene().getMouseOverActor() == this;

			if (hasKeyboardFocus)
			{
				Vector3 tVec = Vector3.REUSABLE_STACK.pop();
				Camera.CURRENT.unprojectScreenPoint(Mouse.getPositionX(), Mouse.getPositionY(), tVec);
				getTransform().unproject(tVec);

				if (tVec.x > getBounds().x + border + cursorPos)
				{
					while (getBounds().x + border + cursorPos < tVec.x)
					{
						if (cursor == text.length())
						{
							break;
						}
						float w = font.getCharWidth(text.charAt(cursor));
						if (getBounds().x + border + cursorPos + w < getBounds().x + getBounds().width - border)
						{
							cursorPos += w;
							cursor++;
						}
						else
						{
							cursorPos = getBounds().width - border - border;
							break;
						}
					}
				}
				else if (tVec.x < getBounds().x + border + cursorPos)
				{
					while (getBounds().x + border + cursorPos > tVec.x)
					{
						if (cursor == 0)
						{
							break;
						}
						float w = font.getCharWidth(text.charAt(cursor - 1));
						if (getBounds().x + border + cursorPos - w > getBounds().x + border)
						{
							cursorPos -= w;
							cursor--;
						}
						else
						{
							cursorPos = 0;
							break;
						}
					}
				}
				Mouse.consume(Mouse.ButtonCode.BUTTON_1);
				Vector3.REUSABLE_STACK.push(tVec);
			}
		}

		if (hasKeyboardFocus)
		{
			if (Keyboard.isKeyDown(Keyboard.QWERTY.KEY_ESCAPE))
			{
				Keyboard.consume(Keyboard.QWERTY.KEY_ESCAPE);
				hasKeyboardFocus = false;
			}

			blink.update(delta);

			if (blink.isDone())
			{
				showCursor = !showCursor;
				blink.reset();
				blink.start();
			}

			for (Keyboard.QWERTY key : Keyboard.getTypedKeys())
			{
				// delete key handling
				if (key.equals(Keyboard.QWERTY.KEY_DELETE))
				{
					if (cursor == text.length())
					{
						continue;
					}

					if (cursor == 0)
					{
						text = text.substring(cursor + 1);
					}
					else
					{
						text = text.substring(0, cursor) + text.substring(cursor + 1);
					}

					Keyboard.consume(key);
				}

				// backspace key handling
				if (key.equals(Keyboard.QWERTY.KEY_BACKSPACE))
				{
					if (cursor == 0)
					{
						continue;
					}

					float w = font.getCharWidth(text.charAt(cursor - 1));
					if (cursor == text.length())
					{
						text = text.substring(0, cursor - 1);
					}
					else
					{
						text = text.substring(0, cursor - 1) + text.substring(cursor);
					}

					cursor--;

					if (border + cursorPos - w > border)
					{
						cursorPos -= w;
					}
					else
					{
						cursorPos = 0;
					}

					Keyboard.consume(key);
				}

				// left arrow key handling
				if (key.equals(Keyboard.QWERTY.KEY_LEFT))
				{
					if (cursor == 0)
					{
						continue;
					}

					float w = font.getCharWidth(text.charAt(cursor - 1));
					cursor--;
					if (border + cursorPos - w > border)
					{
						cursorPos -= w;
					}
					else
					{
						cursorPos = 0;
					}

					Keyboard.consume(key);
				}

				// right arrow key handling
				if (key.equals(Keyboard.QWERTY.KEY_RIGHT))
				{
					if (cursor == text.length())
					{
						continue;
					}

					float w = font.getCharWidth(text.charAt(cursor));
					cursor++;
					if (border + cursorPos + w < getBounds().width - border - border)
					{
						cursorPos += w;
					}
					else
					{
						cursorPos = getBounds().width - border - border;
					}

					Keyboard.consume(key);
				}

				// everything else
				char character = key.primaryCharacter();
				if (Keyboard.isKeyDown(Keyboard.QWERTY.KEY_LEFT_SHIFT) || Keyboard.isKeyDown(Keyboard.QWERTY.KEY_RIGHT_SHIFT))
				{
					character = key.secondaryCharacter();
				}

				// ignore keys with no character and disregard the new line character
				if (character != Keyboard.QWERTY.KEY_NONE.primaryCharacter() && character != Keyboard.QWERTY.KEY_ENTER.primaryCharacter())
				{
					float w = font.getCharWidth(character);
					String temp = text.substring(0, cursor);
					temp += character;
					temp += text.substring(cursor);
					text = temp;
					cursor++;
					if (border + cursorPos + w < getBounds().width - border - border)
					{
						cursorPos += w;
					}
					else
					{
						cursorPos = getBounds().width - border - border;
					}

					Keyboard.consume(key);
				}
			}
		}
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();

		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			g.setDrawColor(background);
			g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			g.setDrawColor(hasKeyboardFocus ? highlighColor : borderColor);
			g.drawRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			g.setDrawColor(textColor);
			if (showCursor && hasKeyboardFocus)
			{
				g.drawLine(getBounds().x + border + cursorPos, getBounds().y, getBounds().x + border + cursorPos, getBounds().y + getBounds().height);
			}
			g.setFont(font);
			g.drawRightString(getBounds().x + border + cursorPos, getBounds().y, -1, -1, text.substring(0, cursor));
			g.drawString(getBounds().x + border + cursorPos, getBounds().y, -1, -1, text.substring(cursor));
			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	public String getText()
	{
		return text;
	}

	public TextBox setText(String text)
	{
		this.text = text == null ? "" : text;
		return this;
	}

	public boolean hasKeyboardFocus()
	{
		return hasKeyboardFocus;
	}

	public TextBox setFont(TrueTypeFont font)
	{
		this.font = font == null ? TrueTypeFont.DEFAULT : font;
		return this;
	}

	public TextBox setBackground(Color background)
	{
		this.background = background == null ? Color.LIGHT_BLUE : background;
		return this;
	}

	public TextBox setHighlightColor(Color color)
	{
		this.highlighColor = color == null ? Color.WHITE : color;
		return this;
	}

	public TextBox setTextColor(Color color)
	{
		this.textColor = color == null ? Color.BLACK : color;
		return this;
	}

	public TextBox setBorderColor(Color borderColor)
	{
		this.borderColor = borderColor == null ? Color.BLACK : borderColor;
		return this;
	}

	public TextBox getAsTextBox()
	{
		return (TextBox) this;
	}
}
