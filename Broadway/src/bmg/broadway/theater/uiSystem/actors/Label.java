package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.graphics.TrueTypeFont;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;

public class Label extends Widget
{
	private String text = "";
	private Color color = Color.BLACK;
	private TrueTypeFont font = TrueTypeFont.DEFAULT;

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();
		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			g.setDrawColor(color);
			g.setFont(font);
			g.drawCenteredString(getBounds().x, getBounds().y, getBounds().width, getBounds().height, text);
			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	public Label setText(String text)
	{
		this.text = text == null ? "" : text;
		setPreferredSize(font.getWidth(text) + 5f, font.getHeight(text));
		invalidate();
		return this;
	}

	public String getText()
	{
		return text;
	}

	public Label setColor(Color color)
	{
		this.color = color == null ? Color.BLACK : color;
		return this;
	}

	public Label setFont(TrueTypeFont font)
	{
		this.font = font == null ? TrueTypeFont.DEFAULT : font;
		return this;
	}

	public Label getAsLabel()
	{
		return (Label) this;
	}
}
