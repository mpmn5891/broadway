package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.emef.util.MathUtils;

/**
 * UI Scene Node that holds a collection of widget actors and displays them in a vertical stack;
 * 
 * @author Matthew Newbury
 *
 */
public class WidgetStack extends WidgetGroup
{
	private float preferredWidth, preferredHeight;
	private float minimumWidth, minimumHeight;
	private float maximumWidth, maximumHeight;
	private Alignment align = Alignment.CENTER;

	public WidgetStack getAsWidgetStack()
	{
		return (WidgetStack) this;
	}

	@Override
	public void pack()
	{
		minimumHeight = 0;
		preferredHeight = 0;
		maximumHeight = 0;
		for (Actor actor : members)
		{

			preferredWidth = MathUtils.max(preferredWidth, actor.getWidth());
			minimumWidth = MathUtils.max(minimumWidth, actor.getWidth());
			maximumWidth = MathUtils.max(maximumWidth, actor.getWidth());
			preferredHeight += actor.getHeight();
			minimumHeight += actor.getHeight();
			maximumHeight += actor.getHeight();

			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.invalidate();
				layout.pack();

				minimumWidth = MathUtils.max(minimumWidth, layout.getMinimumWidth());
				preferredWidth = MathUtils.max(preferredWidth, layout.getPreferredWidth());
				maximumWidth = MathUtils.max(maximumWidth, layout.getMaximumWidth());
				minimumHeight += layout.getMinimumHeight();
				preferredHeight += layout.getPreferredHeight();
				maximumHeight += layout.getMaximumHeight();
			}

			if (actor instanceof Widget)
			{
				Widget widget = (Widget) actor;
				minimumWidth = MathUtils.max(minimumWidth, widget.getMinimumWidth());
				preferredWidth = MathUtils.max(preferredWidth, widget.getPreferredWidth());
				maximumWidth = MathUtils.max(maximumWidth, widget.getMaximumWidth());
				minimumHeight += widget.getMinimumHeight();
				preferredHeight += widget.getTopPadding() + widget.getPreferredHeight() + widget.getBottomPadding();
				maximumHeight += widget.getTopPadding() + widget.getMaximumHeight() + widget.getBottomPadding();
			}
		}

		super.pack();
	}

	@Override
	public void doLayout()
	{
		float x = 0;
		float y = 1;
		float centerLine = getWidth() / 2;
		float lastBottomPadding = -1;

		for (Actor actor : members)
		{
			float tp = 0;
			float bp = 0;

			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.pack();
			}

			if (actor instanceof Widget)
			{
				Widget widget = (Widget) actor;
				tp = widget.getTopPadding();
				bp = widget.getBottomPadding();
			}

			if (lastBottomPadding != -1)
			{
				lastBottomPadding = MathUtils.max(lastBottomPadding, tp);
				y += lastBottomPadding;
			}

			switch (align)
			{
				case LEFT:
				{
					x = 0;
					break;
				}
				case CENTER:
				{
					x = centerLine - (actor.getWidth() / 2);
					break;
				}
				case RIGHT:
				{
					x = getWidth() - actor.getWidth();
					break;
				}
			}

			actor.getTransform().setPosition(x, y);

			y += actor.getHeight() + 1;

			lastBottomPadding = bp;
		}

		setHeight(y);
	}

	public WidgetStack setAlignment(Alignment align)
	{
		this.align = align;
		return this;
	}

	@Override
	public float getPreferredWidth()
	{
		return preferredWidth;
	}

	@Override
	public float getPreferredHeight()
	{
		return preferredHeight;
	}

	@Override
	public float getMinimumWidth()
	{
		return minimumWidth;
	}

	@Override
	public float getMinimumHeight()
	{
		return minimumHeight;
	}

	@Override
	public float getMaximumWidth()
	{
		return maximumWidth;
	}

	@Override
	public float getMaximumHeight()
	{
		return maximumHeight;
	}

	public enum Alignment
	{
		LEFT,
		CENTER,
		RIGHT;
	}
}
