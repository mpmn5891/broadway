package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.graphics.TrueTypeFont;
import bmg.broadway.graphics.cameras.Camera;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;
import bmg.emef.math.Vector3;

public class Slide extends Widget
{
	private TrueTypeFont font = TrueTypeFont.DEFAULT;

	private Color background = Color.LIGHT_BLUE;
	private Color borderColor = Color.BLACK;
	private Color highlight = Color.WHITE;

	private float min = 0, max = 1, val = 0;
	private float sliderPos = 5f;
	private float border = 5f;

	private boolean hovered = false;

	public Slide()
	{
		setPreferredSize(100, 25);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);

		hovered = getScene().getMouseOverActor() == this;

		if (hovered)
		{
			if (Mouse.isButtonDown(Mouse.ButtonCode.BUTTON_1))
			{
				Vector3 tVec = Vector3.REUSABLE_STACK.pop();
				Camera.CURRENT.unprojectScreenPoint(Mouse.getPositionX(), Mouse.getPositionY(), tVec);
				getTransform().unproject(tVec);

				sliderPos = tVec.x;
				if (sliderPos > getBounds().x + getBounds().width - border)
				{
					sliderPos = getBounds().x + getBounds().width - border;
				}
				else if (sliderPos < getBounds().x + border)
				{
					sliderPos = getBounds().x + border;
				}
				val = max + (((getBounds().x + border + sliderPos) * (max - min)) / ((getBounds().width - border) - (getBounds().x + border)));
				Vector3.REUSABLE_STACK.push(tVec);
			}
		}
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();

		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			g.setDrawColor(background);
			g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			g.setDrawColor(borderColor);
			g.drawRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			if (hovered)
			{
				g.setDrawColor(highlight);
			}
			g.fillRectangle(sliderPos - border, getBounds().y, border + border, getBounds().y + getBounds().height);
			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	public float getMinimumValue()
	{
		return min;
	}

	public Slide setMinimumValue(float min)
	{
		this.min = min;
		return this;
	}

	public float getMaximumValue()
	{
		return max;
	}

	public Slide setMaximumValue(float max)
	{
		this.max = max;
		return this;
	}

	public float getValue()
	{
		return val;
	}

	public Slide setValue(float val)
	{
		this.val = val;
		return this;
	}

	public boolean isHovered()
	{
		return hovered;
	}

	public Slide setFont(TrueTypeFont font)
	{
		this.font = font;
		return this;
	}

	public Slide setBackgroundColor(Color background)
	{
		this.background = background;
		return this;
	}

	public Slide setBorderColor(Color borderColor)
	{
		this.borderColor = borderColor;
		return this;
	}

	public Slide setHighlightColor(Color highlight)
	{
		this.highlight = highlight;
		return this;
	}

	public Slide getAsSlide()
	{
		return (Slide) this;
	}
}
