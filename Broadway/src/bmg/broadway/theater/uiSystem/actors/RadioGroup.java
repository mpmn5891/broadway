package bmg.broadway.theater.uiSystem.actors;

import java.util.ArrayList;
import java.util.List;

public class RadioGroup
{
	private List<RadioButton> radios;
	private List<Behavior> behaviors;

	public RadioGroup()
	{
		radios = new ArrayList<>();
		behaviors = new ArrayList<>();
		behaviors.add(Behavior.SINGLE_SELECT);
	}

	public RadioGroup(RadioButton... buttons)
	{
		this();
		for (RadioButton button : buttons)
		{
			addGroupMember(button);
		}
	}

	protected void notify(RadioButton notifier)
	{
		if (behaviors.contains(Behavior.SINGLE_SELECT))
		{
			if (notifier.isSelected())
			{
				for (RadioButton button : radios)
				{
					if (button != notifier)
					{
						button.setSelected(false);
					}
				}
			}
			if (behaviors.contains(Behavior.FORCE_SELECTION))
			{
				boolean oneSelected = false;
				for (RadioButton button : radios)
				{
					if (button.isSelected())
					{
						oneSelected = true;
					}
				}
				if (!oneSelected)
				{
					notifier.setSelected(true);
				}
			}
		}
	}

	private void validate()
	{
		if (radios.isEmpty())
		{
			return;
		}
		if (behaviors.contains(Behavior.FORCE_SELECTION))
		{
			boolean oneSelected = false;
			for (RadioButton button : radios)
			{
				if (button.isSelected())
				{
					oneSelected = true;
				}
			}
			if (!oneSelected)
			{
				radios.get(0).setSelected(true);
			}
		}
	}

	public void addGroupMember(RadioButton button)
	{
		if (button != null)
		{
			radios.add(button);
			button.setGroup(this);
			validate();
		}
	}

	public void removeGroupMember(RadioButton button)
	{
		if (button != null)
		{
			radios.remove(button);
			button.setGroup(null);
		}
	}

	public void addBehavior(Behavior behavior)
	{
		if (behavior != null)
		{
			if (behavior == Behavior.SINGLE_SELECT)
			{
				behaviors.remove(Behavior.MULTI_SELECT);
			}
			if (behavior == Behavior.MULTI_SELECT)
			{
				behaviors.remove(Behavior.SINGLE_SELECT);
			}
			behaviors.add(behavior);
			validate();
		}
	}

	public void removeBehavior(Behavior behavior)
	{
		if (behavior != null)
		{
			behaviors.remove(behavior);
			validate();
		}
	}

	public List<RadioButton> getGroupMembersAsList(List<RadioButton> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}
		for (RadioButton button : radios)
		{
			dest.add(button);
		}
		return dest;
	}

	public enum Behavior
	{
		MULTI_SELECT,
		SINGLE_SELECT,
		FORCE_SELECTION;
	}
}
