package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.geometry.Rectangle;
import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.graphics.cameras.Camera;
import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.broadway.theater.uiSystem.UIScene;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;
import bmg.emef.util.MathUtils;

public class ScrollPane extends Widget
{
	private Color backgroundColor = Color.WHITE;
	private Color borderColor = Color.BLACK;
	private Color scrollBarBackgroundColor = Color.LIGHT_GRAY;
	private Color scrollBarColor = Color.GRAY;
	private Color highlightColor = Color.DARK_GRAY;

	private float borderPadding = 10;

	private boolean drawBorder = true;
	private boolean fillBackground = true;

	private Actor actor = null;

	private float hScrollAmount = 0;
	private float vScrollAmount = 0;

	private Rectangle hScrollArea = new Rectangle();
	private Rectangle hScrollButtonLeft = new Rectangle();
	private Rectangle hScrollButtonRight = new Rectangle();
	private Rectangle hScrollSlide = new Rectangle();

	private Rectangle vScrollArea = new Rectangle();
	private Rectangle vScrollButtonTop = new Rectangle();
	private Rectangle vScrollButtonBottom = new Rectangle();
	private Rectangle vScrollSlide = new Rectangle();

	private Rectangle actorArea = new Rectangle();

	private boolean scrollWidth = false;
	private boolean scrollHeight = false;

	private boolean overTop = false, overBottom = false, overLeft = false, overRight = false, overScrollV = false, overScrollH = false;

	public ScrollPane()
	{
		setPreferredSize(300, 300);
	}

	public ScrollPane(Actor actor)
	{
		this();
		setActor(actor);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		if (actor != null)
		{
			actor.act(delta);
		}

		Vector3 tVec = Vector3.REUSABLE_STACK.pop();
		Camera.CURRENT.unprojectScreenPoint(Mouse.getPositionX(), Mouse.getPositionY(), tVec);
		getTransform().unproject(tVec);

		overTop = vScrollButtonTop.contains(tVec.x, tVec.y);
		overBottom = vScrollButtonBottom.contains(tVec.x, tVec.y);
		overLeft = hScrollButtonLeft.contains(tVec.x, tVec.y);
		overRight = hScrollButtonRight.contains(tVec.x, tVec.y);
		overScrollV = vScrollArea.contains(tVec.x, tVec.y);
		overScrollH = hScrollArea.contains(tVec.x, tVec.y);

		if (actor != null && Mouse.isButtonDown(Mouse.ButtonCode.BUTTON_1))
		{
			float factor = 50;
			if (overTop && scrollHeight)
			{
				actor.getTransform().translate(0, factor * delta);
				if (actor.getTransform().getPosition().y > borderPadding)
				{
					actor.getTransform().setPosition(actor.getTransform().getPosition().x, borderPadding);
				}

				vScrollSlide.y = vScrollArea.y - (((actor.getTransform().getPosition().y - borderPadding) * (vScrollArea.height - vScrollSlide.height)) / vScrollAmount);
			}
			else if (overBottom && scrollHeight)
			{
				actor.getTransform().translate(0, -factor * delta);
				if (actor.getTransform().getPosition().y + actor.getHeight() < actorArea.y + actorArea.height)
				{
					actor.getTransform().setPosition(actor.getTransform().getPosition().x, actorArea.y + actorArea.height - actor.getHeight());
				}

				vScrollSlide.y = vScrollArea.y - (((actor.getTransform().getPosition().y - borderPadding) * (vScrollArea.height - vScrollSlide.height)) / vScrollAmount);
			}
			else if (overLeft && scrollWidth)
			{
				actor.getTransform().translate(factor * delta, 0);
				if (actor.getTransform().getPosition().x > borderPadding)
				{
					actor.getTransform().setPosition(borderPadding, actor.getTransform().getPosition().y);
				}

				hScrollSlide.x = hScrollArea.x - (((actor.getTransform().getPosition().x - borderPadding) * (hScrollArea.width - hScrollSlide.width)) / hScrollAmount);
			}
			else if (overRight && scrollWidth)
			{
				actor.getTransform().translate(-factor * delta, 0);
				if (actor.getTransform().getPosition().x + actor.getWidth() < actorArea.x + actorArea.width)
				{
					actor.getTransform().setPosition(actorArea.x + actorArea.width - actor.getWidth(), actor.getTransform().getPosition().y);
				}

				hScrollSlide.x = hScrollArea.x - (((actor.getTransform().getPosition().x - borderPadding) * (hScrollArea.width - hScrollSlide.width)) / hScrollAmount);
			}
			else if (overScrollV && scrollHeight)
			{
				vScrollSlide.y = tVec.y - (vScrollSlide.height / 2);
				if (vScrollSlide.y < vScrollArea.y)
				{
					vScrollSlide.y = vScrollArea.y;
				}
				if (vScrollSlide.y + vScrollSlide.height > vScrollArea.y + vScrollArea.height)
				{
					vScrollSlide.y = vScrollArea.y + vScrollArea.height - vScrollSlide.height;
				}

				float yoff = ((vScrollSlide.y - vScrollArea.y) * vScrollAmount) / (vScrollArea.height - vScrollSlide.height);

				actor.getTransform().setPosition(actor.getTransform().getPosition().x, borderPadding - yoff);
			}
			else if (overScrollH && scrollWidth)
			{
				hScrollSlide.x = tVec.x - (hScrollSlide.width / 2);
				if (hScrollSlide.x < hScrollArea.x)
				{
					hScrollSlide.x = hScrollArea.x;
				}
				if (hScrollSlide.x + hScrollSlide.width > hScrollArea.x + hScrollArea.width)
				{
					hScrollSlide.x = hScrollArea.x + hScrollArea.width - hScrollSlide.width;
				}

				float xoff = ((hScrollSlide.x - hScrollArea.x) * hScrollAmount) / (hScrollArea.width - hScrollSlide.width);

				actor.getTransform().setPosition(borderPadding - xoff, actor.getTransform().getPosition().y);
			}
		}

		Vector3.REUSABLE_STACK.push(tVec);
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();

		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			if (fillBackground)
			{
				g.setDrawColor(backgroundColor);
				g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			}
			if (actor != null)
			{
				g.setClippingBounds(actorArea);
				actor.draw(batch, delta);
				g.clearClippingBounds();
				g.setTransform(getTransform().getWorldTransform());
			}
			g.setDrawColor(scrollBarBackgroundColor);
			g.fillRectangle(hScrollArea);
			g.fillRectangle(vScrollArea);

			g.setDrawColor(scrollWidth ? overLeft ? highlightColor : scrollBarColor : scrollBarBackgroundColor);
			g.fillRectangle(hScrollButtonLeft);

			g.setDrawColor(scrollWidth ? overRight ? highlightColor : scrollBarColor : scrollBarBackgroundColor);
			g.fillRectangle(hScrollButtonRight);

			g.setDrawColor(scrollHeight ? overTop ? highlightColor : scrollBarColor : scrollBarBackgroundColor);
			g.fillRectangle(vScrollButtonTop);

			g.setDrawColor(scrollHeight ? overBottom ? highlightColor : scrollBarColor : scrollBarBackgroundColor);
			g.fillRectangle(vScrollButtonBottom);

			if (scrollWidth)
			{
				g.setDrawColor(overScrollH ? highlightColor : scrollBarColor);
				g.fillRectangle(hScrollSlide);
			}
			if (scrollHeight)
			{
				g.setDrawColor(overScrollV ? highlightColor : scrollBarColor);
				g.fillRectangle(vScrollSlide);
			}
			if (drawBorder)
			{
				g.setDrawColor(borderColor);
				g.drawRectangle(actorArea.x, actorArea.y, actorArea.width, actorArea.height);
			}

			g.setDrawColor(scrollBarBackgroundColor);
			g.drawLine(vScrollButtonTop.x + (vScrollButtonTop.width / 4), vScrollButtonTop.y + vScrollButtonTop.height - (vScrollButtonTop.height / 4),
					vScrollButtonTop.x + (vScrollButtonTop.width / 2), vScrollButtonTop.y + (vScrollButtonTop.height / 4));
			g.drawLine(vScrollButtonTop.x + vScrollButtonTop.width - (vScrollButtonTop.width / 4), vScrollButtonTop.y + vScrollButtonTop.height - (vScrollButtonTop.height / 4),
					vScrollButtonTop.x + vScrollButtonTop.width - (vScrollButtonTop.width / 2), vScrollButtonTop.y + (vScrollButtonTop.height / 4));

			g.drawLine(vScrollButtonBottom.x + (vScrollButtonBottom.width / 4), vScrollButtonBottom.y + (vScrollButtonBottom.height / 4),
					vScrollButtonBottom.x + (vScrollButtonBottom.width / 2), vScrollButtonBottom.y + vScrollButtonBottom.height - (vScrollButtonBottom.height / 4));
			g.drawLine(vScrollButtonBottom.x + vScrollButtonBottom.width - (vScrollButtonBottom.width / 4), vScrollButtonBottom.y + (vScrollButtonBottom.height / 4),
					vScrollButtonBottom.x + +vScrollButtonBottom.width - (vScrollButtonBottom.width / 2),
					vScrollButtonBottom.y + vScrollButtonBottom.height - (vScrollButtonBottom.height / 4));

			g.drawLine(hScrollButtonLeft.x + (hScrollButtonLeft.width / 4), hScrollButtonLeft.y + (hScrollButtonLeft.height / 2),
					hScrollButtonLeft.x + hScrollButtonLeft.width - (hScrollButtonLeft.width / 4), hScrollButtonLeft.y + hScrollButtonLeft.height - (hScrollButtonLeft.height / 4));
			g.drawLine(hScrollButtonLeft.x + (hScrollButtonLeft.width / 4), hScrollButtonLeft.y + (hScrollButtonLeft.height / 2),
					hScrollButtonLeft.x + hScrollButtonLeft.width - (hScrollButtonLeft.width / 4), hScrollButtonLeft.y + (hScrollButtonLeft.height / 4));

			g.drawLine(hScrollButtonRight.x + (hScrollButtonRight.width / 4), hScrollButtonRight.y + (hScrollButtonRight.height / 4),
					hScrollButtonRight.x + hScrollButtonRight.width - (hScrollButtonRight.width / 4), hScrollButtonRight.y + (hScrollButtonRight.height / 2));
			g.drawLine(hScrollButtonRight.x + (hScrollButtonRight.width / 4), hScrollButtonRight.y + hScrollButtonRight.height - (hScrollButtonRight.height / 4),
					hScrollButtonRight.x + hScrollButtonRight.width - (hScrollButtonRight.width / 4), hScrollButtonRight.y + (hScrollButtonRight.height / 2));

			g.clearClippingBounds();

			if (Environment.Analytics.development)
			{
				g.setDrawColor(Color.GREEN);
				g.drawRectangle(getBounds().x - getLeftPadding(), getBounds().y - getTopPadding(),
						getLeftPadding() + getBounds().width + getRightPadding(), getTopPadding() + getBounds().height + getBottomPadding());
			}
		}
	}

	@Override
	public void doLayout()
	{
		float actorWidth = 0;
		float actorHeight = 0;

		if (actor != null)
		{
			actor.getTransform().setPosition(borderPadding, borderPadding);

			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.invalidate();
				layout.pack();
			}

			actorWidth = actor.getWidth();
			actorHeight = actor.getHeight();
		}

		actorArea.set(getBounds().x + borderPadding, getBounds().y + borderPadding, getWidth() - (borderPadding + borderPadding + MathUtils.max(10, borderPadding)),
				getHeight() - (borderPadding + borderPadding + MathUtils.max(10, borderPadding)));
		vScrollArea.set(getBounds().x + (getWidth() - MathUtils.max(10, borderPadding)), getBounds().y + MathUtils.max(10, borderPadding), MathUtils.max(10, borderPadding),
				getHeight() - (MathUtils.max(10, borderPadding) * 3));
		vScrollButtonTop.set(vScrollArea.x, getBounds().y, vScrollArea.width, vScrollArea.width);
		vScrollButtonBottom.set(vScrollArea.x, vScrollArea.y + vScrollArea.height, vScrollArea.width, vScrollArea.width);
		vScrollSlide.set(vScrollArea.x, vScrollArea.y, vScrollArea.width, 50);

		hScrollArea.set(getBounds().x + MathUtils.max(10, borderPadding), getBounds().y + (getHeight() - MathUtils.max(10, borderPadding)),
				getWidth() - (MathUtils.max(10, borderPadding) * 3), MathUtils.max(10, borderPadding));
		hScrollButtonLeft.set(getBounds().x, hScrollArea.y, hScrollArea.height, hScrollArea.height);
		hScrollButtonRight.set(hScrollArea.x + hScrollArea.width, hScrollArea.y, hScrollArea.height, hScrollArea.height);
		hScrollSlide.set(hScrollArea.x, hScrollArea.y, 50, hScrollArea.height);

		scrollWidth = actor == null ? false : actor.getWidth() > actorArea.width;
		scrollHeight = actor == null ? false : actor.getHeight() > actorArea.height;

		hScrollAmount = actorWidth - actorArea.width;
		vScrollAmount = actorHeight - actorArea.height;
	}

	@Override
	public Actor hit(Vector2 sceneCoords)
	{
		Vector2 tVec = Vector2.REUSABLE_STACK.pop().set(sceneCoords);
		Actor hit = null;
		if (actor != null && actorArea.contains(sceneCoords))
		{
			hit = actor.hit(tVec);
		}
		return hit == null ? super.hit(sceneCoords) : hit;
	}

	public void setActor(Actor actor)
	{
		invalidate();
		if (actor != null)
		{
			actor.setScene(null);
			actor.assignParentActor(null);
		}
		this.actor = actor;
		if (actor != null)
		{
			actor.setScene(this.getScene());
			actor.assignParentActor(this);
		}
	}

	public void clear()
	{
		invalidate();
		if (actor != null)
		{
			actor.setScene(null);
			actor.assignParentActor(null);
		}
		actor = null;
	}

	@Override
	public void setScene(UIScene scene)
	{
		super.setScene(scene);
		if (actor != null)
		{
			actor.setScene(scene);
		}
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		if (actor != null)
		{
			actor.setEnabled(enabled);
		}
	}

	@Override
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
		if (actor != null)
		{
			actor.setVisible(visible);
		}
	}

	public ScrollPane setBackgroundColor(Color backgroundColor)
	{
		this.backgroundColor = backgroundColor;
		return this;
	}

	public ScrollPane setBorderColor(Color borderColor)
	{
		this.borderColor = borderColor;
		return this;
	}

	public ScrollPane setScrollBarBackgroundColor(Color scrollBarBackgroundColor)
	{
		this.scrollBarBackgroundColor = scrollBarBackgroundColor;
		return this;
	}

	public ScrollPane setScrollBarColor(Color scrollBarColor)
	{
		this.scrollBarColor = scrollBarColor;
		return this;
	}

	public ScrollPane setHighlightColor(Color highlightColor)
	{
		this.highlightColor = highlightColor;
		return this;
	}

	public Actor getActor()
	{
		return actor;
	}

	public ScrollPane getAsScrollPane()
	{
		return (ScrollPane) this;
	}
}
