package bmg.broadway.theater.uiSystem.actors;

import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.Graphics2d;
import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.Layout;
import bmg.emef.graphics.Color;
import bmg.emef.util.MathUtils;

public class Pane extends WidgetGroup
{
	protected float preferredWidth, preferredHeight;
	protected float minimumWidth, minimumHeight;
	protected float maximumWidth, maximumHeight;

	protected float leftPadding, topPadding, rightPadding, bottomPadding;

	protected Color backgroundColor = Color.WHITE;
	protected Color borderColor = Color.BLACK;

	protected boolean drawBorder = true;
	protected boolean fillBackground = true;

	public Pane getAsPane()
	{
		return (Pane) this;
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		validate();
		if (isVisible())
		{
			Graphics2d g = batch.createGraphics2d();
			g.setTransform(getTransform().getWorldTransform());
			g.setClippingBounds(getBounds());
			if (fillBackground)
			{
				g.setDrawColor(backgroundColor);
				g.fillRectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height);
			}
			g.setClippingBounds(getBounds().x + leftPadding, getBounds().y + topPadding, getBounds().width - leftPadding - rightPadding,
					getBounds().height - topPadding - bottomPadding);
			for (int i = 0; i < members.size(); i++)
			{
				members.get(i).draw(batch, delta);
			}
			g.setTransform(getTransform().getWorldTransform());
			if (drawBorder)
			{
				g.setDrawColor(borderColor);
				g.drawRectangle(getBounds().x + leftPadding, getBounds().y + topPadding, getBounds().width - leftPadding - rightPadding,
						getBounds().height - topPadding - bottomPadding);
			}
			g.clearClippingBounds();
			g.clearClippingBounds();
		}
	}

	@Override
	public void doLayout()
	{
		float x = leftPadding + 1;
		float y = topPadding + 1;
		float lastRightPadding = -1;
		float lastTopPadding = -1;
		float nextTopPadding = -1;
		float nextHeight = 0;

		for (Actor actor : members)
		{
			float lp = 0;
			float rp = 0;
			float tp = 0;
			float bp = 0;
			float localy = 0;

			if (actor instanceof Layout)
			{
				Layout layout = (Layout) actor;
				layout.invalidate();
				layout.pack();
			}

			if (actor instanceof Widget)
			{
				Widget widget = (Widget) actor;
				lp = widget.getLeftPadding();
				rp = widget.getRightPadding();
				tp = widget.getTopPadding();
				bp = widget.getBottomPadding();
			}

			if (lastRightPadding != -1)
			{
				lastRightPadding = MathUtils.max(lastRightPadding, lp);
				x += lastRightPadding;
			}

			if (x + actor.getWidth() > getWidth() - rightPadding)
			{
				if (x + actor.getWidth() < getMaximumWidth() - rightPadding || getMaximumWidth() == 0)
				{
					setWidth(getWidth() + ((x + actor.getWidth() + 1) - (getWidth() - rightPadding)));
				}
			}

			if (x + actor.getWidth() > getWidth() - rightPadding)
			{
				x = leftPadding + 1;
				lastRightPadding = -1;
				y += nextHeight + 1;
				nextHeight = 0;
				lastTopPadding = nextTopPadding;
				nextTopPadding = 0;
			}

			if (lastTopPadding != -1)
			{
				localy = MathUtils.max(lastTopPadding, tp);
			}

			if (y + localy + actor.getHeight() > getHeight() - bottomPadding)
			{
				if (y + localy + actor.getHeight() < getMaximumHeight() - bottomPadding || getMaximumHeight() == 0)
				{
					setHeight(getHeight() + ((y + localy + actor.getHeight() + 1) - (getHeight() - bottomPadding)));
				}
			}

			actor.getTransform().setPosition(x, y + localy);
			nextHeight = MathUtils.max(nextHeight, actor.getHeight());
			lastRightPadding = rp;
			nextTopPadding = MathUtils.max(nextTopPadding, bp);

			x += actor.getWidth() + 1;
		}
	}

	@Override
	public float getPreferredWidth()
	{
		return preferredWidth;
	}

	@Override
	public float getPreferredHeight()
	{
		return preferredHeight;
	}

	@Override
	public float getMinimumWidth()
	{
		return minimumWidth;
	}

	@Override
	public float getMinimumHeight()
	{
		return minimumHeight;
	}

	@Override
	public float getMaximumWidth()
	{
		return maximumWidth;
	}

	@Override
	public float getMaximumHeight()
	{
		return maximumHeight;
	}

	public Color getBackgroundColor()
	{
		return backgroundColor;
	}

	public Color getBorderColor()
	{
		return borderColor;
	}

	public boolean isDrawBorder()
	{
		return drawBorder;
	}

	public boolean isFillBackground()
	{
		return fillBackground;
	}

	public Pane setPreferredSize(float width, float height)
	{
		this.preferredWidth = width;
		this.preferredHeight = height;
		return this;
	}

	public Pane setPreferredWidth(float preferredWidth)
	{
		this.preferredWidth = preferredWidth;
		return this;
	}

	public Pane setPreferredHeight(float preferredHeight)
	{
		this.preferredHeight = preferredHeight;
		return this;
	}

	public Pane setMinimumSize(float width, float height)
	{
		this.minimumWidth = width;
		this.minimumHeight = height;
		return this;
	}

	public Pane setMinimumWidth(float minimumWidth)
	{
		this.minimumWidth = minimumWidth;
		return this;
	}

	public Pane setMinimumHeight(float minimumHeight)
	{
		this.minimumHeight = minimumHeight;
		return this;
	}

	public Pane setMaximumSize(float width, float height)
	{
		this.maximumWidth = width;
		this.maximumHeight = height;
		return this;
	}

	public Pane setMaximumWidth(float maximumWidth)
	{
		this.maximumWidth = maximumWidth;
		return this;
	}

	public Pane setMaximumHeight(float maximumHeight)
	{
		this.maximumHeight = maximumHeight;
		return this;
	}

	public Pane setBackgroundColor(Color backgroundColor)
	{
		this.backgroundColor = backgroundColor;
		return this;
	}

	public Pane setBorderColor(Color borderColor)
	{
		this.borderColor = borderColor;
		return this;
	}

	public Pane setDrawBorder(boolean shouldDraw)
	{
		this.drawBorder = shouldDraw;
		return this;
	}

	public Pane setFillBackground(boolean fillBackground)
	{
		this.fillBackground = fillBackground;
		return this;
	}

	public float getLeftPadding()
	{
		return leftPadding;
	}

	public float getTopPadding()
	{
		return topPadding;
	}

	public float getRightPadding()
	{
		return rightPadding;
	}

	public float getBottomPadding()
	{
		return bottomPadding;
	}

	public Pane setPadding(float padding)
	{
		return setPadding(padding, padding);
	}

	public Pane setPadding(float leftRight, float topBottom)
	{
		return setPadding(leftRight, topBottom, leftRight, topBottom);
	}

	public Pane setPadding(float left, float top, float right, float bottom)
	{
		leftPadding = left;
		topPadding = top;
		rightPadding = right;
		bottomPadding = bottom;
		return this;
	}

	public Pane setLeftPadding(float leftPadding)
	{
		this.leftPadding = leftPadding;
		return this;
	}

	public Pane setTopPadding(float topPadding)
	{
		this.topPadding = topPadding;
		return this;
	}

	public Pane setRightPadding(float rightPadding)
	{
		this.rightPadding = rightPadding;
		return this;
	}

	public Pane setBottomPadding(float bottomPadding)
	{
		this.bottomPadding = bottomPadding;
		return this;
	}
}
