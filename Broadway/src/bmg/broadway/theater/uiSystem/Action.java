package bmg.broadway.theater.uiSystem;

public abstract class Action
{
	protected Actor actor;
	protected Actor target;

	public abstract void act(float delta);

	public void setActor(Actor actor)
	{
		this.actor = actor;
		if (target == null)
		{
			setTarget(actor);
		}
	}

	public void setTarget(Actor actor)
	{
		this.target = actor;
		onTargetAssigned(actor);
	}

	public Actor getActor()
	{
		return actor;
	}

	public Actor getTarget()
	{
		return target;
	}

	public void reset()
	{
		actor = null;
		target = null;
		onReset();
	}

	// optional event triggered override methods
	protected void onReset()
	{
		// optional override method
	}

	protected void onTargetAssigned(Actor target)
	{
		// optional override method
	}
}
