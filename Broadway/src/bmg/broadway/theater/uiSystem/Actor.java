package bmg.broadway.theater.uiSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bmg.broadway.geometry.Rectangle;
import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.util.IDGen;
import bmg.emef.environment.Exceptions;
import bmg.emef.math.Vector2;

public class Actor
{
	private UIScene scene;
	private Actor parent;
	private final List<Action> actions = new ArrayList<>();
	private final Map<ActionType, List<Action>> actionsByType = new HashMap<>();
	private final ActorTransform actorTransform = new ActorTransform();
	private final Rectangle bounds = new Rectangle();
	private String name;
	private final long id = IDGen.generate();

	private boolean enabled = true, visible;

	public void setScene(UIScene scene)
	{
		if (this.scene != null)
		{
			onRemoveFromScene(this.scene);
		}
		if (scene != null)
		{
			onAddToScene(scene);
		}
		this.scene = scene;
	}

	public void assignParentActor(Actor actor)
	{
		if (parent != null)
		{
			onParentActorUnassigned(parent);
		}
		if (actor != null)
		{
			onParentActorAssigned(actor);
		}
		actorTransform.setParent(actor == null ? null : actor.getTransform());
		parent = actor;
	}

	public void addAction(Action action)
	{
		if (action == null)
		{
			return;
		}
		actions.add(action);
		ActionType type = ActionType.of(action.getClass());
		List<Action> list = actionsByType.get(type);
		if (list == null)
		{
			actionsByType.put(type, list = new ArrayList<>());
		}
		list.add(action);
		action.setActor(this);
	}

	public void removeAction(Action action)
	{
		if (action == null)
		{
			return;
		}
		action.reset();
		ActionType type = ActionType.of(action.getClass());
		List<Action> list = actionsByType.get(type);
		if (list != null)
		{
			list.remove(action);
		}
		action.reset();
	}

	public void clearActions()
	{
		for (Action a : actions)
		{
			a.reset();
			ActionType type = ActionType.of(a.getClass());
			List<Action> list = actionsByType.get(type);
			if (list != null)
			{
				list.remove(a);
			}
		}
		actions.clear();
		actionsByType.clear();
	}

	public void act(float delta)
	{
		actorTransform.update();
		for (int i = 0; i < actions.size(); i++)
		{
			actions.get(i).act(delta);
		}
	}

	public void draw(ClippingBatch batch, float delta)
	{
		// optional override method
	}

	public Actor hit(Vector2 sceneCoords)
	{
		return bounds.contains(actorTransform.unproject(sceneCoords)) ? this : null;
	}

	@SuppressWarnings("unchecked")
	public <T extends Action> T getActionOfType(Class<T> klass)
	{
		ActionType type = ActionType.of(klass);
		List<Action> list = actionsByType.get(type);
		if (list == null || list.isEmpty())
		{
			return null;
		}
		return (T) list.get(0);
	}

	@SuppressWarnings("unchecked")
	public <T extends Action> List<T> getActionsOfType(Class<T> klass, List<T> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		ActionType type = ActionType.of(klass);
		List<Action> list = actionsByType.get(type);
		if (list != null)
		{
			for (Action a : list)
			{
				dest.add((T) a);
			}
		}
		return dest;
	}

	public <T extends Action> boolean hasAction(Class<T> klass)
	{
		ActionType type = ActionType.of(klass);
		List<Action> list = actionsByType.get(type);
		return !(list == null || list.isEmpty());
	}

	public boolean hasActions()
	{
		return !actions.isEmpty();
	}

	public boolean hasParent()
	{
		return parent != null;
	}

	public boolean isDecendantOf(Actor actor)
	{
		if (actor == null)
		{
			return false;
		}
		Actor parent = this;
		while (parent != null)
		{
			if (parent == actor)
			{
				return true;
			}
			parent = parent.parent;
			if (parent == this)
			{
				throw new Exceptions.Exception(this.getClass().getName() + ".isDecendantOf(" + actor.getClass().getName() + ") :: Circular ancestory detected!");
			}
		}
		return false;
	}

	public boolean isAscendantOf(Actor actor)
	{
		if (actor == null)
		{
			return false;
		}
		Actor parent = actor;
		while (parent != null)
		{
			if (parent == this)
			{
				return true;
			}
			parent = parent.parent;
			if (parent == actor)
			{
				throw new Exceptions.Exception(this.getClass().getName() + ".isAscendantOf(" + actor.getClass().getName() + ") :: Circular ancestory detected!");
			}
		}
		return false;
	}

	public Vector2 sceneToLocalCoords(Vector2 sceneCoords)
	{
		return actorTransform.project(sceneCoords);
	}

	public Vector2 localToSceneCoords(Vector2 localCoords)
	{
		return actorTransform.unproject(localCoords);
	}

	public Actor getAsActor()
	{
		return (Actor) this;
	}

	public UIScene getScene()
	{
		return scene;
	}

	public Actor getParent()
	{
		return parent;
	}

	public ActorTransform getTransform()
	{
		return actorTransform;
	}

	public Rectangle getBounds()
	{
		return bounds;
	}

	public float getWidth()
	{
		return getBounds().width;
	}

	public float getHeight()
	{
		return getBounds().height;
	}

	public void setWidth(float width)
	{
		bounds.width = width;
	}

	public void setHeight(float height)
	{
		bounds.height = height;
	}

	public void setSize(float width, float height)
	{
		setWidth(width);
		setHeight(height);
	}

	public String getName()
	{
		return name;
	}

	public long getId()
	{
		return id;
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	public void destroy()
	{
		onDestroy();
		clearActions();
		assignParentActor(null);
		IDGen.free(id);
	}

	// optional event triggered override methods
	protected void onDestroy()
	{
		// optional override method
	}

	protected void onAddToScene(UIScene scene)
	{
		// optional override method
	}

	protected void onRemoveFromScene(UIScene scene)
	{
		// optional override method
	}

	protected void onParentActorAssigned(Actor parent)
	{
		// optional override method
	}

	protected void onParentActorUnassigned(Actor parent)
	{
		// optional override method
	}
}
