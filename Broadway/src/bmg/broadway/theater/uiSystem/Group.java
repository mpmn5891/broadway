package bmg.broadway.theater.uiSystem;

import java.util.ArrayList;
import java.util.List;

import bmg.broadway.graphics.ClippingBatch;
import bmg.emef.math.Vector2;

public class Group extends Actor
{
	protected final List<Actor> members = new ArrayList<>();

	public void add(Actor... actors)
	{
		for (Actor actor : actors)
		{
			if (actor != null)
			{
				if (members.add(actor))
				{
					actor.setScene(this.getScene());
					actor.assignParentActor(this);
				}

			}
		}
	}

	public void remove(Actor... actors)
	{
		for (Actor actor : actors)
		{
			if (actor != null)
			{
				if (members.remove(actor))
				{
					actor.setScene(null);
					actor.assignParentActor(null);
				}

			}
		}
	}

	public void clear()
	{
		for (Actor a : members)
		{
			a.destroy();
		}
		members.clear();
	}

	@Override
	public void setScene(UIScene scene)
	{
		super.setScene(scene);
		for (Actor a : members)
		{
			a.setScene(scene);
		}
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		for (int i = 0; i < members.size(); i++)
		{
			members.get(i).act(delta);
		}
	}

	@Override
	public void draw(ClippingBatch batch, float delta)
	{
		for (int i = 0; i < members.size(); i++)
		{
			members.get(i).draw(batch, delta);
		}
	}

	@Override
	public Actor hit(Vector2 sceneCoords)
	{
		Vector2 tVec = Vector2.REUSABLE_STACK.pop();
		Actor hit = null;
		for (Actor a : members)
		{
			tVec.set(sceneCoords);
			hit = a.hit(tVec);
			if (hit != null)
			{
				break;
			}
		}
		Vector2.REUSABLE_STACK.push(tVec);
		return hit == null ? super.hit(sceneCoords) : hit;
	}

	public List<Actor> getChildrenAsList(List<Actor> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}
		for (int i = 0; i < members.size(); i++)
		{
			dest.add(members.get(i));
		}
		return dest;
	}

	@SuppressWarnings("unchecked")
	public <T extends Actor> List<T> getChildrenOfTypeAsList(Class<T> klass, List<T> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}

		for (int i = 0; i < members.size(); i++)
		{
			Actor a = members.get(i);
			if (a.getClass() == klass || a.getClass().getSuperclass() == klass)
			{
				dest.add((T) a);
			}
		}

		return dest;
	}

	public boolean hasChildren()
	{
		return !members.isEmpty();
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		for (int i = 0; i < members.size(); i++)
		{
			members.get(i).setEnabled(enabled);
		}
	}

	@Override
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
		for (int i = 0; i < members.size(); i++)
		{
			members.get(i).setVisible(visible);
		}
	}

	public Group getAsGroup()
	{
		return (Group) this;
	}

	@Override
	protected void onDestroy()
	{
		clear();
	}
}
