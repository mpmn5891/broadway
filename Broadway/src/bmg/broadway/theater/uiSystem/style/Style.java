package bmg.broadway.theater.uiSystem.style;

import bmg.broadway.graphics.TrueTypeFont;
import bmg.broadway.theater.uiSystem.Actor;
import bmg.broadway.theater.uiSystem.actors.Button;
import bmg.broadway.theater.uiSystem.actors.Label;
import bmg.broadway.theater.uiSystem.actors.Pane;
import bmg.broadway.theater.uiSystem.actors.RadioButton;
import bmg.broadway.theater.uiSystem.actors.ScrollPane;
import bmg.broadway.theater.uiSystem.actors.Slide;
import bmg.broadway.theater.uiSystem.actors.TextBox;
import bmg.broadway.theater.uiSystem.actors.WidgetGroup;
import bmg.emef.graphics.Color;

public class Style
{
	public static final Color defaultBackgroundColor = new Color(0x1E3544);
	public static final Color defaultForgroundColor = new Color(0x071E2C);
	public static final Color defaultBorderColor = new Color(0x3F5360);
	public static final Color defaultHighlightColor = new Color(0xA1B2BD);
	public static final Color defaultTextColor = new Color(0xFFFFFF);
	public static TrueTypeFont defaultFont = TrueTypeFont.DEFAULT;

	private Color backgroundColor, forgroundColor, borderColor, highlightColor, textColor;
	private TrueTypeFont font;

	public Style()
	{
		backgroundColor = defaultBackgroundColor;
		forgroundColor = defaultForgroundColor;
		borderColor = defaultBorderColor;
		highlightColor = defaultHighlightColor;
		textColor = defaultTextColor;
		font = defaultFont;
	}

	public Style(Color background, Color forground_highlight, Color border_text)
	{
		this();
		set(background, forground_highlight, border_text, forground_highlight, border_text, null);
	}

	public Style(Color background, Color forground, Color border, Color highlight, Color text, TrueTypeFont font)
	{
		this();
		set(background, forground, border, highlight, text, font);
	}

	public Style set(Color background, Color forground, Color border, Color highlight, Color text, TrueTypeFont font)
	{
		this.backgroundColor = background == null ? defaultBackgroundColor : background;
		this.forgroundColor = forground == null ? defaultForgroundColor : forground;
		this.borderColor = border == null ? defaultBorderColor : border;
		this.highlightColor = highlight == null ? defaultHighlightColor : highlight;
		this.textColor = text == null ? defaultTextColor : text;
		this.font = font == null ? defaultFont : font;
		return this;
	}

	public Style setBackgroundColor(Color color)
	{
		this.backgroundColor = color == null ? defaultBackgroundColor : color;
		return this;
	}

	public Style setForgroundColor(Color color)
	{
		this.forgroundColor = color == null ? defaultForgroundColor : color;
		return this;
	}

	public Style setBorderColor(Color color)
	{
		this.borderColor = color == null ? defaultBorderColor : color;
		return this;
	}

	public Style setHighlightColor(Color color)
	{
		this.highlightColor = color == null ? defaultHighlightColor : color;
		return this;
	}

	public Style setTextColor(Color color)
	{
		this.textColor = color == null ? defaultTextColor : color;
		return this;
	}

	public Style setFont(TrueTypeFont font)
	{
		this.font = font == null ? defaultFont : font;
		return this;
	}

	public Button style(Button button)
	{
		if (button == null)
		{
			button = new Button();
		}
		button.setBackgroundColor(forgroundColor).setHighlightColor(highlightColor).setBorderColor(borderColor).setFontColor(textColor).setFont(font);
		return button;
	}

	public Label style(Label label)
	{
		if (label == null)
		{
			label = new Label();
		}
		label.setColor(textColor).setFont(font);
		return label;
	}

	public Pane style(Pane pane)
	{
		if (pane == null)
		{
			pane = new Pane();
		}
		pane.setBackgroundColor(backgroundColor).setBorderColor(borderColor);
		return pane;
	}

	public RadioButton style(RadioButton radio)
	{
		if (radio == null)
		{
			radio = new RadioButton();
		}
		radio.setBackgroundColor(forgroundColor).setHighlightColor(highlightColor).setBorderColor(borderColor);
		return radio;
	}

	public ScrollPane style(ScrollPane pane)
	{
		if (pane == null)
		{
			pane = new ScrollPane();
		}
		pane.setBackgroundColor(backgroundColor).setBorderColor(borderColor).setScrollBarBackgroundColor(forgroundColor).setScrollBarColor(borderColor)
				.setHighlightColor(highlightColor);

		style(pane.getActor());

		return pane;
	}

	public Slide style(Slide slide)
	{
		if (slide == null)
		{
			slide = new Slide();
		}
		slide.setBackgroundColor(forgroundColor).setHighlightColor(highlightColor).setBorderColor(borderColor).setFont(font);
		return slide;
	}

	public TextBox style(TextBox textbox)
	{
		if (textbox == null)
		{
			textbox = new TextBox();
		}
		textbox.setBackground(forgroundColor).setHighlightColor(highlightColor).setBorderColor(borderColor).setTextColor(textColor).setFont(font);
		return textbox;
	}

	public void style(Object... objects)
	{
		for (Object object : objects)
		{
			if (object instanceof Button)
			{
				style((Button) object);
			}
			if (object instanceof Label)
			{
				style((Label) object);
			}
			if (object instanceof Pane)
			{
				style((Pane) object);
			}
			if (object instanceof RadioButton)
			{
				style((RadioButton) object);
			}
			if (object instanceof Slide)
			{
				style((Slide) object);
			}
			if (object instanceof ScrollPane)
			{
				style((ScrollPane) object);
			}
			if (object instanceof TextBox)
			{
				style((TextBox) object);
			}
			if (object instanceof WidgetGroup)
			{
				WidgetGroup wg = (WidgetGroup) object;
				for (Actor actor : wg.getChildrenAsList(null))
				{
					style(actor);
				}
			}
		}
	}
}
