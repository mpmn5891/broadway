package bmg.broadway.theater.uiSystem;

import java.util.HashMap;
import java.util.Map;

public class ActionType
{
	// static
	private static final Map<Class<? extends Action>, ActionType> actionTypes = new HashMap<>();
	private static int actionsIndex = 0;

	static ActionType of(Class<? extends Action> klass)
	{
		ActionType type = actionTypes.get(klass);
		if (type == null)
		{
			actionTypes.put(klass, type = new ActionType());
		}
		return type;
	}

	// instance
	private int index;

	private ActionType()
	{
		this.index = actionsIndex++;
	}

	boolean equals(ActionType other)
	{
		return this.index == other.index;
	}

	int getIndex()
	{
		return index;
	}
}
