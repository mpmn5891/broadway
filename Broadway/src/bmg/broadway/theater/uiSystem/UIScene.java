package bmg.broadway.theater.uiSystem;

import java.util.ArrayList;
import java.util.List;

import bmg.broadway.geometry.Rectangle;
import bmg.broadway.graphics.ClippingBatch;
import bmg.broadway.graphics.cameras.OrthoCamera;
import bmg.broadway.graphics.shaders.ShaderPrograms;
import bmg.emef.Input.Mouse;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class UIScene
{
	private final OrthoCamera camera;
	private final Program shaderProgram;
	private final ClippingBatch batch;
	private final Group root;
	private final List<Actor> tempList;
	private Actor mouseOverActor = null;

	public UIScene()
	{
		camera = new OrthoCamera();
		this.shaderProgram = ShaderPrograms.UI;
		this.batch = new ClippingBatch();
		root = new Group();
		root.setScene(this);
		tempList = new ArrayList<>();
	}

	public void add(Actor actor)
	{
		if (actor != null)
		{
			root.add(actor);
		}
	}

	public void remove(Actor actor)
	{
		if (actor != null)
		{
			root.remove(actor);
		}
	}

	public void update(float delta)
	{
		camera.apply();
		mouseOverActor = root.hit(Mouse.getMousePosition());
		for (Actor a : root.getChildrenAsList(tempList))
		{
			Rectangle p = a.getBounds();
			Vector3 min = Vector3.REUSABLE_STACK.pop().set(p.x, p.y, 0);
			Vector3 max = Vector3.REUSABLE_STACK.pop().set(p.x + p.width, p.y + p.height, 0);
			a.getTransform().project(min);
			a.getTransform().project(max);
			camera.projectVector(min);
			camera.projectVector(max);
			a.setVisible(min.x < 1f && max.y < 1f && max.x > -1f && min.y > -1f);
			Vector3.REUSABLE_STACK.push(min);
			Vector3.REUSABLE_STACK.push(max);
			a.act(delta);
		}
		tempList.clear();
	}

	public void render(float delta)
	{
		Texture current = Texture.CURRENT;
		Texture.EMPTY.bind();
		camera.apply();
		batch.applyShaderProgram(shaderProgram);
		root.draw(batch, delta);
		batch.flush();
		current.bind();
	}

	public void dispose()
	{
		root.destroy();
	}

	public Vector2 screenToSceneCoords(Vector2 screenCoords)
	{
		Vector3 tVec = Vector3.REUSABLE_STACK.pop();
		camera.unprojectScreenPoint(screenCoords.x, screenCoords.y, tVec);
		screenCoords.set(tVec.x, tVec.y);
		return screenCoords;
	}

	public Vector2 sceneToScreenCoords(Vector2 sceneCoords)
	{
		Vector3 tVec = Vector3.REUSABLE_STACK.pop().set(sceneCoords);
		camera.projectVector(tVec);
		sceneCoords.set(tVec.x, tVec.y);
		return sceneCoords;
	}

	public OrthoCamera getCamera()
	{
		return camera;
	}

	public Group getRootNode()
	{
		return root;
	}

	public Actor getMouseOverActor()
	{
		return mouseOverActor;
	}
}
