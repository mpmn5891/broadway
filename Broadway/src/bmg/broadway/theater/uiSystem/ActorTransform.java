package bmg.broadway.theater.uiSystem;

import bmg.emef.math.Matrix4;
import bmg.emef.math.Quaternion;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class ActorTransform
{
	private final Vector3 position = new Vector3(0, 0, 0);
	private final Vector3 scale = new Vector3(1, 1, 1);
	private final Quaternion rotation = new Quaternion();

	private final Transform local = new Transform();
	private final Transform world = new Transform();

	private ActorTransform parent = null;

	private boolean hasChanged = false;
	private boolean changed = true;

	public void update()
	{
		hasChanged = false;
		if (changed)
		{
			local.reset().rotate(this.rotation).translate(this.position).scale(this.scale);
			hasChanged = true;
			changed = false;
		}
		if (parent != null)
		{
			world.set(local).apply(parent.getWorldTransform());
		}
	}

	public Vector2 project(Vector2 coords)
	{
		Vector3 tVec = Vector3.REUSABLE_STACK.pop().set(coords).multiply(world.matrix);
		coords.set(tVec.x, tVec.y);
		Vector3.REUSABLE_STACK.push(tVec);
		return coords;
	}

	public Vector2 unproject(Vector2 coords)
	{
		Matrix4 tMat = Matrix4.REUSABLE_STACK.pop().set(world.matrix).invert();
		Vector3 tVec = Vector3.REUSABLE_STACK.pop().set(coords).multiply(tMat);
		coords.set(tVec.x, tVec.y);
		Vector3.REUSABLE_STACK.push(tVec);
		Matrix4.REUSABLE_STACK.push(tMat);
		return coords;
	}

	public Vector3 project(Vector3 coords)
	{
		return coords.multiply(world.matrix);
	}

	public Vector3 unproject(Vector3 coords)
	{
		Matrix4 tMat = Matrix4.REUSABLE_STACK.pop().set(world.matrix).invert();
		coords.multiply(tMat);
		Matrix4.REUSABLE_STACK.push(tMat);
		return coords;
	}

	public Vector3 getPosition()
	{
		return position;
	}

	public ActorTransform setPosition(Vector2 v)
	{
		return setPosition(v.x, v.y);
	}

	public ActorTransform setPosition(float x, float y)
	{
		return setPosition(x, y, 0);
	}

	public ActorTransform setPosition(Vector3 v)
	{
		return setPosition(v.x, v.y, v.z);
	}

	public ActorTransform setPosition(float x, float y, float z)
	{
		changed = true;
		this.position.set(x, y, z);
		return this;
	}

	public ActorTransform translate(Vector2 v)
	{
		return translate(v.x, v.y);
	}

	public ActorTransform translate(float x, float y)
	{
		return translate(x, y, 0);
	}

	public ActorTransform translate(Vector3 v)
	{
		return translate(v.x, v.y, v.z);
	}

	public ActorTransform translate(float x, float y, float z)
	{
		changed = true;
		this.position.add(x, y, z);
		return this;
	}

	public Vector3 getScale()
	{
		return scale;
	}

	public ActorTransform setScale(Vector2 v)
	{
		return setScale(v.x, v.y);
	}

	public ActorTransform setScale(float sx, float sy)
	{
		return setScale(sx, sy, 1);
	}

	public ActorTransform setScale(Vector3 v)
	{
		return setScale(v.x, v.y, v.z);
	}

	public ActorTransform setScale(float sx, float sy, float sz)
	{
		changed = true;
		this.scale.set(sx, sy, sz);
		return this;
	}

	public ActorTransform scale(Vector2 v)
	{
		return scale(v.x, v.y);
	}

	public ActorTransform scale(float sx, float sy)
	{
		return scale(sx, sy, 1);
	}

	public ActorTransform scale(Vector3 v)
	{
		return scale(v.x, v.y, v.z);
	}

	public ActorTransform scale(float sx, float sy, float sz)
	{
		changed = true;
		this.scale.scale(sx, sy, sz);
		return this;
	}

	public Quaternion getRotation()
	{
		return rotation;
	}

	public ActorTransform setRotation(Quaternion q)
	{
		changed = true;
		this.rotation.set(q);
		return this;
	}

	public ActorTransform setRotation(Vector3 v)
	{
		changed = true;
		this.rotation.set(v.x, v.y, v.z);
		return this;
	}

	public ActorTransform setRotation(float r)
	{
		changed = true;
		this.rotation.set(0, 0, r);
		return this;
	}

	public ActorTransform setRotation(float rx, float ry, float rz)
	{
		changed = true;
		this.rotation.set(rx, ry, rz);
		return this;
	}

	public ActorTransform rotate(Vector3 v)
	{
		changed = true;
		this.rotation.set(v.x, v.y, v.z);
		return this;
	}

	public ActorTransform rotate(Quaternion q)
	{
		changed = true;
		this.rotation.multiply(q);
		return this;
	}

	public ActorTransform rotate(float r)
	{
		return rotate(0, 0, r);
	}

	public ActorTransform rotate(float rx, float ry, float rz)
	{
		changed = true;
		Quaternion quat = Quaternion.REUSABLE_STACK.pop();
		this.rotation.multiply(quat.set(rx, ry, rz));
		Quaternion.REUSABLE_STACK.push(quat);
		return this;
	}

	public Transform getLocalTransform()
	{
		return local;
	}

	public Transform getWorldTransform()
	{
		return parent == null ? local : world;
	}

	public ActorTransform getParent()
	{
		return parent;
	}

	public void setParent(ActorTransform parent)
	{
		this.parent = parent;
	}

	public boolean hasChanged()
	{
		return hasChanged || (parent != null && parent.hasChanged());
	}
}
