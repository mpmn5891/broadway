package bmg.broadway.theater.uiSystem;

public interface Layout
{
	public void validate();

	public void invalidate();

	public void pack();

	public void doLayout();

	public float getPreferredWidth();

	public float getPreferredHeight();

	public float getMinimumWidth();

	public float getMinimumHeight();

	public float getMaximumWidth();

	public float getMaximumHeight();
}
