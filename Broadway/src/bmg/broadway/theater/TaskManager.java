package bmg.broadway.theater;

import java.util.LinkedList;
import java.util.Queue;

import bmg.emef.environment.Environment;
import bmg.emef.events.callbacks.SimpleCallback;

public class TaskManager
{
	private static TaskManager instance;

	private static final Object initLock = new Object();
	private static final Object postInitLock = new Object();
	private static final Object updateLock = new Object();
	private static final Object preRenderLock = new Object();
	private static final Object renderLock = new Object();
	private static final Object postRenderLock = new Object();
	private static final Object ioLock = new Object();

	private Queue<SimpleCallback> initTasks;
	private Queue<SimpleCallback> postInitTasks;
	private Queue<SimpleCallback> updateTasks;
	private Queue<SimpleCallback> preRenderTasks;
	private Queue<SimpleCallback> renderTasks;
	private Queue<SimpleCallback> postRenderTasks;
	private Queue<SimpleCallback> ioTasks;

	private Thread ioThread;

	protected static TaskManager getInstance()
	{
		if (TaskManager.instance == null)
		{
			TaskManager.instance = new TaskManager();
		}
		return TaskManager.instance;
	}

	private TaskManager()
	{
		initTasks = new LinkedList<>();
		postInitTasks = new LinkedList<>();
		updateTasks = new LinkedList<>();
		preRenderTasks = new LinkedList<>();
		renderTasks = new LinkedList<>();
		postRenderTasks = new LinkedList<>();
		ioTasks = new LinkedList<>();
		Environment.eventManager.registerInitializeListener(this::processInitTasks);
		Environment.eventManager.registerUpdateListener(this::processUpdateTasks);
		Environment.eventManager.registerRenderListener(this::processRenderTasks);
		Environment.eventManager.registerDisposeListener(() ->
		{
			clearInitTasks();
			clearPostInitTasks();
			clearUpdateTasks();
			clearPreRenderTasks();
			clearRenderTasks();
			clearPostRenderTasks();
			clearFileOperations();
		});
		ioThread = new Thread(new Runnable()
		{
			public void run()
			{
				synchronized (ioLock)
				{
					SimpleCallback task = null;
					while ((task = ioTasks.poll()) != null)
					{
						task.invoke();
					}
				}
			}
		});
		ioThread.start();
	}

	public void runOnInit(SimpleCallback task)
	{
		synchronized (initLock)
		{
			initTasks.add(task);
		}
	}

	public void runOnPostInit(SimpleCallback task)
	{
		synchronized (postInitLock)
		{
			postInitTasks.add(task);
		}
	}

	public void runOnUpdate(SimpleCallback task)
	{
		synchronized (updateLock)
		{
			updateTasks.add(task);
		}
	}

	public void runOnPreRender(SimpleCallback task)
	{
		synchronized (preRenderLock)
		{
			preRenderTasks.add(task);
		}
	}

	public void runOnRender(SimpleCallback task)
	{
		synchronized (renderLock)
		{
			renderTasks.add(task);
		}
	}

	public void runOnPostRender(SimpleCallback task)
	{
		synchronized (postRenderLock)
		{
			postRenderTasks.add(task);
		}
	}

	public void fileOperation(SimpleCallback task)
	{
		synchronized (ioLock)
		{
			ioTasks.add(task);
		}
	}

	public void clearInitTasks()
	{
		synchronized (initLock)
		{
			initTasks.clear();
		}
	}

	public void clearPostInitTasks()
	{
		synchronized (postInitLock)
		{
			postInitTasks.clear();
		}
	}

	public void clearUpdateTasks()
	{
		synchronized (updateLock)
		{
			updateTasks.clear();
		}
	}

	public void clearPreRenderTasks()
	{
		synchronized (preRenderLock)
		{
			preRenderTasks.clear();
		}
	}

	public void clearRenderTasks()
	{
		synchronized (renderLock)
		{
			renderTasks.clear();
		}
	}

	public void clearPostRenderTasks()
	{
		synchronized (postRenderLock)
		{
			postRenderTasks.clear();
		}
	}

	public void clearFileOperations()
	{
		synchronized (ioLock)
		{
			ioTasks.clear();
		}
	}

	private void processInitTasks()
	{
		synchronized (initLock)
		{
			SimpleCallback task = null;
			while ((task = initTasks.poll()) != null)
			{
				task.invoke();
			}
		}

		synchronized (postInitLock)
		{
			SimpleCallback task = null;
			while ((task = postInitTasks.poll()) != null)
			{
				task.invoke();
			}
		}
	}

	private void processUpdateTasks(float delta)
	{
		synchronized (updateLock)
		{
			SimpleCallback task = null;
			while ((task = updateTasks.poll()) != null)
			{
				task.invoke();
			}
		}
		if (!ioTasks.isEmpty())
		{
			ioThread.run();
		}
	}

	private void processRenderTasks(float delta)
	{
		synchronized (preRenderLock)
		{
			SimpleCallback task = null;
			while ((task = renderTasks.poll()) != null)
			{
				task.invoke();
			}
		}

		synchronized (renderLock)
		{
			SimpleCallback task = null;
			while ((task = renderTasks.poll()) != null)
			{
				task.invoke();
			}
		}

		synchronized (postRenderLock)
		{
			SimpleCallback task = null;
			while ((task = renderTasks.poll()) != null)
			{
				task.invoke();
			}
		}
	}
}
