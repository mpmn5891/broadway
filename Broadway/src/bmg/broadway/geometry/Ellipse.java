package bmg.broadway.geometry;

import bmg.emef.util.MathUtils;

public class Ellipse
{
	public float x, y, rx, ry;

	public Ellipse()
	{
		this(0, 0, 0, 0);
	}

	public Ellipse(float rx, float ry)
	{
		this(0, 0, rx, ry);
	}

	public Ellipse(float x, float y, float rx, float ry)
	{
		this.x = x;
		this.y = y;
		this.rx = rx;
		this.ry = ry;
	}

	public Ellipse set(Ellipse other)
	{
		if (this.equals(other))
		{
			return this;
		}
		return set(other.x, other.y, other.rx, other.ry);
	}

	public Ellipse set(float x, float y, float rx, float ry)
	{
		this.x = x;
		this.y = y;
		this.rx = rx;
		this.ry = ry;
		return this;
	}

	public Polygon createPolygon(Polygon polygon)
	{
		if (polygon == null)
			polygon = new Polygon();

		polygon.clearVertices();
		polygon.setRotation(0);
		polygon.setScale(1, 1);
		polygon.setPosition(x, y);

		polygon.clearVertices();

		for (int i = 0; i < 360; i++)
			polygon.addVertex(x + rx + MathUtils.cos(i) * rx, y + ry + MathUtils.sin(i) * ry);

		return polygon;
	}
}
