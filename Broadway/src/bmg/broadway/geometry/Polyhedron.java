package bmg.broadway.geometry;

import java.util.ArrayList;
import java.util.List;

import bmg.emef.math.Quaternion;
import bmg.emef.math.Vector3;
import bmg.emef.util.MathUtils;

public class Polyhedron
{
	private Vector3 position;
	private Vector3 rotation;
	private Vector3 scale;
	private List<Vector3> vertices;

	private float minX, minY, minZ, maxX, maxY, maxZ;

	private Cuboid bounds;

	public Polyhedron(Polyhedron other)
	{
		this();

		position.set(other.position);

		for (Vector3 vertex : other.vertices)
			addVertex(vertex.copy());
	}

	public Polyhedron()
	{
		vertices = new ArrayList<>();
		position = new Vector3();
		scale = new Vector3(1, 1, 1);
		clearVertices();
	}

	public void clearVertices()
	{
		vertices.clear();
		minX = minY = minZ = Float.POSITIVE_INFINITY;
		maxX = maxY = maxZ = Float.NEGATIVE_INFINITY;
	}

	public void addVertex(float x, float y, float z)
	{
		addVertex(new Vector3(x, y, z));
	}

	public void addVertex(Vector3 v)
	{
		vertices.add(v);

		minX = MathUtils.min(v.x, minX);
		minY = MathUtils.min(v.y, minY);
		minZ = MathUtils.min(v.z, minZ);
		maxX = MathUtils.max(v.x, maxX);
		maxY = MathUtils.max(v.y, maxY);
		maxZ = MathUtils.max(v.z, maxZ);
	}

	private void updateBounds()
	{
		float minX, minY, minZ, maxX, maxY, maxZ;

		minX = minY = minZ = Float.POSITIVE_INFINITY;
		maxX = maxY = maxZ = Float.NEGATIVE_INFINITY;

		for (Vector3 v : vertices)
		{
			minX = MathUtils.min(minX, v.x);
			minY = MathUtils.min(minY, v.y);
			minZ = MathUtils.min(minZ, v.z);
			maxX = MathUtils.max(maxX, v.x);
			maxY = MathUtils.max(maxY, v.y);
			maxZ = MathUtils.max(maxZ, v.z);
		}

		if (bounds == null)
		{
			bounds = new Cuboid(new Vector3(minX, minY, minZ).add(position), new Vector3(maxX, maxY, maxZ).add(position));
		}
		else
		{
			bounds.set(maxX - minX, maxY - minY, maxZ - minZ, position);
		}
	}

	public boolean contains(Vector3 p)
	{
		int i, j = vertices.size() - 1;
		boolean oddNodes = false;

		Vector3 vi = Vector3.REUSABLE_STACK.pop();
		Vector3 vj = Vector3.REUSABLE_STACK.pop();

		for (i = 0; i < vertices.size(); j = i++)
		{
			vi.set(vertices.get(i)).add(position);
			vj.set(vertices.get(j)).add(position);

			if ((((vi.y <= p.y) && (p.y < vj.y)) ||
					((vj.y <= p.y) && (p.y < vi.y)) ||
					((vj.z <= p.z) && (p.z < vi.z))) &&
					(p.x < (vj.x - vi.x) * (p.y - vi.y) / (vj.y - vi.y) + vi.x))
				oddNodes = !oddNodes;
		}

		Vector3.REUSABLE_STACK.push(vi);
		Vector3.REUSABLE_STACK.push(vj);

		return oddNodes;
	}

	public void translate(float x, float y, float z)
	{
		for (Vector3 v : vertices)
		{
			v.add(x, y, z);
		}
		updateBounds();
	}

	public void rotate(float x, float y, float z)
	{
		Quaternion temp = Quaternion.REUSABLE_STACK.pop();
		for (Vector3 v : vertices)
		{
			temp.set(x, y, z).multiply(v, v);
		}
		Quaternion.REUSABLE_STACK.push(temp);
		updateBounds();
		rotation.add(x, y, z);
	}

	public void scale(float s)
	{
		scale(s, s, s);
	}

	public void scale(float x, float y, float z)
	{
		for (Vector3 v : vertices)
		{
			v.scale(x, y, z);
		}
		updateBounds();
		scale.scale(x, y, z);
	}

	public void setRotation(float x, float y, float z)
	{
		rotate(x - rotation.x, y - rotation.y, z - rotation.z);
	}

	public void setScale(float x, float y, float z)
	{
		scale(x - scale.x, y - scale.y, z - scale.z);
	}

	public List<Vector3> getVertices()
	{
		return vertices;
	}

	public Vector3 getVertex(int index)
	{
		return vertices.get(index);
	}

	public Vector3 getPosition()
	{
		return position;
	}

	public void setPosition(Vector3 position)
	{
		this.position.set(position);

		if (bounds != null)
			bounds.position.set(position);
	}

	public Vector3 getScale()
	{
		return scale;
	}

	public void setScale(Vector3 scale)
	{
		setScale(scale.x, scale.y, scale.z);
	}

	public int vertexCount()
	{
		return vertices.size();
	}

	public Cuboid getBounds()
	{
		updateBounds();
		return bounds;
	}

	public float getWidth()
	{
		return maxX - minX;
	}

	public float getHeight()
	{
		return maxY - minY;
	}

	public float getThickness()
	{
		return maxZ - minZ;
	}

	public Vector3 getRotation()
	{
		return rotation;
	}

	public void setRotation(Vector3 rotation)
	{
		setRotation(rotation.x, rotation.y, rotation.z);
	}

	public Polyhedron copy()
	{
		return new Polyhedron(this);
	}
}
