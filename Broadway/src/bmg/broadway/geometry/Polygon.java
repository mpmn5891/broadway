package bmg.broadway.geometry;

import java.util.ArrayList;
import java.util.List;

import bmg.emef.math.Vector2;
import bmg.emef.util.MathUtils;

public class Polygon
{
	private Vector2 position;
	private Vector2 scale;
	private List<Vector2> vertices;
	private float rotation;

	private float minX, minY, maxX, maxY;
	private float width, height;

	private Rectangle bounds;

	public Polygon()
	{
		this.vertices = new ArrayList<>();
		this.position = new Vector2();
		this.scale = new Vector2(1, 1);
		clearVertices();
	}

	public void clearVertices()
	{
		vertices.clear();
		minX = minY = Float.POSITIVE_INFINITY;
		maxX = maxY = Float.NEGATIVE_INFINITY;
		rotation = 0;
	}

	public Polygon addVertex(float x, float y)
	{
		return addVertex(new Vector2(x, y));
	}

	public Polygon addVertex(Vector2 v)
	{
		vertices.add(v);
		minX = MathUtils.min(v.x, minX);
		minY = MathUtils.min(v.y, minY);
		maxX = MathUtils.max(v.x, maxX);
		maxY = MathUtils.max(v.y, maxY);
		return this;
	}

	private void updateBounds()
	{
		if (bounds == null)
			bounds = new Rectangle();

		float minX, minY, maxX, maxY;

		minX = minY = Float.POSITIVE_INFINITY;
		maxX = maxY = Float.NEGATIVE_INFINITY;

		for (Vector2 v : vertices)
		{
			minX = MathUtils.min(minX, v.x);
			minY = MathUtils.min(minY, v.y);
			maxX = MathUtils.max(maxX, v.x);
			maxY = MathUtils.max(maxY, v.y);
		}

		bounds.set(position.x + minX + ((maxX - minX) / 2), position.y + minY + ((maxY - minY) / 2), maxX - minX, maxY - minY);

		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		this.width = maxX - minX;
		this.height = maxY - minY;
	}

	public Polygon translate(Vector2 v)
	{
		return translate(v.x, v.y);
	}

	public Polygon translate(float x, float y)
	{
		position.add(x, y);

		for (Vector2 v : vertices)
		{
			v.add(x, y);
		}
		return this;
	}

	public Polygon scale(float s)
	{
		return scale(s, s);
	}

	public Polygon scale(float x, float y)
	{
		for (Vector2 v : vertices)
		{
			v.scale(x, y);
		}
		scale.scale(x, y);
		return this;
	}

	public float getRotation()
	{
		return rotation;
	}

	public Polygon setRotation(float rotation)
	{
		if (this.rotation != rotation)
		{
			return rotate(rotation - this.rotation);
		}
		return this;
	}

	public Polygon rotate(float angle)
	{
		return rotate(angle, 0, 0);
	}

	public Polygon rotate(float angle, float originX, float originY)
	{
		rotation += angle;
		if (angle != 0)
		{
			for (Vector2 vertex : vertices)
			{
				vertex.subtract(originX, originY).rotate(angle).add(originX, originY);
			}
		}
		return this;
	}

	public boolean contains(Vector2 p)
	{
		return contains(p.x, p.y);
	}

	public boolean contains(float x, float y)
	{
		boolean oddNodes = false;

		Vector2 vi = Vector2.REUSABLE_STACK.pop();
		Vector2 vj = Vector2.REUSABLE_STACK.pop();

		for (int i = 0; i < vertices.size(); i++)
		{
			vi.set(vertices.get(i)).add(position);
			vj.set(vertices.get((i + 1) % vertices.size())).add(position);

			if ((((vi.y <= y) && (y < vj.y)) ||
					((vj.y <= y) && (y < vi.y))) &&
					(x < (vj.x - vi.x) * (y - vi.y) / (vj.y - vi.y) + vi.x))
				oddNodes = !oddNodes;
		}

		Vector2.REUSABLE_STACK.push(vi);
		Vector2.REUSABLE_STACK.push(vj);

		return oddNodes;
	}

	public List<Vector2> getVertices()
	{
		return vertices;
	}

	public Vector2 getVertex(int index)
	{
		return vertices.get(index);
	}

	public int vertexCount()
	{
		return vertices.size();
	}

	public Vector2 getPosition()
	{
		return position;
	}

	public Polygon setPosition(Vector2 v)
	{
		return setPosition(v.x, v.y);
	}

	public Polygon setPosition(float x, float y)
	{
		this.position.set(x, y);
		if (bounds != null)
		{
			bounds.setCenter(position.x, position.y);
		}
		return this;
	}

	public Polygon setScale(Vector2 s)
	{
		return scale(s.x, s.y);
	}

	public Polygon setScale(int sx, int sy)
	{
		return scale(sx / scale.x, sy / scale.y);
	}

	public Rectangle getBounds()
	{
		updateBounds();
		return bounds;
	}

	public float getMinX()
	{
		return minX;
	}

	public float getMinY()
	{
		return minY;
	}

	public float getMaxX()
	{
		return maxX;
	}

	public float getMaxY()
	{
		return maxY;
	}

	public float getWidth()
	{
		return width;
	}

	public float getHeight()
	{
		return height;
	}

	public Polygon copy()
	{
		Polygon p = new Polygon();
		p.setPosition(position);
		for (Vector2 v : vertices)
		{
			p.addVertex(v.copy());
		}
		p.updateBounds();
		return p;
	}

	public Polygon set(Polygon other)
	{
		if (this.equals(other))
		{
			return this;
		}
		this.clearVertices();
		for (Vector2 v : other.getVertices())
		{
			this.addVertex(v);
		}
		this.updateBounds();
		return this;
	}

	@Override
	public int hashCode()
	{
		int result = position.hashCode();
		result = 31 * result + vertices.hashCode();
		result = 31 * result + (rotation != +0.0f ? Float.floatToIntBits(rotation) : 0);
		result = 31 * result + (minX != +0.0f ? Float.floatToIntBits(minX) : 0);
		result = 31 * result + (minY != +0.0f ? Float.floatToIntBits(minY) : 0);
		result = 31 * result + (maxX != +0.0f ? Float.floatToIntBits(maxX) : 0);
		result = 31 * result + (maxY != +0.0f ? Float.floatToIntBits(maxY) : 0);
		result = 31 * result + bounds.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Polygon polygon = (Polygon) o;

		return Float.compare(polygon.maxX, maxX) == 0 &&
				Float.compare(polygon.maxY, maxY) == 0 &&
				Float.compare(polygon.minX, minX) == 0 &&
				Float.compare(polygon.minY, minY) == 0 &&
				Float.compare(polygon.rotation, rotation) == 0 &&
				bounds.equals(polygon.bounds) &&
				position.equals(polygon.position) &&
				vertices.equals(polygon.vertices);
	}

	@Override
	public String toString()
	{
		return "Polygon{" +
				"position=" + position +
				", vertices=" + vertices +
				", rotation=" + rotation +
				", minX=" + minX +
				", minY=" + minY +
				", maxX=" + maxX +
				", maxY=" + maxY +
				", bounds=" + bounds +
				'}';
	}
}
