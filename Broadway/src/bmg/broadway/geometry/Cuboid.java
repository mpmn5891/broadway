package bmg.broadway.geometry;

import bmg.emef.math.Vector3;

public class Cuboid
{
	public float width;
	public float height;
	public float thickness;
	public Vector3 position;

	public Cuboid(Vector3 position, float width, float height, float thickness)
	{
		this.width = width;
		this.height = height;
		this.thickness = thickness;

		this.position = new Vector3(position);
	}

	public Cuboid()
	{
		width = height = thickness = 1;
		position = new Vector3();
	}

	public Cuboid(Vector3 min, Vector3 max)
	{
		Vector3 size = max.subtract(min);

		width = size.x;
		height = size.y;
		thickness = size.z;

		max.add(min);

		position = new Vector3(min).add(max).scale(0.5f);
	}

	public Polyhedron createPolyhedron()
	{
		return createPolyhedron(null);
	}

	public Polyhedron createPolyhedron(Polyhedron polyhedron)
	{
		if (polyhedron == null)
			polyhedron = new Polyhedron();

		polyhedron.clearVertices();

		// Precomputed dimensions
		final float halfWidth = width / 2;
		final float halfHeight = height / 2;
		final float halfThickness = thickness / 2;

		// Front face
		polyhedron.addVertex(-halfWidth, -halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, -halfHeight, +halfThickness);
		polyhedron.addVertex(-halfWidth, +halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, +halfHeight, +halfThickness);

		// Right face
		polyhedron.addVertex(+halfWidth, +halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, -halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, +halfHeight, -halfThickness);
		polyhedron.addVertex(+halfWidth, -halfHeight, -halfThickness);

		// Back face
		polyhedron.addVertex(+halfWidth, -halfHeight, -halfThickness);
		polyhedron.addVertex(-halfWidth, -halfHeight, -halfThickness);
		polyhedron.addVertex(+halfWidth, +halfHeight, -halfThickness);
		polyhedron.addVertex(-halfWidth, +halfHeight, -halfThickness);

		// Left face
		polyhedron.addVertex(-halfWidth, +halfHeight, -halfThickness);
		polyhedron.addVertex(-halfWidth, -halfHeight, -halfThickness);
		polyhedron.addVertex(-halfWidth, +halfHeight, +halfThickness);
		polyhedron.addVertex(-halfWidth, -halfHeight, +halfThickness);

		// Bottom face
		polyhedron.addVertex(-halfWidth, -halfHeight, +halfThickness);
		polyhedron.addVertex(-halfWidth, -halfHeight, -halfThickness);
		polyhedron.addVertex(+halfWidth, -halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, -halfHeight, -halfThickness);

		// Move to top
		polyhedron.addVertex(+halfWidth, -halfHeight, -halfThickness);
		polyhedron.addVertex(-halfWidth, +halfHeight, +halfThickness);

		// Top face
		polyhedron.addVertex(-halfWidth, +halfHeight, +halfThickness);
		polyhedron.addVertex(+halfWidth, +halfHeight, +halfThickness);
		polyhedron.addVertex(-halfWidth, +halfHeight, -halfThickness);
		polyhedron.addVertex(+halfWidth, +halfHeight, -halfThickness);

		return polyhedron;
	}

	public void set(float width, float height, float thickness, Vector3 position)
	{
		this.position.set(position);

		this.width = width;
		this.height = height;
		this.thickness = thickness;
	}

	public float getIntersectionWidth(Cuboid aabb)
	{
		float tx1 = this.position.x - this.width / 2;
		float rx1 = aabb.position.x - aabb.width / 2;

		float tx2 = tx1 + this.width;
		float rx2 = rx1 + aabb.width;

		return tx2 > rx2 ? rx2 - tx1 : tx2 - rx1;
	}

	public float getIntersectionHeight(Cuboid aabb)
	{
		float ty1 = this.position.y - this.height / 2;
		float ry1 = aabb.position.y - aabb.height / 2;

		float ty2 = ty1 + this.height;
		float ry2 = ry1 + aabb.height;

		return ty2 > ry2 ? ry2 - ty1 : ty2 - ry1;
	}

	public float getIntersectionThickness(Cuboid aabb)
	{
		float tz1 = this.position.z - this.thickness / 2;
		float rz1 = aabb.position.z - aabb.thickness / 2;

		float tz2 = tz1 + this.thickness;
		float rz2 = rz1 + aabb.thickness;

		return tz2 > rz2 ? rz2 - tz1 : tz2 - rz1;
	}
}
