package bmg.broadway.geometry;

import bmg.emef.math.Vector2;

public class Rectangle
{
	public float x, y, width, height;

	public Rectangle()
	{
		this(0, 0, 0, 0);
	}

	public Rectangle(float widht, float height)
	{
		this(0, 0, widht, height);
	}

	public Rectangle(Vector2 min, Vector2 max)
	{
		this(min.x, min.y, max.x, max.y);
	}

	public Rectangle(float x, float y, float width, float height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Polygon createPolygon()
	{
		return createPolygon(null);
	}

	public Polygon createPolygon(Polygon polygon)
	{
		if (polygon == null)
		{
			polygon = new Polygon();
		}

		polygon.clearVertices();
		polygon.setRotation(0);
		polygon.setScale(1, 1);
		polygon.setPosition(x + width / 2, y + height / 2);

		polygon.addVertex(-width / 2, -height / 2);
		polygon.addVertex(+width / 2, -height / 2);
		polygon.addVertex(+width / 2, +height / 2);
		polygon.addVertex(-width / 2, +height / 2);

		return polygon;
	}

	public boolean contains(Vector2 point)
	{
		return contains(point.x, point.y);
	}

	public boolean contains(float xPoint, float yPoint)
	{
		return (xPoint > x) && (xPoint < x + width) && (yPoint > y) && (yPoint < y + height);
	}

	public boolean contains(Rectangle r)
	{
		return (r.x >= x) && (r.x + r.width <= x + width) && (r.y >= y) && (r.y + r.height <= y + height);
	}

	public boolean intersects(Rectangle r)
	{
		return (x < r.x + r.width) && (r.x < x + width) && (y < r.y + r.height) && (r.y < y + height);
	}

	public float getIntersectionWidth(Rectangle aabb)
	{
		float tx1 = this.x;
		float rx1 = aabb.x;

		float tx2 = tx1 + this.width;
		float rx2 = rx1 + aabb.width;

		return tx2 > rx2 ? rx2 - tx1 : tx2 - rx1;
	}

	public float getIntersectionHeight(Rectangle aabb)
	{
		float ty1 = this.y;
		float ry1 = aabb.y;

		float ty2 = ty1 + this.height;
		float ry2 = ry1 + aabb.height;

		return ty2 > ry2 ? ry2 - ty1 : ty2 - ry1;
	}

	public Rectangle setPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
		return this;
	}

	public Rectangle setCenter(float x, float y)
	{
		this.x = x + width / 2;
		this.y = y + height / 2;
		return this;
	}

	public Rectangle set(Rectangle other)
	{
		return set(other.x, other.y, other.width, other.height);
	}

	public Rectangle set(float x, float y, float width, float height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		return this;
	}

	public Rectangle copy()
	{
		return new Rectangle(x, y, width, height);
	}

	@Override
	public int hashCode()
	{
		int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
		result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
		result = 31 * result + (width != +0.0f ? Float.floatToIntBits(width) : 0);
		result = 31 * result + (height != +0.0f ? Float.floatToIntBits(height) : 0);
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Rectangle rectangle = (Rectangle) o;

		return Float.compare(rectangle.height, height) == 0 &&
				Float.compare(rectangle.width, width) == 0 &&
				Float.compare(rectangle.x, x) == 0 &&
				Float.compare(rectangle.y, y) == 0;
	}

	@Override
	public String toString()
	{
		return "Rectangle{" +
				"width=" + width +
				", height=" + height +
				'}';
	}
}
