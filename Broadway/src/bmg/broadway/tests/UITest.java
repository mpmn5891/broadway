package bmg.broadway.tests;

import bmg.broadway.theater.Stage;
import bmg.broadway.theater.Theater;
import bmg.broadway.theater.uiSystem.UIScene;
import bmg.broadway.theater.uiSystem.actors.Button;
import bmg.broadway.theater.uiSystem.actors.Label;
import bmg.broadway.theater.uiSystem.actors.Pane;
import bmg.broadway.theater.uiSystem.actors.RadioButton;
import bmg.broadway.theater.uiSystem.actors.RadioGroup;
import bmg.broadway.theater.uiSystem.actors.ScrollPane;
import bmg.broadway.theater.uiSystem.actors.Slide;
import bmg.broadway.theater.uiSystem.actors.TextBox;
import bmg.broadway.theater.uiSystem.actors.WidgetShelf;
import bmg.broadway.theater.uiSystem.actors.WidgetStack;
import bmg.broadway.theater.uiSystem.style.Style;
import bmg.emef.Input.Keyboard;
import bmg.emef.Input.Mouse;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.gl.GLContext;

public class UITest extends Stage
{
	private UIScene scene;
	private ScrollPane scroll;
	private Pane pane;
	private WidgetShelf shelf;
	private Button button;
	private Label label;
	private TextBox textBox;
	private WidgetStack stack;
	private RadioButton radio;
	private RadioButton radio2;
	private RadioButton radio3;
	private RadioGroup radioGroup;
	private Slide slide;
	private Style style;

	public UITest()
	{
		super("ActorTest");
	}

	@Override
	public void setup()
	{
		GLContext.clearColor(Color.BEIGE);
		Environment.Analytics.noLog = true;

		scene = new UIScene();

		scroll = new ScrollPane();
		scroll.setFillParent(true);

		pane = new Pane();
		pane.setFillBackground(true);
		pane.setFillParent(false);
		pane.setDrawBorder(true);
		pane.setPadding(5);
		pane.setPreferredSize(300, 300);

		shelf = new WidgetShelf();
		shelf.setAlignment(WidgetShelf.Alignment.TOP);

		button = new Button();
		button.setText("BUTTON");
		button.setPadding(10);
		button.setFillHoverStyle(false);

		label = new Label();
		label.setText("Label");

		textBox = new TextBox();
		textBox.setText("Text Box Test Text");
		textBox.setPadding(12);

		stack = new WidgetStack();
		stack.setAlignment(WidgetStack.Alignment.RIGHT);
		radio = new RadioButton();
		radio.setButtonShape(RadioButton.buttonShape.toggle);
		radio.setPadding(10, 5);
		radio2 = new RadioButton();
		radio2.setButtonShape(RadioButton.buttonShape.toggle);
		radio2.setPadding(10, 5);
		radio3 = new RadioButton();
		radio3.setButtonShape(RadioButton.buttonShape.toggle);
		radio3.setPadding(10, 5);
		radioGroup = new RadioGroup(radio, radio2, radio3);
		radioGroup.addBehavior(RadioGroup.Behavior.FORCE_SELECTION);
		stack.add(radio, radio2, radio3);
		stack.pack();

		slide = new Slide();
		slide.setPadding(10);

		shelf.add(button, label, textBox, stack, slide);
		pane.add(shelf);

		scroll.setActor(pane);

		scene.add(scroll);

		scroll.pack();

		style = new Style();
		style.style(scroll);

		Environment.display.centerOnScreen();
	}

	@Override
	public void update(float delta)
	{
		scene.update(delta);

		if (button.isClicked())
		{
			Environment.exit();
		}

		if (Keyboard.isKeyDown(Keyboard.QWERTY.KEY_ESCAPE))
		{
			Environment.exit();
		}

		Environment.display.setTitle("ActorTest :: " + Environment.loop.getFPS() + " :: " + Environment.Analytics.renderCallsThisFrame
				+ " :: " + Mouse.getMousePosition().toString());
	}

	@Override
	public void resized(float width, float height)
	{
		GLContext.viewport(0, 0, (int) width, (int) height);
		scene.getCamera().initProjection(width, height);
	}

	@Override
	public void render(float delta)
	{
		scene.render(delta);
	}

	@Override
	public void dispose()
	{
		scene.dispose();
	}

	public static void main(String... args)
	{
		UITest test = new UITest();
		Theater.getInstance().addStage(test);
		Theater.getInstance().setCurrentStage(test.getName());
		Theater.getInstance().lightsCameraAction();
	}
}
