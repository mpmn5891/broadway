package bmg.broadway.util;

import bmg.emef.math.Vector3;
import bmg.emef.util.ObjectStack;

public class Ray
{
	public static final ObjectStack<Ray> RUSABLE_STACK = new ObjectStack<>(Ray::new);

	private final Vector3 origin = new Vector3();
	private final Vector3 direction = new Vector3();

	public Vector3 getPoint(float distance, Vector3 dest)
	{
		if (dest == null)
		{
			dest = new Vector3();
		}
		dest.set(direction).scale(distance).normalize().add(origin);
		return dest;
	}

	public void setOrigin(Vector3 origin)
	{
		if (origin == null)
		{
			return;
		}
		this.origin.set(origin);
	}

	public void setDirection(Vector3 direction)
	{
		if (direction == null)
		{
			return;
		}
		this.direction.set(direction);
	}

	public Vector3 getOrigin()
	{
		return origin;
	}

	public Vector3 getDirection()
	{
		return direction;
	}

	@Override
	public int hashCode()
	{
		int result = origin.hashCode();
		result = 31 * result + direction.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Ray ray = (Ray) o;

		return origin.equals(ray.origin) && direction.equals(ray.direction);
	}

	@Override
	public String toString()
	{
		return "Ray{" +
				"origin=" + origin +
				", direction=" + direction +
				'}';
	}
}
