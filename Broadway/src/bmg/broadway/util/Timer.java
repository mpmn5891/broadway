package bmg.broadway.util;

public class Timer
{
	private float duration, currentTime;
	private boolean running, done;

	public Timer(float time)
	{
		this.duration = time;
		running = false;
		done = false;
	}

	public void update(float delta)
	{
		if (!running)
		{
			return;
		}
		if (done)
		{
			return;
		}

		currentTime += delta;
		if (currentTime >= duration)
		{
			done = true;
			running = false;
		}
	}

	public void start()
	{
		if (running)
		{
			return;
		}

		running = true;
		done = false;
		currentTime = 0f;
	}

	public void reset()
	{
		running = false;
		currentTime = 0;
		done = false;
	}

	public float pause()
	{
		running = false;
		return currentTime;
	}

	public void resume()
	{
		running = true;
	}

	public boolean isDone()
	{
		return done;
	}

	public float getDuration()
	{
		return duration;
	}

	public float getProgress()
	{
		return currentTime;
	}
}
