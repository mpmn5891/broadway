package bmg.broadway.graphics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bmg.broadway.geometry.Polygon;
import bmg.broadway.geometry.Rectangle;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics.Primitive;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.math.Plane;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector3;

public class ShapeBatch
{
	private ClippingBatch batch;
	private Map<Mode, List<Shape>> shapes;
	private boolean active;

	public ShapeBatch(ClippingBatch batch)
	{
		this.batch = batch;
		this.shapes = new HashMap<>();
		active = false;
	}

	public void begin()
	{
		if (active) return;

		shapes.clear();
		active = true;
	}

	public void render(Polygon polygon, Mode mode, Transform transform, Rectangle clip, Color color)
	{
		if (!active)
		{
			throw new Exceptions.Exception(
					"[" + this.getClass().getName() + "].render(Polygon, Mode, Transform, Rectangle, Color) :: Can not render while inactive, use ShapeBatch.begin() first.");
		}

		List<Shape> list = shapes.get(mode);
		if (list == null)
		{
			shapes.put(mode, list = new ArrayList<>());
		}
		Shape shape = new Shape();
		shape.polygon = polygon;
		shape.transform = transform;
		shape.clip = clip;
		shape.color = color;

		list.add(shape);
	}

	public void flush()
	{
		if (!active) return;

		Texture current = Texture.CURRENT;
		Texture.EMPTY.bind();
		batch.applyShaderProgram(Program.CURRENT);

		for (java.util.Map.Entry<Mode, List<Shape>> entry : shapes.entrySet())
		{
			List<Shape> list = entry.getValue();
			Collections.reverse(list);

			for (Shape shape : list)
			{
				Vector3 tVec = Vector3.REUSABLE_STACK.pop();

				setClip(shape.clip, shape.transform);
				batch.begin(entry.getKey() == Mode.FILLED ? Primitive.TRIANGLES : Primitive.LINES);
				{
					for (int i = 0; i < shape.polygon.vertexCount(); i++)
					{
						if (entry.getKey() == Mode.FILLED)
						{
							batch.flushOnOverflow(3);
							batch.vertex(tVec.set(shape.polygon.getPosition()).multiply(shape.transform.matrix));
							batch.color(shape.color);
						}
						else
						{
							batch.flushOnOverflow(2);
						}
						batch.vertex(tVec.set(shape.polygon.getVertex(i)).add(shape.polygon.getPosition(), 0).multiply(shape.transform.matrix));
						batch.color(shape.color);
						batch.vertex(tVec.set(shape.polygon.getVertex((i + 1) % shape.polygon.vertexCount())).add(shape.polygon.getPosition(), 0).multiply(shape.transform.matrix));
						batch.color(shape.color);
					}
				}
				batch.end();

				Vector3.REUSABLE_STACK.push(tVec);
			}
		}

		current.bind();
		active = false;
	}

	private void setClip(Rectangle clip, Transform transform)
	{
		Plane l = Plane.REUSABLE_STACK.pop();
		Plane r = Plane.REUSABLE_STACK.pop();
		Plane t = Plane.REUSABLE_STACK.pop();
		Plane b = Plane.REUSABLE_STACK.pop();
		Vector3 tVec = Vector3.REUSABLE_STACK.pop();
		tVec.set(0, 0, 0).multiply(transform.matrix);
		l.set(1, 0, 0, -(clip.x));
		r.set(-1, 0, 0, (clip.x + clip.width));
		t.set(0, 1, 0, -(clip.y));
		b.set(0, -1, 0, (clip.y + clip.height));
		batch.beginClip(l, r, t, b);
		Vector3.REUSABLE_STACK.push(tVec);
		Plane.REUSABLE_STACK.push(l, r, t, b);
	}

	private class Shape
	{
		public Polygon polygon;
		public Transform transform;
		public Rectangle clip;
		public Color color;
	}

	public enum Mode
	{
		OUTLINE,
		FILLED;
	}
}
