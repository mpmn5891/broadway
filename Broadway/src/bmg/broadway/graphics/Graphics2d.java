package bmg.broadway.graphics;

import bmg.broadway.geometry.Polygon;
import bmg.broadway.geometry.Rectangle;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics.Primitive;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.math.Plane;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class Graphics2d
{
	private final ClippingBatch batch;
	private TrueTypeFont font;
	private Color drawColor;
	private final Transform transform = new Transform();
	private final Polygon tempPolygon = new Polygon();
	private final Vector3 tempVector = new Vector3();
	private final ClipStack clipStack = new ClipStack();
	private boolean clipped = false;

	public Graphics2d(ClippingBatch batch)
	{
		this.batch = batch;
		font = TrueTypeFont.DEFAULT;
		drawColor = Color.WHITE;
	}

	public void setFont(TrueTypeFont font)
	{
		if (font != null)
		{
			this.font = font;
		}
	}

	public TrueTypeFont getFont()
	{
		return this.font;
	}

	public void setDrawColor(Color color)
	{
		if (color != null)
		{
			this.drawColor = color;
		}
	}

	public void setTransform(Transform transform)
	{
		if (transform != null)
		{
			this.transform.set(transform);
		}
	}

	public void setClippingBounds(Rectangle bounds)
	{
		setClippingBounds(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	public void setClippingBounds(float left, float top, float right, float bottom)
	{
		Rectangle temp = new Rectangle(left, top, right, bottom);
		Vector3 tVec = Vector3.REUSABLE_STACK.pop().set(left, top, 0);
		tVec.multiply(transform.matrix);
		temp.x = tVec.x;
		temp.y = tVec.y;
		Vector3.REUSABLE_STACK.push(tVec);
		clipped = clipStack.push(temp);
		setClip(temp.x, temp.width, temp.y, temp.height);
	}

	public void clearClippingBounds()
	{
		clipStack.pop();
		Rectangle temp = clipStack.peek();
		clipped = !(temp.width > 0 && temp.height > 0);
		setClip(temp.x, temp.width, temp.y, temp.height);
	}

	private void setClip(float left, float right, float top, float bottom)
	{
		Plane l = Plane.REUSABLE_STACK.pop();
		Plane r = Plane.REUSABLE_STACK.pop();
		Plane t = Plane.REUSABLE_STACK.pop();
		Plane b = Plane.REUSABLE_STACK.pop();
		Vector3 tVec = Vector3.REUSABLE_STACK.pop();
		tVec.set(0, 0, 0).multiply(transform.matrix);
		l.set(1, 0, 0, -(left));
		r.set(-1, 0, 0, (left + right));
		t.set(0, 1, 0, -(top));
		b.set(0, -1, 0, (top + bottom));
		batch.beginClip(l, r, t, b);
		Vector3.REUSABLE_STACK.push(tVec);
		Plane.REUSABLE_STACK.push(l, r, t, b);
	}

	public void drawLine(float x1, float y1, float x2, float y2)
	{
		if (clipped) return;

		Texture current = Texture.CURRENT;
		Texture.EMPTY.bind();
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.LINES);
		{
			batch.flushOnOverflow(2);
			batch.vertex(tempVector.set(x1, y1, 0).multiply(transform.matrix));
			batch.color(drawColor);
			batch.vertex(tempVector.set(x2, y2, 0).multiply(transform.matrix));
			batch.color(drawColor);
		}
		batch.end();
		current.bind();
	}

	public void drawPoints(Vector2[] points)
	{
		if (clipped) return;

		if (points.length > 1)
		{
			Texture current = Texture.CURRENT;
			Texture.EMPTY.bind();
			batch.applyShaderProgram(Program.CURRENT);
			batch.begin(Primitive.LINE_LOOP);
			{
				for (Vector2 point : points)
				{
					batch.vertex(tempVector.set(point).multiply(transform.matrix));
					batch.color(drawColor);
				}
			}
			batch.end();
			current.bind();
		}
	}

	public void drawRectangle(Rectangle rectangle)
	{
		drawRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}

	public void drawRectangle(float x, float y, float width, float height)
	{
		if (clipped) return;

		ShapeBuilder.rectangle(width, height, tempPolygon);
		drawPolygon(x, y, tempPolygon);
	}

	public void fillRectangle(Rectangle rectangle)
	{
		fillRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}

	public void fillRectangle(float x, float y, float width, float height)
	{
		if (clipped) return;

		ShapeBuilder.rectangle(width, height, tempPolygon);
		fillPolygon(x, y, tempPolygon);
	}

	public void drawRoundedRectangle(float x, float y, float width, float height, float radius)
	{
		if (clipped) return;

		ShapeBuilder.roundedRectangle(width, height, radius, tempPolygon);
		drawPolygon(x, y, tempPolygon);
	}

	public void fillRoundedRectangle(float x, float y, float width, float height, float radius)
	{
		if (clipped) return;

		ShapeBuilder.roundedRectangle(width, height, radius, tempPolygon);
		fillPolygon(x, y, tempPolygon);
	}

	public void drawOval(float x, float y, float xRadius, float yRadius)
	{
		if (clipped) return;

		ShapeBuilder.ellipse(xRadius, yRadius, tempPolygon);
		drawPolygon(x, y, tempPolygon);
	}

	public void fillOval(float x, float y, float xRadius, float yRadius)
	{
		if (clipped) return;

		ShapeBuilder.ellipse(xRadius, yRadius, tempPolygon);
		fillPolygon(x, y, tempPolygon);
	}

	public void drawArc(float x, float y, float startAngle, float endAngle, float radius)
	{
		if (clipped) return;

		ShapeBuilder.arc(0, 0, startAngle, endAngle, radius, tempPolygon);
		drawPolygon(x, y, tempPolygon);
	}

	public void fillArc(float x, float y, float startAngle, float endAngle, float radius)
	{
		if (clipped) return;

		ShapeBuilder.arc(0, 0, startAngle, endAngle, radius, tempPolygon);
		fillPolygon(x, y, tempPolygon);
	}

	public void drawPolygon(float x, float y, Polygon polygon)
	{
		if (clipped) return;

		tempPolygon.set(polygon);
		tempPolygon.setPosition(x + tempPolygon.getWidth() / 2, y + tempPolygon.getHeight() / 2);
		Texture current = Texture.CURRENT;
		Texture.EMPTY.bind();
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.LINES);
		{
			for (int i = 0; i < tempPolygon.vertexCount(); i++)
			{
				batch.flushOnOverflow(2);
				batch.vertex(tempVector.set(tempPolygon.getVertex(i)).add(tempPolygon.getPosition(), 0).multiply(transform.matrix));
				batch.color(drawColor);
				batch.vertex(tempVector.set(tempPolygon.getVertex((i + 1) % tempPolygon.vertexCount())).add(tempPolygon.getPosition(), 0).multiply(transform.matrix));
				batch.color(drawColor);
			}
		}
		batch.end();
		current.bind();
	}

	public void fillPolygon(float x, float y, Polygon polygon)
	{
		if (clipped) return;

		tempPolygon.set(polygon);
		tempPolygon.setPosition(x + tempPolygon.getWidth() / 2, y + tempPolygon.getHeight() / 2);
		Texture current = Texture.CURRENT;
		Texture.EMPTY.bind();
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.TRIANGLES);
		{
			for (int i = 0; i < tempPolygon.vertexCount(); i++)
			{
				batch.flushOnOverflow(3);
				batch.vertex(tempVector.set(tempPolygon.getPosition()).multiply(transform.matrix));
				batch.color(drawColor);
				batch.vertex(tempVector.set(tempPolygon.getVertex(i)).add(tempPolygon.getPosition(), 0).multiply(transform.matrix));
				batch.color(drawColor);
				batch.vertex(tempVector.set(tempPolygon.getVertex((i + 1) % tempPolygon.vertexCount())).add(tempPolygon.getPosition(), 0).multiply(transform.matrix));
				batch.color(drawColor);
			}
		}
		batch.end();
		current.bind();
	}

	public void drawTexture(float x, float y, float width, float height, Texture texture)
	{
		if (clipped) return;

		batch.end(true);

		Texture current = Texture.CURRENT;
		texture.bind();
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.TRIANGLES);
		{
			batch.flushOnOverflow(6);

			batch.vertex(tempVector.set(x, y, 0).multiply(transform.matrix));
			batch.texCoord(0, 0);
			batch.vertex(tempVector.set(x + width, y, 0).multiply(transform.matrix));
			batch.texCoord(1, 0);
			batch.vertex(tempVector.set(x + width, y + height, 0).multiply(transform.matrix));
			batch.texCoord(1, 1);

			batch.vertex(tempVector.set(x, y, 0).multiply(transform.matrix));
			batch.texCoord(0, 0);
			batch.vertex(tempVector.set(x + width, y + height, 0).multiply(transform.matrix));
			batch.texCoord(1, 1);
			batch.vertex(tempVector.set(x, y + height, 0).multiply(transform.matrix));
			batch.texCoord(0, 1);
		}
		batch.end(true);
		current.bind();
	}

	public void drawString(float x, float y, String message)
	{
		if (clipped) return;

		drawString(x, y, -1, -1, message);
	}

	public void drawString(float x, float y, float width, float height, String message)
	{
		if (clipped) return;

		font.drawString(message, x, y, width, height, drawColor, transform, batch);
	}

	public void drawCenteredString(float x, float y, float width, float height, String message)
	{
		if (clipped) return;

		font.drawString(message, x + width / 2, y, width, height, drawColor, transform, TrueTypeFont.Modifiers.CENTER, batch);
	}

	public void drawRightString(float x, float y, float width, float height, String message)
	{
		if (clipped) return;

		font.drawString(message, x, y, width, height, drawColor, transform, TrueTypeFont.Modifiers.RIGHT, batch);
	}
}
