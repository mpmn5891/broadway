package bmg.broadway.graphics;

import bmg.broadway.geometry.Polygon;
import bmg.emef.environment.Environment;
import bmg.emef.util.MathUtils;

public class ShapeBuilder
{
	public static Polygon rectangle(float width, float height, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		width = width / 2;
		height = height / 2;
		dest.clearVertices();
		dest.addVertex(-width, -height);
		dest.addVertex(width, -height);
		dest.addVertex(width, height);
		dest.addVertex(-width, height);
		dest.getBounds();
		return dest;
	}

	public static Polygon circle(float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		for (int i = 0; i < 360; i++)
		{
			dest.addVertex(MathUtils.cos(i) * radius, MathUtils.sin(i) * radius);
		}
		dest.getBounds();
		return dest;
	}

	public static Polygon ellipse(float xRadius, float yRadius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		for (int i = 0; i < 360; i++)
		{
			dest.addVertex(MathUtils.cos(i) * xRadius, MathUtils.sin(i) * yRadius);
		}
		dest.getBounds();
		return dest;
	}

	public static Polygon roundedRectangle(float width, float height, float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		if (width / 2 < radius)
		{
			Environment.log.warn("[" + ShapeBuilder.class.getName() + "] Creating rounded rectangle with {radius} greater than half of {width}. Decreasing {radius}.");
			radius = width / 2;
		}
		if (height / 2 < radius)
		{
			Environment.log.warn("[" + ShapeBuilder.class.getName() + "] Creating rounded rectangle with {radius} greater than half of {height}. Decreasing {radius}.");
			radius = height / 2;
		}

		float halfWidth = width / 2;
		float halfHeight = height / 2;

		roundedCorner(-halfWidth + radius, -halfHeight + radius, 180, 270, radius, dest);
		roundedCorner(halfWidth - radius, -halfHeight + radius, 270, 360, radius, dest);
		roundedCorner(halfWidth - radius, halfHeight - radius, 0, 90, radius, dest);
		roundedCorner(-halfWidth + radius, halfHeight - radius, 90, 180, radius, dest);

		dest.getBounds();

		return dest;
	}

	public static Polygon arc(float x, float y, float startAngle, float endAngle, float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		for (float i = startAngle; i < endAngle; i++)
		{
			dest.addVertex(x + MathUtils.cos(i) * radius, y + MathUtils.sin(i) * radius);
		}
		dest.getBounds();
		return dest;
	}

	public static Polygon star(float outerRadius, float innerRadius, int numSpikes, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();

		if (innerRadius > outerRadius)
		{
			Environment.log.warn("[" + ShapeBuilder.class.getName() + "] Creating star with {innerRadius} greater than {outerRadius}. Flipping.");
			float temp = outerRadius;
			outerRadius = innerRadius;
			innerRadius = temp;
		}

		if (numSpikes < 2)
		{
			Environment.log.warn("[" + ShapeBuilder.class.getName() + "] Creating star with {numSpikes} less than 2. Increasing {numSpikes}.");
			numSpikes = 2;
		}

		double step = 360.0 / (numSpikes * 2);
		int count = 0;
		while (count < numSpikes * 2)
		{
			dest.addVertex((float) MathUtils.cos((count * step) - 90) * outerRadius, (float) MathUtils.sin((count * step) - 90) * outerRadius);
			count++;
			dest.addVertex((float) MathUtils.cos((count * step) - 90) * innerRadius, (float) MathUtils.sin((count * step) - 90) * innerRadius);
			count++;
		}

		dest.getBounds();
		return dest;
	}

	public static Polygon hexagon(float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		radialShape(radius, 6, dest);
		dest.getBounds();
		return dest;
	}

	public static Polygon octagon(float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		radialShape(radius, 8, dest);
		dest.getBounds();
		return dest;
	}

	public static Polygon pentagon(float radius, Polygon dest)
	{
		if (dest == null)
		{
			dest = new Polygon();
		}
		dest.clearVertices();
		radialShape(radius, 5, dest);
		dest.getBounds();
		return dest;
	}

	private static Polygon roundedCorner(float x, float y, float startAngle, float endAngle, float radius, Polygon poly)
	{
		for (float i = startAngle; i < endAngle; i++)
		{
			poly.addVertex(x + MathUtils.cos(i) * radius, y + MathUtils.sin(i) * radius);
		}
		return poly;
	}

	private static Polygon radialShape(float radius, int sides, Polygon poly)
	{
		double step = 360.0 / sides;
		for (int i = 0; i < sides; i++)
		{
			poly.addVertex((float) MathUtils.cos((i * step) - 90) * radius, (float) MathUtils.sin((i * step) - 90) * radius);
		}
		return poly;
	}
}
