package bmg.broadway.graphics;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics.Primitive;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector3;
import bmg.emef.util.MathUtils;

public class TrueTypeFont
{
	// static functions
	public static final int PLAIN = Font.PLAIN;
	public static final int ITALIC = Font.ITALIC;
	public static final int BOLD = Font.BOLD;
	public static final int ITALIC_BOLD = Font.ITALIC + Font.BOLD;

	public static final TrueTypeFont DEFAULT = new TrueTypeFont("Arial");

	private static final int STANDARD_CHARACTERS = 256;
	private static final String TRAILING_WHITESPACE = "\\s+$";

	private FontChar[] fontChars = new FontChar[STANDARD_CHARACTERS];
	private boolean antialias = true;
	private int size;
	private int style;
	private Texture[] fontPages;
	private Font awtFont;
	private FontMetrics fontMetrics;

	public TrueTypeFont(String name)
	{
		this(name, 18);
	}

	public TrueTypeFont(String name, int size)
	{
		this(name, size, PLAIN);
	}

	public TrueTypeFont(String name, int size, int style)
	{
		this(name, size, style, true);
	}

	public TrueTypeFont(String name, int size, int style, boolean antialias)
	{
		this(new Font(name, style, size), antialias);
	}

	public TrueTypeFont(Font awtFont)
	{
		this(awtFont, true);
	}

	public TrueTypeFont(TrueTypeFont font)
	{
		this(font.awtFont, font.antialias);
	}

	public TrueTypeFont(Font awtFont, boolean antialias)
	{
		this.awtFont = awtFont;
		this.antialias = antialias;
		size = awtFont.getSize();
		style = awtFont.getStyle();
		createCharSet();
	}

	public TrueTypeFont(InputStream is)
	{
		this(is, 18);
	}

	public TrueTypeFont(InputStream is, int size)
	{
		this(is, size, PLAIN);
	}

	public TrueTypeFont(InputStream is, int size, int style)
	{
		this(is, size, style, true);
	}

	public TrueTypeFont(InputStream is, int size, int style, boolean antialias)
	{
		try
		{
			this.awtFont = Font.createFont(Font.TRUETYPE_FONT, is);
			this.antialias = antialias;
			awtFont = awtFont.deriveFont(style, size);
			this.size = awtFont.getSize();
			this.style = awtFont.getStyle();
			createCharSet();
		}
		catch (Exception thrown)
		{
			Exceptions.Exception.reThrow(thrown);
		}
	}

	public void drawString(String text, float x, float y, float width, float height, Color color, Transform transform, Modifiers mod, ClippingBatch batch)
	{
		switch (mod)
		{
			case CENTER:
				drawRightToLeft(text.substring(0, text.length() / 2), x, y, width / 2, height / 2, color, transform, batch);
				drawLeftToRight(text.substring(text.length() / 2), x, y, width / 2, height / 2, color, transform, batch);
				break;
			case LEFT:
				drawLeftToRight(text, x, y, width, height, color, transform, batch);
				break;
			case RIGHT:
				drawRightToLeft(text, x, y, width, height, color, transform, batch);
				break;
		}
	}

	private void drawLeftToRight(String text, float x, float y, float width, float height, Color color, Transform transform, ClippingBatch batch)
	{
		if (batch == null || transform == null || color == null || text == null || (text = text.replace(TRAILING_WHITESPACE, "")).equals(""))
		{
			return;
		}
		Texture current = Texture.CURRENT;
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.TRIANGLES);
		{
			Vector3 tVec = Vector3.REUSABLE_STACK.pop();
			float startx = x;
			Texture page = null;
			for (char ch : text.toCharArray())
			{
				if (ch == '\n')
				{
					x = startx;
					y += fontMetrics.getHeight();
					if (height > -1 && y > height)
					{
						break;
					}
					continue;
				}

				FontChar fchar = fontChars[(int) ch];

				if (page == null || page != fontPages[fchar.page])
				{
					batch.flush();
					page = fontPages[fchar.page];
					page.bind();
				}

				float minx = x - fchar.padding;
				float maxx = x + fchar.w - fchar.padding;
				float miny = y;
				float maxy = y + fchar.h;

				x += fchar.advance;

				if (width > -1 && x > startx + width)
				{
					continue;
				}

				float minu = fchar.x / page.getWidth();
				float maxu = (fchar.x + fchar.w) / page.getWidth();
				float minv = fchar.y / page.getHeight();
				float maxv = (fchar.y + fchar.h) / page.getHeight();

				batch.flushOnOverflow(6);

				batch.vertex(tVec.set(minx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, minv);

				batch.vertex(tVec.set(maxx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, minv);

				batch.vertex(tVec.set(maxx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, maxv);

				batch.vertex(tVec.set(minx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, minv);

				batch.vertex(tVec.set(maxx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, maxv);

				batch.vertex(tVec.set(minx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, maxv);
			}
			Vector3.REUSABLE_STACK.push(tVec);
		}
		batch.end(true);
		current.bind();
	}

	private void drawRightToLeft(String text, float x, float y, float width, float height, Color color, Transform transform, ClippingBatch batch)
	{
		if (batch == null || transform == null || color == null || text == null || (text = text.replace(TRAILING_WHITESPACE, "")).equals(""))
		{
			return;
		}
		Texture current = Texture.CURRENT;
		batch.applyShaderProgram(Program.CURRENT);
		batch.begin(Primitive.TRIANGLES);
		{
			Vector3 tVec = Vector3.REUSABLE_STACK.pop();
			float startx = x;
			Texture page = null;
			for (int i = text.length() - 1; i > -1; i--)
			{
				char ch = text.charAt(i);

				if (ch == '\n')
				{
					x = startx;
					y += fontMetrics.getHeight();
					if (height > -1 && y > height)
					{
						break;
					}
					continue;
				}

				FontChar fchar = fontChars[(int) ch];

				if (page == null || page != fontPages[fchar.page])
				{
					batch.flush();
					page = fontPages[fchar.page];
					page.bind();
				}

				x -= fchar.advance;

				float minx = x - fchar.padding;
				float maxx = x + fchar.w - fchar.padding;
				float miny = y;
				float maxy = y + fchar.h;

				if (width > -1 && x < startx - width)
				{
					continue;
				}

				float minu = fchar.x / page.getWidth();
				float maxu = (fchar.x + fchar.w) / page.getWidth();
				float minv = fchar.y / page.getHeight();
				float maxv = (fchar.y + fchar.h) / page.getHeight();

				batch.flushOnOverflow(6);

				batch.vertex(tVec.set(minx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, minv);

				batch.vertex(tVec.set(maxx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, minv);

				batch.vertex(tVec.set(maxx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, maxv);

				batch.vertex(tVec.set(minx, miny, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, minv);

				batch.vertex(tVec.set(maxx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(maxu, maxv);

				batch.vertex(tVec.set(minx, maxy, 0f).multiply(transform.matrix));
				batch.color(color);
				batch.texCoord(minu, maxv);
			}
			Vector3.REUSABLE_STACK.push(tVec);
		}
		batch.end(true);
		current.bind();
	}

	public void drawString(String text, float x, float y, float width, float height, Color color, Transform transform, ClippingBatch batch)
	{
		drawLeftToRight(text, x, y, width, height, color, transform, batch);
	}

	public float getCharWidth(char character)
	{
		return fontChars[(int) character].advance;
	}

	public float getWidth(String text)
	{
		float width = 0;
		float lineWidth = 0;
		for (char ch : text.toCharArray())
		{
			if (ch == '\n')
			{
				width = MathUtils.max(width, lineWidth);
				lineWidth = 0;
				continue;
			}
			lineWidth += fontChars[(int) ch].advance;
		}
		width = MathUtils.max(width, lineWidth);
		return width;
	}

	public float getHeight()
	{
		return fontMetrics.getHeight();
	}

	public float getHeight(String text)
	{
		float height = fontMetrics.getHeight();
		for (char ch : text.toCharArray())
		{
			if (ch == '\n')
			{
				height += fontChars[(int) ch].h;
			}
		}
		return height;
	}

	public boolean isAntialias()
	{
		return antialias;
	}

	public int getSize()
	{
		return size;
	}

	public int getStyle()
	{
		return style;
	}

	public String getFontName()
	{
		return awtFont.getFamily();
	}

	public TrueTypeFont setSize(float size)
	{
		return setSizeAndStyle(size, awtFont.getStyle());
	}

	public TrueTypeFont setStyle(int style)
	{
		return setSizeAndStyle(awtFont.getSize(), style);
	}

	public TrueTypeFont setSizeAndStyle(float size, int style)
	{
		dispose();
		awtFont = awtFont.deriveFont(style, size);
		createCharSet();
		return this;
	}

	public TrueTypeFont derive(float size)
	{
		return new TrueTypeFont(awtFont.deriveFont(size));
	}

	public TrueTypeFont derive(int style)
	{
		return new TrueTypeFont(awtFont.deriveFont(style));
	}

	public TrueTypeFont derive(float size, int style)
	{
		return new TrueTypeFont(awtFont.deriveFont(style, size));
	}

	public void dispose()
	{
		for (Texture t : fontPages)
		{
			t.dispose();
		}
	}

	private void createCharSet()
	{
		final int maxWidth = 1024;
		final int maxHeight = 1024;

		BufferedImage tmp = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = tmp.createGraphics();
		g2d.setFont(awtFont);
		g2d.setColor(java.awt.Color.BLACK);
		if (antialias)
		{
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}
		fontMetrics = g2d.getFontMetrics();

		int xposition = 0;
		int yposition = 0;
		int page = 0;
		final int padding = fontMetrics.getMaxAdvance();
		List<Texture> pages = new ArrayList<>();

		for (int i = 0; i < STANDARD_CHARACTERS; i++)
		{
			char ch = (char) i;
			fontChars[i] = new FontChar();

			if (xposition + 2 * padding > maxWidth)
			{
				xposition = 0;
				yposition += fontMetrics.getHeight() + padding;
			}
			if (yposition + 2 * padding > maxHeight)
			{
				xposition = yposition = 0;
				page++;
			}

			fontChars[i].advance = fontMetrics.stringWidth("_" + ch) - fontMetrics.stringWidth("_");
			fontChars[i].padding = padding;
			fontChars[i].page = page;
			fontChars[i].x = xposition;
			fontChars[i].y = yposition;
			fontChars[i].w = fontChars[i].advance + (2 * padding);
			fontChars[i].h = fontMetrics.getHeight();

			xposition += fontChars[i].w + 10;
		}

		page = 0;
		for (int i = 0; i < STANDARD_CHARACTERS; i++)
		{
			FontChar fChar = fontChars[i];
			if (page != fChar.page)
			{
				g2d.dispose();
				pages.add(Texture.fromDirectBuffer(toBuffer(tmp), tmp.getWidth(), tmp.getHeight(), 4));
				tmp = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_ARGB);
				g2d = tmp.createGraphics();
				g2d.setFont(awtFont);
				g2d.setColor(java.awt.Color.BLACK);
				if (antialias)
				{
					g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				}
				page = fChar.page;
			}
			g2d.drawString(String.valueOf((char) i), fChar.x + padding, fChar.y + fontMetrics.getAscent());
		}
		g2d.dispose();
		pages.add(Texture.fromDirectBuffer(toBuffer(tmp), tmp.getWidth(), tmp.getHeight(), 4));
		fontPages = new Texture[pages.size()];
		fontPages = pages.toArray(fontPages);
	}

	private DirectBuffer toBuffer(BufferedImage image)
	{
		DirectBuffer buffer = DirectBuffer.create((image.getWidth() * image.getHeight()) * 4);
		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++)
			{
				int pixel = image.getRGB(x, y);
				buffer.nativeBuffer().put((byte) ((pixel >> 16) & 0xFF));
				buffer.nativeBuffer().put((byte) ((pixel >> 8) & 0xFF));
				buffer.nativeBuffer().put((byte) (pixel & 0xFF));
				buffer.nativeBuffer().put((byte) ((pixel >> 24) & 0xFF));
			}
		}
		buffer.nativeBuffer().rewind();
		return buffer;
	}

	private static class FontChar
	{
		public int x, y;
		public int w, h;
		public int advance, padding;
		public int page;
	}

	public enum Modifiers
	{
		LEFT,
		RIGHT,
		CENTER;
	}
}
