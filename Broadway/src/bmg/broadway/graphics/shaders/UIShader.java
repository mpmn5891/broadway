package bmg.broadway.graphics.shaders;

import bmg.broadway.graphics.cameras.Camera;
import bmg.emef.environment.Environment;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Shader;
import bmg.emef.io.FileHandle;

public class UIShader extends Program
{
	public UIShader()
	{
		Shader vert = new Shader(Shader.Type.VERTEX);
		vert.source(Environment.io.getFileReader().readTextFile(FileHandle.getInternalFile("shaders/clipping.vert")));
		vert.compile();
		Shader frag = new Shader(Shader.Type.FRAGMENT);
		frag.source(Environment.io.getFileReader().readTextFile(FileHandle.getInternalFile("shaders/default.frag")));
		frag.compile();
		attach(vert);
		attach(frag);
		link();
		vert.dispose();
		frag.dispose();
	}

	@Override
	public void prepareFrame()
	{
		setUniform("projection", Camera.CURRENT.getProjection());
		setUniform("view", Camera.CURRENT.getView());
	}
}
