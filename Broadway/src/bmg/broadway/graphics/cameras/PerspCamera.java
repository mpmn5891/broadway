package bmg.broadway.graphics.cameras;

import bmg.emef.environment.Environment;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.math.Matrix4;
import bmg.emef.math.Quaternion;
import bmg.emef.math.Transforms;
import bmg.emef.math.Vector3;
import bmg.emef.util.MathUtils;

public class PerspCamera extends Camera
{
	private Matrix4 projection;
	private Matrix4 view;

	private Quaternion rotation;
	private Vector3 position;

	private Vector3 forward;
	private Vector3 right;
	private Vector3 up;

	private float angleX;
	private float angleY;
	private float angleZ;

	private float xAngleLimit = -1;
	private float yAngleLimit = -1;
	private float zAngleLimit = -1;

	private boolean hasChanged = false;

	public PerspCamera()
	{
		this(70, Environment.display.getAspectRatio(), 0.01f, 100f);
	}

	public PerspCamera(float fov, float aspect, float zNear, float zFar)
	{
		this.projection = Transforms.createPerspective(fov, aspect, zNear, zFar, new Matrix4());
		this.view = new Matrix4();
		this.position = new Vector3(0, 0, 0);
		this.rotation = new Quaternion();
		this.forward = new Vector3();
		this.right = new Vector3();
		this.up = new Vector3();
	}

	public PerspCamera(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		this.projection = Transforms.createFrustum(left, right, bottom, top, zNear, zFar, new Matrix4());
		this.view = new Matrix4();
		this.position = new Vector3();
		this.rotation = new Quaternion();
		this.forward = new Vector3();
		this.right = new Vector3();
		this.up = new Vector3();
	}

	public PerspCamera initProjection(float fov, float aspect, float zNear, float zFar)
	{
		hasChanged = true;
		Transforms.createPerspective(fov, aspect, zNear, zFar, projection);
		return this;
	}

	public PerspCamera initProjection(float width, float height)
	{
		return initProjection(0, width, height, 0, 0.01f, 100f);
	}

	public PerspCamera initProjection(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		hasChanged = true;
		Transforms.createFrustum(left, right, bottom, top, zNear, zFar, projection);
		return this;
	}

	public void apply()
	{
		if (hasChanged)
		{
			Vector3 tempVec = Vector3.REUSABLE_STACK.pop();
			Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();
			Quaternion tempQua = Quaternion.REUSABLE_STACK.pop();

			view.initIdentity()
					.multiply(Transforms.createRotation(tempQua.set(rotation).invert(), tempMat))
					.multiply(Transforms.createTranslation(tempVec.set(position).negate(), tempMat));

			Quaternion.REUSABLE_STACK.push(tempQua);
			Vector3.REUSABLE_STACK.push(tempVec);
			Matrix4.REUSABLE_STACK.push(tempMat);
		}

		GLContext.enable(Graphics.Constants.GL_DEPTH_TEST);

		Camera.CURRENT = this;
		hasChanged = false;
	}

	@Override
	public Vector3 projectVector(Vector3 dest)
	{
		if (dest == null)
		{
			dest = new Vector3();
		}
		if (hasChanged)
		{
			Vector3 tempVec = Vector3.REUSABLE_STACK.pop();
			Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();
			Quaternion tempQua = Quaternion.REUSABLE_STACK.pop();

			view.initIdentity()
					.multiply(Transforms.createRotation(tempQua.set(rotation).invert(), tempMat))
					.multiply(Transforms.createTranslation(tempVec.set(position).negate(), tempMat));

			Quaternion.REUSABLE_STACK.push(tempQua);
			Vector3.REUSABLE_STACK.push(tempVec);
			Matrix4.REUSABLE_STACK.push(tempMat);
			hasChanged = false;
		}
		Matrix4 combined = Matrix4.REUSABLE_STACK.pop().set(projection).multiply(view);
		dest.multiply(combined);
		Matrix4.REUSABLE_STACK.push(combined);
		return dest;
	}

	@Override
	public Vector3 unprojectVector(Vector3 dest)
	{
		if (dest == null)
		{
			dest = new Vector3();
		}
		if (hasChanged)
		{
			Vector3 tempVec = Vector3.REUSABLE_STACK.pop();
			Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();
			Quaternion tempQua = Quaternion.REUSABLE_STACK.pop();

			view.initIdentity()
					.multiply(Transforms.createRotation(tempQua.set(rotation).invert(), tempMat))
					.multiply(Transforms.createTranslation(tempVec.set(position).negate(), tempMat));

			Quaternion.REUSABLE_STACK.push(tempQua);
			Vector3.REUSABLE_STACK.push(tempVec);
			Matrix4.REUSABLE_STACK.push(tempMat);
			hasChanged = false;
		}
		Matrix4 invert = Matrix4.REUSABLE_STACK.pop();
		invert.set(projection).multiply(view).invert();
		dest.multiply(invert);
		Matrix4.REUSABLE_STACK.push(invert);
		return dest;
	}

	public PerspCamera move(Vector3 direction, float amount)
	{
		hasChanged = true;
		position.add(direction.normalize().scale(amount));
		return this;
	}

	public PerspCamera lookAt(Vector3 point)
	{
		return lookAt(point, getUp().normalize());
	}

	public PerspCamera lookAt(Vector3 point, Vector3 up)
	{
		hasChanged = true;
		Transforms.createLookAtQuaternion(position, point, up, rotation);
		return this;
	}

	public PerspCamera moveForward(float amount)
	{
		return move(getForward(), amount);
	}

	public PerspCamera moveBackward(float amount)
	{
		return move(getForward().negate(), amount);
	}

	public PerspCamera moveLeft(float amount)
	{
		return move(getRight().negate(), amount);
	}

	public PerspCamera moveRight(float amount)
	{
		return move(getRight(), amount);
	}

	public PerspCamera moveUp(float amount)
	{
		return move(getUp(), amount);
	}

	public PerspCamera moveDown(float amount)
	{
		return move(getUp().negate(), amount);
	}

	public PerspCamera rotateX(float angle)
	{
		hasChanged = true;

		angleX += angle;

		angleX = xAngleLimit > -1 ? MathUtils.clamp(angleX, -xAngleLimit, xAngleLimit) : angleX;

		rotation.set(angleX, angleY, angleZ);

		return this;
	}

	public PerspCamera rotateY(float angle)
	{
		hasChanged = true;

		angleY += angle;

		angleY = yAngleLimit > -1 ? MathUtils.clamp(angleY, -yAngleLimit, yAngleLimit) : angleY;

		rotation.set(angleX, angleY, angleZ);

		return this;
	}

	public PerspCamera rotateZ(float angle)
	{
		hasChanged = true;

		angleZ += angle;

		angleZ = zAngleLimit > -1 ? MathUtils.clamp(angleZ, -zAngleLimit, zAngleLimit) : angleZ;

		rotation.set(angleX, angleY, angleZ);

		return this;
	}

	public PerspCamera lerp(PerspCamera other, float alpha)
	{
		hasChanged = true;

		position.lerp(other.position, alpha);
		rotation.lerp(other.rotation, alpha);

		return this;
	}

	public PerspCamera slerp(PerspCamera other, float alpha)
	{
		hasChanged = true;

		position.lerp(other.position, alpha);
		rotation.slerp(other.rotation, alpha);

		return this;
	}

	public PerspCamera limitAxisX(float maxAngle)
	{
		xAngleLimit = maxAngle;

		return this;
	}

	public PerspCamera limitAxisY(float maxAngle)
	{
		yAngleLimit = maxAngle;

		return this;
	}

	public PerspCamera limitAxisZ(float maxAngle)
	{
		zAngleLimit = maxAngle;

		return this;
	}

	@Override
	public Matrix4 getProjection()
	{
		return projection;
	}

	@Override
	public Matrix4 getView()
	{
		return view;
	}

	public Quaternion getRotation()
	{
		return rotation;
	}

	public Vector3 getPosition()
	{
		return position;
	}

	public Vector3 getForward()
	{
		return rotation.multiply(forward.set(Vector3.AXIS_Z).negate(), forward);
	}

	public Vector3 getRight()
	{
		return rotation.multiply(right.set(Vector3.AXIS_X), right);
	}

	public Vector3 getUp()
	{
		return rotation.multiply(Vector3.AXIS_Y, up);
	}

	public float getAngleX()
	{
		return angleX;
	}

	public float getAngleY()
	{
		return angleY;
	}

	public float getAngleZ()
	{
		return angleZ;
	}

	public PerspCamera setRotation(Quaternion rotation)
	{
		this.rotation = rotation;

		return this;
	}

	public PerspCamera setPosition(Vector3 position)
	{
		this.position = position;

		return this;
	}
}
