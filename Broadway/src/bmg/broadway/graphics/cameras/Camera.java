package bmg.broadway.graphics.cameras;

import bmg.broadway.util.Ray;
import bmg.emef.environment.Environment;
import bmg.emef.math.Matrix4;
import bmg.emef.math.Vector3;

public abstract class Camera
{
	public static Camera CURRENT;

	public abstract void apply();

	public abstract Matrix4 getProjection();

	public abstract Matrix4 getView();

	public abstract Vector3 projectVector(Vector3 dest);

	public abstract Vector3 unprojectVector(Vector3 dest);

	public Vector3 unprojectScreenPoint(float x, float y, Vector3 dest)
	{
		if (dest == null)
		{
			dest = new Vector3();
		}

		dest.set(((2.0f * x) / Environment.display.getWidth()) - 1.0f, 1.0f - ((2.0f * y) / Environment.display.getHeight()), 1.0f);
		unprojectVector(dest);

		return dest;
	}

	public Ray calculateRayFromScreenPoint(float x, float y, Ray dest)
	{
		if (dest == null)
		{
			dest = new Ray();
		}

		return dest;
	}
}
