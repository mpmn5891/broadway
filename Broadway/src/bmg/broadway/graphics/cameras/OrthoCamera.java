package bmg.broadway.graphics.cameras;

import bmg.emef.environment.Environment;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.math.Matrix4;
import bmg.emef.math.Transforms;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class OrthoCamera extends Camera
{
	private Matrix4 projection, view;
	private float width, height;

	public OrthoCamera()
	{
		this(Environment.display.getWidth(), Environment.display.getHeight());
	}

	public OrthoCamera(float width, float height)
	{
		this(0, width, height, 0);
	}

	public OrthoCamera(float left, float right, float bottom, float top)
	{
		width = left - right;
		height = bottom - top;
		projection = Transforms.createOrtho2d(left, right, bottom, top, 0, 100, new Matrix4());
		view = new Matrix4().initIdentity();
	}

	public OrthoCamera initProjection(float width, float height)
	{
		return initProjection(0, width, height, 0);
	}

	public OrthoCamera initProjection(float left, float right, float bottom, float top)
	{
		width = left - right;
		height = bottom - top;
		Transforms.createOrtho2d(left, right, bottom, top, 0, 100, projection);
		view.initIdentity();
		return this;
	}

	@Override
	public Vector3 projectVector(Vector3 dest)
	{
		Matrix4 combined = Matrix4.REUSABLE_STACK.pop().set(projection).multiply(view);
		dest.multiply(combined);
		Matrix4.REUSABLE_STACK.push(combined);
		return dest;
	}

	@Override
	public Vector3 unprojectVector(Vector3 dest)
	{
		Matrix4 invert = Matrix4.REUSABLE_STACK.pop();
		invert.set(projection).multiply(view).invert();
		dest.multiply(invert);
		Matrix4.REUSABLE_STACK.push(invert);
		return dest;
	}

	public OrthoCamera translate(Vector2 v)
	{
		return translate(v.x, v.y);
	}

	public OrthoCamera translate(float x, float y)
	{
		Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();
		Vector3 tempVec = Vector3.REUSABLE_STACK.pop();

		view.multiply(Transforms.createTranslation(tempVec.set(x, y, 0), tempMat));

		Matrix4.REUSABLE_STACK.push(tempMat);
		Vector3.REUSABLE_STACK.push(tempVec);
		return this;
	}

	public OrthoCamera translateTo(Vector2 v)
	{
		return translateTo(v.x, v.y);
	}

	public OrthoCamera translateTo(float x, float y)
	{
		Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();
		Vector3 tempVec = Vector3.REUSABLE_STACK.pop();

		view.initIdentity().multiply(Transforms.createTranslation(tempVec.set(x, y, 0), tempMat));

		Matrix4.REUSABLE_STACK.push(tempMat);
		Vector3.REUSABLE_STACK.push(tempVec);
		return this;
	}

	public OrthoCamera center(Vector2 v)
	{
		return center(v.x, v.y);
	}

	public OrthoCamera center(float x, float y)
	{
		view.initIdentity();
		x = (width / 2) - x;
		y = (height / 2) - y;
		return translate(x, y);
	}

	public OrthoCamera rotate(Vector3 axis, float angle)
	{
		Matrix4 tempMat = Matrix4.REUSABLE_STACK.pop();

		view.multiply(Transforms.createRotation(axis, angle, tempMat));

		Matrix4.REUSABLE_STACK.push(tempMat);
		return this;
	}

	public void apply()
	{
		GLContext.disable(Graphics.Constants.GL_DEPTH_TEST);

		Camera.CURRENT = this;
	}

	@Override
	public Matrix4 getProjection()
	{
		return projection;
	}

	@Override
	public Matrix4 getView()
	{
		return view;
	}
}
