package bmg.broadway.graphics.cameras;

import java.util.List;

import bmg.broadway.geometry.Polygon;
import bmg.broadway.geometry.Polyhedron;
import bmg.emef.math.Matrix4;
import bmg.emef.math.Plane;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;
import bmg.emef.util.MathUtils;

public class Frustum
{
	public static final int PLANE_LEFT = 0,
			PLANE_RIGHT = 1,
			PLANE_TOP = 2,
			PLANE_BOTTOM = 3,
			PLANE_NEAR = 4,
			PLANE_FAR = 5;

	public static final int CORNER_TOP_LEFT_FAR = 0,
			CORNER_TOP_RIGHT_FAR = 1,
			CORNER_TOP_RIGHT_NEAR = 2,
			CORNER_TOP_LEFT_NEAR = 3,
			CORNER_BOTTOM_LEFT_FAR = 4,
			CORNER_BOTTOM_RIGHT_FAR = 5,
			CORNER_BOTTOM_RIGHT_NEAR = 6,
			CORNER_BOTTOM_LEFT_NEAR = 7;

	public static final int CORNER_2D_TOP_LEFT = 0,
			CORNER_2D_TOP_RIGHT = 1,
			CORNER_2D_BOTTOM_LEFT = 2,
			CORNER_2D_BOTTOM_RIGHT = 3;

	private Plane[] planes;
	private Matrix4 matrix;
	private Vector3[] corners;
	private Vector2[] corners2d;
	private Polygon polygon;
	private Polyhedron polyhedron;

	public Frustum()
	{
		planes = new Plane[6];
		for (int i = 0; i < planes.length; i++)
		{
			planes[i] = new Plane();
		}

		matrix = new Matrix4().initIdentity();

		corners = new Vector3[8];
		for (int i = 0; i < corners.length; i++)
		{
			corners[i] = new Vector3();
		}

		polygon = new Polygon();
		corners2d = new Vector2[4];
		for (int i = 0; i < corners2d.length; i++)
		{
			corners2d[i] = new Vector2();
			polygon.addVertex(corners2d[i]);
		}

		polyhedron = new Polyhedron();
		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_NEAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_NEAR]);
		polyhedron.addVertex(corners[CORNER_TOP_LEFT_NEAR]);
		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_NEAR]);

		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_NEAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_NEAR]);
		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_FAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_FAR]);

		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_FAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_FAR]);
		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_FAR]);
		polyhedron.addVertex(corners[CORNER_TOP_LEFT_FAR]);

		polyhedron.addVertex(corners[CORNER_TOP_LEFT_FAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_FAR]);
		polyhedron.addVertex(corners[CORNER_TOP_LEFT_NEAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_NEAR]);

		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_NEAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_LEFT_FAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_NEAR]);
		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_FAR]);

		polyhedron.addVertex(corners[CORNER_BOTTOM_RIGHT_FAR]);
		polyhedron.addVertex(corners[CORNER_TOP_LEFT_NEAR]);

		polyhedron.addVertex(corners[CORNER_TOP_LEFT_NEAR]);
		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_NEAR]);
		polyhedron.addVertex(corners[CORNER_TOP_LEFT_FAR]);
		polyhedron.addVertex(corners[CORNER_TOP_RIGHT_FAR]);
	}

	public Frustum update(Camera camera)
	{
		return update(camera.getProjection(), camera.getView());
	}

	public Frustum update(Matrix4 projection, Matrix4 view)
	{
		// set matrix
		matrix.set(projection).multiply(view);

		// calculate planes
		planes[PLANE_LEFT].set(matrix.get(0, 3) + matrix.get(0, 0),
				matrix.get(1, 3) + matrix.get(1, 0),
				matrix.get(2, 3) + matrix.get(2, 0),
				matrix.get(3, 3) + matrix.get(3, 0));
		planes[PLANE_RIGHT].set(matrix.get(0, 3) - matrix.get(0, 0),
				matrix.get(1, 3) - matrix.get(1, 0),
				matrix.get(2, 3) - matrix.get(2, 0),
				matrix.get(3, 3) - matrix.get(3, 0));
		planes[PLANE_TOP].set(matrix.get(0, 3) - matrix.get(0, 1),
				matrix.get(1, 3) - matrix.get(1, 1),
				matrix.get(2, 3) - matrix.get(2, 1),
				matrix.get(3, 3) - matrix.get(3, 1));
		planes[PLANE_BOTTOM].set(matrix.get(0, 3) + matrix.get(0, 1),
				matrix.get(1, 3) + matrix.get(1, 1),
				matrix.get(2, 3) + matrix.get(2, 1),
				matrix.get(3, 3) + matrix.get(3, 1));
		planes[PLANE_NEAR].set(matrix.get(0, 3) + matrix.get(0, 2),
				matrix.get(1, 3) + matrix.get(1, 2),
				matrix.get(2, 3) + matrix.get(2, 2),
				matrix.get(3, 3) + matrix.get(3, 2));
		planes[PLANE_FAR].set(matrix.get(0, 3) - matrix.get(0, 2),
				matrix.get(1, 3) - matrix.get(1, 2),
				matrix.get(2, 3) - matrix.get(2, 2),
				matrix.get(3, 3) - matrix.get(3, 2));

		// calculate corners
		Plane.intersection(planes[PLANE_TOP], planes[PLANE_LEFT], planes[PLANE_FAR], corners[CORNER_TOP_LEFT_FAR]);
		Plane.intersection(planes[PLANE_TOP], planes[PLANE_LEFT], planes[PLANE_NEAR], corners[CORNER_TOP_LEFT_NEAR]);
		Plane.intersection(planes[PLANE_TOP], planes[PLANE_RIGHT], planes[PLANE_FAR], corners[CORNER_TOP_RIGHT_FAR]);
		Plane.intersection(planes[PLANE_TOP], planes[PLANE_RIGHT], planes[PLANE_NEAR], corners[CORNER_TOP_RIGHT_NEAR]);

		Plane.intersection(planes[PLANE_BOTTOM], planes[PLANE_LEFT], planes[PLANE_FAR], corners[CORNER_BOTTOM_LEFT_FAR]);
		Plane.intersection(planes[PLANE_BOTTOM], planes[PLANE_LEFT], planes[PLANE_NEAR], corners[CORNER_BOTTOM_LEFT_NEAR]);
		Plane.intersection(planes[PLANE_BOTTOM], planes[PLANE_RIGHT], planes[PLANE_FAR], corners[CORNER_BOTTOM_RIGHT_FAR]);
		Plane.intersection(planes[PLANE_BOTTOM], planes[PLANE_RIGHT], planes[PLANE_NEAR], corners[CORNER_BOTTOM_RIGHT_NEAR]);

		// calculate 2d corners
		corners2d[CORNER_2D_TOP_LEFT].set(corners[CORNER_TOP_LEFT_NEAR].x, corners[CORNER_TOP_LEFT_NEAR].y);
		corners2d[CORNER_2D_TOP_RIGHT].set(corners[CORNER_TOP_RIGHT_NEAR].x, corners[CORNER_TOP_RIGHT_NEAR].y);
		corners2d[CORNER_2D_BOTTOM_LEFT].set(corners[CORNER_BOTTOM_LEFT_NEAR].x, corners[CORNER_BOTTOM_LEFT_NEAR].y);
		corners2d[CORNER_2D_BOTTOM_RIGHT].set(corners[CORNER_BOTTOM_RIGHT_NEAR].x, corners[CORNER_BOTTOM_RIGHT_NEAR].y);

		return this;
	}

	public boolean intersects(Vector3 position, float radius)
	{
		if (isInside(position))
		{
			return true;
		}

		for (Plane plane : planes)
		{
			if (plane.normal.dot(position) + radius + plane.d < 0)
			{
				return false;
			}
		}
		return true;
	}

	public boolean intersects(Vector3 position, float width, float height, float thickness)
	{
		if (isInside(position))
		{
			return true;
		}

		float halfWidth = width / 2;
		float halfHeight = height / 2;
		float halfThickness = thickness / 2;
		float x = position.x;
		float y = position.y;
		float z = position.z;

		for (Plane plane : planes)
		{
			if (plane.testPoint(x + halfWidth, y + halfHeight, z + halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x + halfWidth, y + halfHeight, z - halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x + halfWidth, y - halfHeight, z + halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x + halfWidth, y - halfHeight, z - halfThickness) == Plane.Side.BACK &&

					plane.testPoint(x - halfWidth, y + halfHeight, z + halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x - halfWidth, y + halfHeight, z - halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x - halfWidth, y - halfHeight, z + halfThickness) == Plane.Side.BACK &&
					plane.testPoint(x - halfWidth, y - halfHeight, z - halfThickness) == Plane.Side.BACK)
			{
				return false;
			}
		}
		return true;
	}

	public boolean intersects(Polygon poly)
	{
		Vector2 center = poly.getPosition();

		if (isInside(center.x, center.y, 0))
		{
			return true;
		}

		for (Vector2 v : poly.getVertices())
		{
			if (isInside(v.x + poly.getPosition().x, v.y + poly.getPosition().y, planes[PLANE_NEAR].d))
			{
				return true;
			}
		}

		return testPolygonCollision(polygon, poly);
	}

	public boolean intersects(Polyhedron poly)
	{
		Vector3 position = poly.getPosition();

		if (isInside(position))
		{
			return true;
		}

		if (intersects(position, poly.getWidth(), poly.getHeight(), poly.getThickness()))
		{
			for (Vector3 v : poly.getVertices())
			{
				if (isInside(v.x + position.x, v.y + position.y, v.z + position.z))
				{
					return true;
				}
			}

			return testPolyhedronCollision(polyhedron, poly);
		}
		return false;
	}

	public boolean isInside(float x, float y, float z)
	{
		boolean inside = false;

		for (Plane plane : planes)
		{
			if (!(inside = plane.testPoint(x, y, z) == Plane.Side.FRONT))
			{
				break;
			}
		}

		return inside;
	}

	public boolean isInside(Vector3 point)
	{
		return isInside(point.x, point.y, point.z);
	}

	public boolean isInside(Vector3 point, float radius)
	{
		if (!isInside(point))
		{
			return false;
		}

		for (Plane plane : planes)
		{
			float m = plane.normal.dot(point);
			float n = radius * MathUtils.abs(point.x - radius) +
					radius * MathUtils.abs(point.y - radius) +
					radius * MathUtils.abs(point.z - radius);

			if (m + n < 0)
			{
				return false;
			}
		}

		return true;
	}

	public boolean isInside(Vector3 point, float width, float height, float thickness)
	{
		if (!isInside(point))
		{
			return false;
		}

		for (Plane plane : planes)
		{
			float m = plane.normal.dot(point);
			float n = width / 2 * MathUtils.abs(point.x - width / 2) +
					height / 2 * MathUtils.abs(point.y - height / 2) +
					thickness / 2 * MathUtils.abs(point.z - thickness / 2);

			if (m + n < 0)
			{
				return false;
			}
		}

		return true;
	}

	public boolean isInside(Polygon poly)
	{
		if (!isInside(poly.getPosition().x, poly.getPosition().y, planes[PLANE_NEAR].d))
		{
			return false;
		}

		Vector3 tVec = Vector3.REUSABLE_STACK.pop();

		boolean inside = false;
		for (Vector2 v : poly.getVertices())
		{
			tVec.set(v.x, v.y, planes[PLANE_NEAR].d).add(poly.getPosition(), 0);
			if (!(inside = isInside(tVec)))
			{
				break;
			}
		}

		Vector3.REUSABLE_STACK.push(tVec);

		return inside;
	}

	public boolean isInside(Polyhedron poly)
	{
		if (!isInside(poly.getPosition()))
		{
			return false;
		}

		Vector3 tVec = Vector3.REUSABLE_STACK.pop();

		boolean inside = false;
		for (Vector3 v : poly.getVertices())
		{
			tVec.set(v).add(poly.getPosition());
			if (!(inside = isInside(tVec)))
			{
				break;
			}
		}

		Vector3.REUSABLE_STACK.push(tVec);

		return inside;
	}

	public Plane getPlane(int id)
	{
		return planes[id];
	}

	public Vector3 getCorner(int id)
	{
		return corners[id];
	}

	public Vector2 getCorner2D(int id)
	{
		return corners2d[id];
	}

	public Polygon getPolygon()
	{
		return polygon;
	}

	public Polyhedron getPolyhedron()
	{
		return polyhedron;
	}

	@Override
	public String toString()
	{
		return "Frustum{ left"
				+ planes[PLANE_LEFT].toString() + "\n right"
				+ planes[PLANE_RIGHT].toString() + "\n top"
				+ planes[PLANE_TOP].toString() + "\n bottom"
				+ planes[PLANE_BOTTOM].toString() + "}";
	}

	private boolean testPolygonCollision(Polygon a, Polygon b)
	{
		Vector2 tVec = Vector2.REUSABLE_STACK.pop();

		for (int i = 0; i < a.vertexCount(); i++)
		{
			Vector2 e1 = a.getVertex(i);
			Vector2 e2 = a.getVertex((i + 1) % a.vertexCount());

			tVec.set(e2).subtract(e1).perpendicular().normalize();

			if (isSeparatingAxis(a, b, tVec))
			{
				Vector2.REUSABLE_STACK.push(tVec);
				return false;
			}
		}

		for (int i = 0; i < b.vertexCount(); i++)
		{
			Vector2 e1 = b.getVertex(i);
			Vector2 e2 = b.getVertex((i + 1) % b.vertexCount());

			tVec.set(e2).subtract(e1).perpendicular().normalize();
			if (isSeparatingAxis(a, b, tVec))
			{
				Vector2.REUSABLE_STACK.push(tVec);
				return false;
			}
		}

		Vector2.REUSABLE_STACK.push(tVec);

		return true;
	}

	private boolean testPolyhedronCollision(Polyhedron a, Polyhedron b)
	{
		Vector3 axis = Vector3.REUSABLE_STACK.pop();
		Vector3 edge1 = Vector3.REUSABLE_STACK.pop();
		Vector3 edge2 = Vector3.REUSABLE_STACK.pop();

		Vector3 v1, v2, v3;

		for (int v = 0; v < a.vertexCount() - 2; v++)
		{
			if ((v & 1) != 0)
			{
				v1 = a.getVertex(v);
				v2 = a.getVertex(v + 1);
				v3 = a.getVertex(v + 2);
			}
			else
			{
				v1 = a.getVertex(v);
				v2 = a.getVertex(v + 2);
				v3 = a.getVertex(v + 1);
			}

			edge1.set(v2).add(a.getPosition()).subtract(v1);
			edge2.set(v3).add(a.getPosition()).subtract(v1);
			axis.set(edge1).cross(edge2).normalize();

			if (axis.lengthSquared() == 0)
			{
				continue;
			}

			if (isSeparatingAxis(a, b, axis))
			{
				Vector3.REUSABLE_STACK.push(axis, edge1, edge2);
				return false;
			}
		}

		for (int v = 0; v < b.vertexCount() - 2; v++)
		{
			if ((v & 1) != 0)
			{
				v1 = b.getVertex(v);
				v2 = b.getVertex(v + 1);
				v3 = b.getVertex(v + 2);
			}
			else
			{
				v1 = b.getVertex(v);
				v2 = b.getVertex(v + 2);
				v3 = b.getVertex(v + 1);
			}

			edge1.set(v2).add(b.getPosition()).subtract(v1);
			edge2.set(v3).add(b.getPosition()).subtract(v1);
			axis.set(edge1).cross(edge2).normalize();

			if (axis.lengthSquared() == 0)
			{
				continue;
			}

			if (isSeparatingAxis(a, b, axis))
			{
				Vector3.REUSABLE_STACK.push(axis, edge1, edge2);
				return false;
			}
		}

		Vector3.REUSABLE_STACK.push(axis, edge1, edge2);

		return true;
	}

	private boolean isSeparatingAxis(Polygon a, Polygon b, Vector2 axis)
	{
		Vector2 offset = Vector2.REUSABLE_STACK.pop();
		Vector2 aRange = Vector2.REUSABLE_STACK.pop();
		Vector2 bRange = Vector2.REUSABLE_STACK.pop();

		offset.set(b.getPosition()).subtract(a.getPosition());

		float projectedOffset = offset.dot(axis);

		flattenPoints(a.getVertices(), axis, aRange);
		flattenPoints(b.getVertices(), axis, bRange);

		bRange.add(projectedOffset, projectedOffset);

		if (aRange.x > bRange.y || bRange.x > aRange.y)
		{
			Vector2.REUSABLE_STACK.push(offset, aRange, bRange);
			return true;
		}

		Vector2.REUSABLE_STACK.push(offset, aRange, bRange);

		return false;
	}

	private boolean isSeparatingAxis(Polyhedron a, Polyhedron b, Vector3 axis)
	{
		Vector3 offset = Vector3.REUSABLE_STACK.pop();
		Vector2 aRange = Vector2.REUSABLE_STACK.pop();
		Vector2 bRange = Vector2.REUSABLE_STACK.pop();

		offset.set(b.getPosition()).subtract(a.getPosition());
		float projectedOffset = offset.dot(axis);

		flattenPoints(a.getVertices(), axis, aRange);
		flattenPoints(b.getVertices(), axis, bRange);

		bRange.add(projectedOffset, projectedOffset);

		if (aRange.x > bRange.y || bRange.x > aRange.y)
		{
			Vector3.REUSABLE_STACK.push(offset);
			Vector2.REUSABLE_STACK.push(aRange, bRange);
			return true;
		}

		Vector3.REUSABLE_STACK.push(offset);
		Vector2.REUSABLE_STACK.push(aRange, bRange);

		return false;
	}

	private Vector2 flattenPoints(List<Vector2> vertices, Vector2 normal, Vector2 projection)
	{
		float min = Float.MAX_VALUE;
		float max = Float.MIN_VALUE;

		for (Vector2 vertex : vertices)
		{
			float dot = vertex.dot(normal);
			if (dot < min)
			{
				min = dot;
			}
			if (dot > max)
			{
				max = dot;
			}
		}

		return projection.set(min, max);
	}

	private Vector2 flattenPoints(List<Vector3> vertices, Vector3 axis, Vector2 projection)
	{
		float min = axis.dot(vertices.get(0));
		float max = min;

		for (Vector3 v : vertices)
		{
			float dot = axis.dot(v);
			if (dot < min)
			{
				min = dot;
			}
			if (dot > max)
			{
				max = dot;
			}
		}

		return projection.set(min, max);
	}
}
