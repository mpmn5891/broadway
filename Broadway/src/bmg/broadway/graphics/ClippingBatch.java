package bmg.broadway.graphics;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.Graphics.Primitive;
import bmg.emef.graphics.gl.ArrayObject;
import bmg.emef.graphics.gl.BufferObject;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.graphics.gl.Program;
import bmg.emef.math.Plane;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;
import bmg.emef.math.Vector4;
import bmg.emef.util.MathUtils;

public class ClippingBatch
{
	private Primitive renderPrimitive;
	private int batchSize, maxBatchSize, vertexCount, normalCount, colorCount, textureCount;
	private boolean active = false;

	private Plane clipPlaneLeft, clipPlaneRight, clipPlaneTop, clipPlaneBottom;

	private float[] vertexData, normalData, colorData, textureData, clipLeftData, clipRightData, clipTopData, clipBottomData;

	private ArrayObject vao;
	private BufferObject vboVertex, vboNormal, vboColor, vboTexture, vboClipLeft, vboClipRight, vboClipTop, vboClipBottom;

	private int vertexLocation, normalLocation, colorLocation, textureLocation, clipLeftLocation, clipRightLocation, clipTopLocation, clipBottomLocation;

	private Graphics2d graphics2d;

	private String debugLine = "";
	private int numSkips = 0;

	public ClippingBatch()
	{
		this(4096, 1024 * 1024);
	}

	public ClippingBatch(int startingSize, int maxSize)
	{
		if (maxSize < startingSize)
		{
			throw new Exceptions.Exception("Max size [" + maxSize + "] does not allow starting size of [" + startingSize + "]");
		}

		clipPlaneLeft = new Plane().set(1, 0, 0, 0);
		clipPlaneRight = new Plane().set(-1, 0, 0, Environment.display.getWidth());
		clipPlaneTop = new Plane().set(0, 1, 0, 0);
		clipPlaneBottom = new Plane().set(0, -1, 0, Environment.display.getHeight());

		vertexData = new float[startingSize * 4];
		normalData = new float[startingSize * 4];
		colorData = new float[startingSize * 1];
		textureData = new float[startingSize * 2];
		clipLeftData = new float[startingSize * 4];
		clipRightData = new float[startingSize * 4];
		clipTopData = new float[startingSize * 4];
		clipBottomData = new float[startingSize * 4];

		vao = new ArrayObject();
		vao.bind();
		vboVertex = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboNormal = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboColor = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboTexture = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboClipLeft = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboClipRight = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboClipTop = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vboClipBottom = new BufferObject(BufferObject.Target.ARRAY_BUFFER);

		vertexLocation = -1;
		normalLocation = -1;
		colorLocation = -1;
		textureLocation = -1;
		clipLeftLocation = -1;
		clipRightLocation = -1;
		clipTopLocation = -1;
		clipBottomLocation = -1;

		this.batchSize = startingSize;
		this.maxBatchSize = maxSize;
	}

	public Graphics2d createGraphics2d()
	{
		if (graphics2d == null)
		{
			graphics2d = new Graphics2d(this);
		}
		return graphics2d;
	}

	public void beginClip(Plane left, Plane right, Plane top, Plane bottom)
	{
		clipPlaneLeft = left == null ? clipPlaneLeft : left;
		clipPlaneRight = right == null ? clipPlaneRight : right;
		clipPlaneTop = top == null ? clipPlaneTop : top;
		clipPlaneBottom = bottom == null ? clipPlaneBottom : bottom;
	}

	public void endClip()
	{
		clipPlaneLeft.set(1, 0, 0, 0);
		clipPlaneRight.set(-1, 0, 0, Environment.display.getWidth());
		clipPlaneTop.set(0, 1, 0, 0);
		clipPlaneBottom.set(0, -1, 0, Environment.display.getHeight());
	}

	public void begin()
	{
		begin(Primitive.TRIANGLES);
	}

	public void begin(Primitive renderShape)
	{
		if (active)
		{
			if (!(this.renderPrimitive == renderShape))
			{
				debugLine += "    Primitive Changed to " + renderShape + '\n';
				end(true);
			}
		}
		if (!active)
		{
			Environment.log.debug("[" + this.getClass().getName() + '\n' + debugLine);
			debugLine = "";
			this.renderPrimitive = renderShape;
			vertexData = new float[batchSize * 4];
			normalData = new float[batchSize * 4];
			colorData = new float[batchSize * 4];
			textureData = new float[batchSize * 2];
			clipLeftData = new float[batchSize * 4];
			clipRightData = new float[batchSize * 4];
			clipTopData = new float[batchSize * 4];
			clipBottomData = new float[batchSize * 4];
			vertexCount = 0;
			normalCount = 0;
			colorCount = 0;
			textureCount = 0;
			active = true;
		}
	}

	public void end()
	{
		end(false);
	}

	public void end(boolean force)
	{
		if (force)
		{
			flush();
			active = false;
		}
	}

	public void flush()
	{
		if (vertexCount == 0 || vertexLocation == -1)
		{
			numSkips++;
			return;
		}

		debugLine += "    Skipped " + numSkips + '\n';
		numSkips = 0;

		fillEmptySpace();
		Program.CURRENT.prepareFrame();

		vao.bind();

		vao.enableAttributeArray(vertexLocation);
		if (normalLocation != -1)
		{
			vao.enableAttributeArray(normalLocation);
		}
		if (colorLocation != -1)
		{
			vao.enableAttributeArray(colorLocation);
		}
		if (textureLocation != -1)
		{
			vao.enableAttributeArray(textureLocation);
		}
		if (clipLeftLocation != -1)
		{
			vao.enableAttributeArray(clipLeftLocation);
		}
		if (clipRightLocation != -1)
		{
			vao.enableAttributeArray(clipRightLocation);
		}
		if (clipTopLocation != -1)
		{
			vao.enableAttributeArray(clipTopLocation);
		}
		if (clipBottomLocation != -1)
		{
			vao.enableAttributeArray(clipBottomLocation);
		}

		GLContext.enable(Graphics.Constants.GL_CLIP_DISTANCE0);
		GLContext.enable(Graphics.Constants.GL_CLIP_DISTANCE1);
		GLContext.enable(Graphics.Constants.GL_CLIP_DISTANCE2);
		GLContext.enable(Graphics.Constants.GL_CLIP_DISTANCE3);

		buildGLBuffers();

		debugLine += "    Flushed " + vertexCount + " vertices" + '\n';

		GLContext.drawArrays(vao, renderPrimitive, 0, vertexCount);

		vao.disableAttributeArray(vertexLocation);
		if (normalLocation != -1)
		{
			vao.disableAttributeArray(normalLocation);
		}
		if (colorLocation != -1)
		{
			vao.disableAttributeArray(colorLocation);
		}
		if (textureLocation != -1)
		{
			vao.disableAttributeArray(textureLocation);
		}
		if (clipLeftLocation != -1)
		{
			vao.disableAttributeArray(clipLeftLocation);
		}
		if (clipRightLocation != -1)
		{
			vao.disableAttributeArray(clipRightLocation);
		}
		if (clipTopLocation != -1)
		{
			vao.disableAttributeArray(clipTopLocation);
		}
		if (clipBottomLocation != -1)
		{
			vao.disableAttributeArray(clipBottomLocation);
		}

		GLContext.disable(Graphics.Constants.GL_CLIP_DISTANCE0);
		GLContext.disable(Graphics.Constants.GL_CLIP_DISTANCE1);
		GLContext.disable(Graphics.Constants.GL_CLIP_DISTANCE2);
		GLContext.disable(Graphics.Constants.GL_CLIP_DISTANCE3);
		GLContext.bindVertexArrayObject(null);

		vertexCount = 0;
		normalCount = 0;
		colorCount = 0;
		textureCount = 0;
	}

	public void flushOnOverflow(int capacity)
	{
		if (capacity > maxBatchSize)
		{
			throw new Exceptions.Exception("[" + this.getClass().getName() + ".flushOnOverflow(" + capacity + ")] Requesting capacity greater than maximum batch size");
		}
		if (vertexCount + capacity > maxBatchSize)
		{
			debugLine += "    Overflowed by " + (maxBatchSize - (vertexCount + capacity)) + '\n';
			end(true);
		}
	}

	public void applyShaderProgram(Program program)
	{
		if (active)
		{
			if (!(Program.CURRENT == program))
			{
				debugLine += "    " + program.getClass().getName() + " Shader applied mid draw, forced flush" + '\n';
				end(true);
			}
		}
		if (!active)
		{
			debugLine += "    " + program.getClass().getName() + " Shader applied" + '\n';
			program.use();
			setVertexLocation(program.getAttribute("position"));
			setNormalLocation(program.getAttribute("normal"));
			setColorLocation(program.getAttribute("color"));
			setTextureLocation(program.getAttribute("texCoord"));
			setClipLeftLocation(program.getAttribute("clipLeft"));
			setClipRightLocation(program.getAttribute("clipRight"));
			setClipTopLocation(program.getAttribute("clipTop"));
			setClipBottomLocation(program.getAttribute("clipBottom"));
		}
	}

	public void vertex(float x, float y, float z, float w)
	{
		if (vertexCount >= batchSize)
		{
			if (batchSize >= maxBatchSize)
			{
				flush();
			}
			else
			{
				resizeBuffers(batchSize + MathUtils.min(1024, maxBatchSize - batchSize));
			}
		}

		fillEmptySpace();
		final int offset = vertexCount * 4;
		vertexData[offset] = x;
		vertexData[offset + 1] = y;
		vertexData[offset + 2] = z;
		vertexData[offset + 3] = w;
		vertexCount++;

		clipLeftData[offset] = clipPlaneLeft.normal.x;
		clipLeftData[offset + 1] = clipPlaneLeft.normal.y;
		clipLeftData[offset + 2] = clipPlaneLeft.normal.z;
		clipLeftData[offset + 3] = clipPlaneLeft.d;

		clipRightData[offset] = clipPlaneRight.normal.x;
		clipRightData[offset + 1] = clipPlaneRight.normal.y;
		clipRightData[offset + 2] = clipPlaneRight.normal.z;
		clipRightData[offset + 3] = clipPlaneRight.d;

		clipTopData[offset] = clipPlaneTop.normal.x;
		clipTopData[offset + 1] = clipPlaneTop.normal.y;
		clipTopData[offset + 2] = clipPlaneTop.normal.z;
		clipTopData[offset + 3] = clipPlaneTop.d;

		clipBottomData[offset] = clipPlaneBottom.normal.x;
		clipBottomData[offset + 1] = clipPlaneBottom.normal.y;
		clipBottomData[offset + 2] = clipPlaneBottom.normal.z;
		clipBottomData[offset + 3] = clipPlaneBottom.d;
	}

	public void normal(float x, float y, float z, float w)
	{
		final int offset = normalCount * 4;
		normalData[offset] = x;
		normalData[offset + 1] = y;
		normalData[offset + 2] = z;
		normalData[offset + 3] = w;
		normalCount++;
	}

	public void color(float r, float g, float b, float a)
	{
		final int offset = colorCount * 4;
		colorData[offset] = r;
		colorData[offset + 1] = g;
		colorData[offset + 2] = b;
		colorData[offset + 3] = a;
		colorCount++;
	}

	public void texCoord(float u, float v)
	{
		final int offset = textureCount * 2;
		textureData[offset] = u;
		textureData[offset + 1] = v;
		textureCount++;
	}

	public void vertex(Vector2 v)
	{
		vertex(v.x, v.y);
	}

	public void vertex(Vector3 v)
	{
		vertex(v.x, v.y, v.z);
	}

	public void vertex(Vector4 v)
	{
		vertex(v.z, v.y, v.z, v.w);
	}

	public void vertex(float x, float y)
	{
		vertex(x, y, 0, 1);
	}

	public void vertex(float x, float y, float z)
	{
		vertex(x, y, z, 1);
	}

	public void normal(Vector3 v)
	{
		normal(v.x, v.y, v.z);
	}

	public void normal(Vector4 v)
	{
		normal(v.x, v.y, v.z, v.w);
	}

	public void normal(float x, float y, float z)
	{
		normal(x, y, z, 0);
	}

	public void color(Color color)
	{
		color(color.r, color.g, color.b, color.a);
	}

	public void texCoord(Vector2 v)
	{
		texCoord(v.x, v.y);
	}

	public int getBatchSize()
	{
		return batchSize;
	}

	public int getMaxBatchSize()
	{
		return maxBatchSize;
	}

	public boolean isActive()
	{
		return active;
	}

	protected void setVertexLocation(int vertexLocation)
	{
		this.vertexLocation = vertexLocation;
	}

	protected void setNormalLocation(int normalLocation)
	{
		this.normalLocation = normalLocation;
	}

	protected void setColorLocation(int colorLocation)
	{
		this.colorLocation = colorLocation;
	}

	protected void setTextureLocation(int textureLocation)
	{
		this.textureLocation = textureLocation;
	}

	protected void setClipLeftLocation(int clipLeftLocation)
	{
		this.clipLeftLocation = clipLeftLocation;
	}

	protected void setClipRightLocation(int clipRightLocation)
	{
		this.clipRightLocation = clipRightLocation;
	}

	protected void setClipTopLocation(int clipTopLocation)
	{
		this.clipTopLocation = clipTopLocation;
	}

	protected void setClipBottomLocation(int clipBottomLocation)
	{
		this.clipBottomLocation = clipBottomLocation;
	}

	private void buildGLBuffers()
	{
		vao.bind();
		if (vertexLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(vertexData);
			vboVertex.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(vertexLocation, 4, Graphics.Constants.GL_FLOAT, vboVertex);
			DirectBuffer.free(buffer);
			vertexData = new float[batchSize * 4];
		}
		if (normalLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(normalData);
			vboNormal.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(normalLocation, 4, Graphics.Constants.GL_FLOAT, vboNormal);
			DirectBuffer.free(buffer);
			normalData = new float[batchSize * 4];
		}
		if (colorLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(colorData);
			vboColor.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(colorLocation, 4, Graphics.Constants.GL_FLOAT, vboColor);
			DirectBuffer.free(buffer);
			colorData = new float[batchSize * 4];
		}
		if (textureLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(textureData);
			vboTexture.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(textureLocation, 2, Graphics.Constants.GL_FLOAT, vboTexture);
			DirectBuffer.free(buffer);
			textureData = new float[batchSize * 2];
		}
		if (clipLeftLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(clipLeftData);
			vboClipLeft.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(clipLeftLocation, 4, Graphics.Constants.GL_FLOAT, vboClipLeft);
			DirectBuffer.free(buffer);
			clipLeftData = new float[batchSize * 4];
		}
		if (clipRightLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(clipRightData);
			vboClipRight.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(clipRightLocation, 4, Graphics.Constants.GL_FLOAT, vboClipRight);
			DirectBuffer.free(buffer);
			clipRightData = new float[batchSize * 4];
		}
		if (clipTopLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(clipTopData);
			vboClipTop.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(clipTopLocation, 4, Graphics.Constants.GL_FLOAT, vboClipTop);
			DirectBuffer.free(buffer);
			clipTopData = new float[batchSize * 4];
		}
		if (clipBottomLocation != -1)
		{
			DirectBuffer buffer = DirectBuffer.wrap(clipBottomData);
			vboClipBottom.uploadData(buffer, BufferObject.Usage.STATIC_DRAW);
			vao.pointAttribute(clipBottomLocation, 4, Graphics.Constants.GL_FLOAT, vboClipBottom);
			DirectBuffer.free(buffer);
			clipBottomData = new float[batchSize * 4];
		}
	}

	private void resizeBuffers(int newBatchSize)
	{
		if (newBatchSize <= batchSize || newBatchSize > maxBatchSize)
		{
			Environment.log
					.debug("[" + this.getClass().getName() + ".resizeBuffers(" + newBatchSize + ")] Attempting to create a batch with size greater than maximum batch size.");
			throw new Exceptions.Exception("Illegal Operation :: Resizeing batch buffers past max batch size");
		}
		// resize vertex data
		float[] temp = new float[newBatchSize * 4];
		for (int i = 0; i < vertexData.length; i++)
		{
			temp[i] = vertexData[i];
		}
		vertexData = temp;
		// resize normal data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < normalData.length; i++)
		{
			temp[i] = normalData[i];
		}
		normalData = temp;
		// resize color data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < colorData.length; i++)
		{
			temp[i] = colorData[i];
		}
		colorData = temp;
		// resize texture data
		temp = new float[newBatchSize * 2];
		for (int i = 0; i < textureData.length; i++)
		{
			temp[i] = textureData[i];
		}
		textureData = temp;
		// resize clipLeft data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < clipLeftData.length; i++)
		{
			temp[i] = clipLeftData[i];
		}
		clipLeftData = temp;
		// resize clipRight data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < clipRightData.length; i++)
		{
			temp[i] = clipRightData[i];
		}
		clipRightData = temp;
		// resize clipTop data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < clipTopData.length; i++)
		{
			temp[i] = clipTopData[i];
		}
		clipTopData = temp;
		// resize clipBottom data
		temp = new float[newBatchSize * 4];
		for (int i = 0; i < clipBottomData.length; i++)
		{
			temp[i] = clipBottomData[i];
		}
		clipBottomData = temp;

		batchSize = newBatchSize;
	}

	private void fillEmptySpace()
	{
		Color fillColor = textureCount == vertexCount ? Color.TRANSPARENT : Color.WHITE;
		while (normalCount < vertexCount)
		{
			normal(0, 0, 0, 0);
		}
		while (colorCount < vertexCount)
		{
			color(fillColor);
		}
		while (textureCount < vertexCount)
		{
			texCoord(0, 0);
		}
	}
}
