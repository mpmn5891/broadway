package bmg.broadway.graphics;

import java.util.Deque;
import java.util.LinkedList;

import bmg.broadway.geometry.Rectangle;
import bmg.emef.environment.Environment;

public class ClipStack
{
	private final Rectangle display = new Rectangle();
	private Deque<Rectangle> stack = new LinkedList<>();

	public ClipStack()
	{
		display.set(0, 0, Environment.display.getWidth(), Environment.display.getHeight());

		Environment.eventManager.registerResizeListener(() ->
		{
			display.set(0, 0, Environment.display.getWidth(), Environment.display.getHeight());
		});
	}

	public boolean push(Rectangle rectangle)
	{
		boolean clipped = false;

		if (!rectangle.equals(display))
		{
			Rectangle currentClip = peek();

			mergeDown(currentClip, rectangle);

			if (rectangle.width == 0 && rectangle.height > 0)
			{
				rectangle.width = 1;
			}
			if (rectangle.height == 0 && rectangle.width > 0)
			{
				rectangle.height = 1;
			}

			if (rectangle.width > 0 && rectangle.width < 1)
			{
				rectangle.width = 1;
			}
			if (rectangle.height > 0 && rectangle.height < 1)
			{
				rectangle.height = 1;
			}

			if (rectangle.width == 0 || rectangle.height == 0)
			{
				clipped = true;
			}

			stack.push(rectangle);
		}

		return clipped;
	}

	public Rectangle pop()
	{
		return stack.isEmpty() ? display : stack.pop();
	}

	public Rectangle peek()
	{
		return stack.isEmpty() ? display : stack.peek();
	}

	private Rectangle mergeDown(Rectangle bottom, Rectangle top)
	{
		if (bottom.contains(top))
		{
			return top;
		}

		if (bottom.intersects(top))
		{
			if (top.x < bottom.x)
			{
				top.width -= bottom.x - top.x;
				top.x = bottom.x;
			}
			if (top.y < bottom.y)
			{
				top.height -= bottom.y - top.y;
				top.y = bottom.y;
			}

			if (top.x + top.width > bottom.x + bottom.width)
			{
				top.width = top.width - ((top.x + top.width) - (bottom.x + bottom.width));
			}
			if (top.y + top.height > bottom.y + bottom.height)
			{
				top.height = top.height - ((top.y + top.height) - (bottom.y + bottom.height));
			}
		}
		else
		{
			top.width = 0;
			top.height = 0;
		}

		return top;
	}
}
