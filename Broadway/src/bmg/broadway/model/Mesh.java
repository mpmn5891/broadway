package bmg.broadway.model;

import java.util.ArrayList;
import java.util.List;

import bmg.emef.graphics.Color;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;

public class Mesh
{
	private final List<Vector3> vertices = new ArrayList<>();
	private final List<Vector3> normals = new ArrayList<>();
	private final List<Vector2> texCoords = new ArrayList<>();
	private final List<Color> colors = new ArrayList<>();

}
